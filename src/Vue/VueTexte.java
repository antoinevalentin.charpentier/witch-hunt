package Vue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

import Controleur.ControleurClicEntier;
import Controleur.ControleurSaisiNomJoueur;
import Controleur.ControleurClicCarteRumeur;
import Controleur.ControleurClicJoueur;

import Modele.Partie;
import Modele.enums.EnumStatutPartie;

@SuppressWarnings("deprecation")
/**
 * Classe qui permet d'impl�menter une vue textuelle du jeu.
 * Tous les messages situ�s dans la liste listeMessagesDetailleAAfficher de la classe Partie seront affich�s dans la console.
 * Toutes les saisies sont effectu�es via la console �galement.
 * Toutes les interactions entre le programme et l'utilisateur se font par l'interm�diaire du terminal.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class VueTexte implements Observer, Runnable {

	/**
	 * Attribut qui stocke l'indice du dernier message lu parmi la liste listeMessagesDetailleAAfficher de la classe Partie.
	 */
	private int indiceMessageDansListe;
	
	/**
	 * Constructeur de la classe VueTexte.
	 * Il permet d'initialiser l'indice du dernier message affich� dans la console.
	 * Ajouter la vue en tant qu'observer de l'instance de la partie.
	 * Ce constructeur permet �galement de d�marrer la vue sur un autre Thread.
	 */
    public VueTexte() {
    	this.indiceMessageDansListe = 0;
		Partie.getPartie().addObserver(this);
		Thread t = new Thread(this);
		t.start();
    }
    
    /**
     * M�thode qui permet de demander � un joueur de saisir un �l�ment particulier en fonction du statut de la partie.
     * Cette saisie s'effectue sur un autre thread.
     */
    public void run() {

    	//liste de l'ensemble des statut de la partie qui demande une s�lection d'une carte rumeur
		ArrayList<EnumStatutPartie> listeSelectionCarteRumeur = new ArrayList<>(Arrays.asList(EnumStatutPartie.SELECTION_CARTE_RUMEUR_UTILISATION_EFFET,EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_DEFAUSSER,EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_RECUPERER)); 
		//liste de l'ensemble des statut de la partie qui demande une s�lection d'un joueur
		ArrayList<EnumStatutPartie> listeSelectionJoueur = new ArrayList<>(Arrays.asList(EnumStatutPartie.SELECTION_JOUEUR_SUIVANT,EnumStatutPartie.SELECTION_JOUEUR_ACCUSATION));  
		//liste de l'ensemble des statut de la partie qui demande une s�lection d'un entier
		ArrayList<EnumStatutPartie> listeSelectionEntier = new ArrayList<>(Arrays.asList(EnumStatutPartie.SELECTION_ENTIER_MENU,EnumStatutPartie.SELECTION_ENTIER_ACCUSATION_OU_EFFET_CHASSEUR,EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_EFFET_SORCIERE, EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION,EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_DEFAUSSER,EnumStatutPartie.SELECTION_ENTIER_ROLE,EnumStatutPartie.SELECTION_ENTIER_NOMBRE_JOUEUR,EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE)); 
		
    			
		boolean quitter = false;
		boolean besoinDeSaisirUnElement = true;
		do {
			//on r�cup�re le bool�en qui permet de savoir si une saisi est attendu ou non
			synchronized(Partie.getPartie()) {
				besoinDeSaisirUnElement = Partie.getPartie().isSelectionEnAttente();
			}
			//on regarde si le joueur doit saisir un �l�ment dans la console
			if(besoinDeSaisirUnElement) {			
				if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_CHAINE_CARACTERE_NOM_JOUEUR)) {
					//on regarde s'il a besoin de saisir le nom d'un joueur
					ControleurSaisiNomJoueur.saisirNomJoueur();
			    }else if(listeSelectionCarteRumeur.contains(Partie.getPartie().getStatutPartie())) {
			    	//on regarde si l'utilisateur doit saisir le nom d'une carte rumeur
			    	if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_DEFAUSSER)) {
			    		//s'il doit choisir une carte rumeur qu'il doit d�fausser par la suite
			    		ControleurClicCarteRumeur.choisirCarteRumeurParmiListe("dont vous voulez vous s�parer");
			    	}else if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_RECUPERER)) {
			    		//on regarde s'il doit choisir une carte rumeur qu'il va r�cup�rer par la sute
			    		ControleurClicCarteRumeur.choisirCarteRumeurParmiListe("dont vous pouvez r�cup�rer");
			    	}else {
			    		ControleurClicCarteRumeur.choisirCarteRumeurParmiListe("");
			    	}
			    }else if(listeSelectionJoueur.contains(Partie.getPartie().getStatutPartie())) {
			    	//on regarde si le joueur doit choisir un autre joueur
			    	ControleurClicJoueur.choisirJoueurParmiListe();
			    }else if(listeSelectionEntier.contains(Partie.getPartie().getStatutPartie())) {
			    	//on regarde si le joueur doit saisir un entier
			    	ControleurClicEntier.choisirParmiIntervalleEntier();
			    	//d�s que le joueur � s�lection� un entier du menu, aloes on clear la liste des messages, il faut alors r�initialiser le compteur de l'indice des messages dans la liste
			    	if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_MENU)) {
			    		this.indiceMessageDansListe = 0;
			    	}
			    }
			}
		} while (quitter == false);
		System.exit(0);
    }

    @Override
    /**
     * Quand la classe Partie notifie la vue, on affiche alors � l'�cran les nouveaux messages qui se sont ajout�s dans la liste listeMessagesDetailleAAfficher de la classe Partie par rapport � la derni�re fois.
     */
    public synchronized void update(Observable arg0, Object arg1) {
    	//on affiche tout les messages qui ont besoin d'�tre affich� dans la vue
    	//ces messages sont stock� dans un attribut de la classe partie : listeMessageAAfficher
    	//on parcours alors l'ensemble des message a afficher
    	while(Partie.getPartie().getListeMessagesDetailleAAfficher().size()>this.indiceMessageDansListe) {
    		System.out.println(Partie.getPartie().getListeMessagesDetailleAAfficher().get(this.indiceMessageDansListe));
    		this.indiceMessageDansListe++;
    	}
    	
    }

}
