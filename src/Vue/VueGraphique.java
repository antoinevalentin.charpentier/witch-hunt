package Vue;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

import Modele.CarteRumeur;
import Modele.Effet;
import Modele.Joueur;
import Modele.Main;
import Modele.Partie;
import Modele.enums.EnumJouabiliteEffetCarteRumeur;
import Modele.enums.EnumRole;
import Modele.enums.EnumStatutPartie;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import Controleur.ControleurClicCarteRumeur;
import Controleur.ControleurClicEntier;
import Controleur.ControleurClicJoueur;
import Controleur.ControleurRotationIdentite;
import Controleur.ControleurSaisiNomJoueur;
import Controleur.ControleurSlider;

import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.awt.Color;
import java.awt.Font;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

@SuppressWarnings("deprecation")
/**
 * Classe qui permet d'impl�menter une interface graphique au jeu.
 * L'utilisateur effectue les diff�rentes saisie en cliquant sur des boutons de l'interface.
 * Il y a deux containers diff�rents, un affichage d�di� au menu et l'autre au jeu.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class VueGraphique implements Observer{
	/**
	 * La fen�tre o� l'on dessine les diff�rents composants du jeu.
	 */
	private JFrame frame;
	
	/**
	 * Container regroupant les diff�rents composants du menu. C'est-�-dire par exemple le fond du menu, les diff�rents boutons.
	 */
	private JPanel containerMenu;
	
	/**
	 * Container regroupant les diff�rents composants affich�s lors d'une partie de Witch Hunt.
	 * Ce container se divise en deux sous-containers : panelInformations, panelContenuSelection.
	 * panelInformations d�signe le JPanel qui contient l'ensemble des informations d'une manche, c'est-�-dire la partie sup�rieure de l'�cran encadr� avec du orange : boite de dialogue, num�ro de manche, informations sur la partie.
	 * panelContenuSelection d�signe l'emplacement sur la fen�tre o� le joueur pourra cliquer sur des composants pour effectuer les diff�rents choix.
	 */
	private JPanel containerJeu;
	
	/**
	 * Composant qui contient l'ensemble des messages qui sont affich� � l'utilisateur.
	 * Les messages qui y vont �tre affich�s � l'int�rieur de ce composant appartiennent � l'attribut listeMessagesAAfficher de la classe partie.
	 * Les messages � l'int�rieur de ce composant permettent de contextualiser, d'informer l'utilisateur sur ce qu'il se passe et ce qui est attendu de lui. 
	 * Ce composant appartient au panelContenuSelection pr�sent dans le container de jeu.
	 */
	private JTextArea textAreaBoiteDialogue;
	
	/**
	 * Composant qui permet d'afficher toutes les informations associ�es � une manche lors d'une partie.
	 * Nous pouvons y retrouver :
	 * <ul>
	 * <li>Le joueur qui doit jouer</li>
	 * <li>Le joueur qui a d�but� le tour</li>
	 * <li>Les joueurs encore en vie durant la manche</li>
	 * <li>Le score de chaque joueurs</li>
	 * <li>le r�le de la personne qui doit joueur</li>
	 * </ul>
	 */
	private JTextArea textAreaInformationsManche;
	
	/**
	 * Texte affich� en haut de l'�cran lors d'une partie pour indiquer le num�ro de la manche en cours. 
	 */
	private JLabel labelNumeroManche;
	
	/**
	 * D�signe le container sur la fen�tre qui contiendra tout les composants o� le joueur pourra cliquer sur ces composants pour effectuer les diff�rents choix.
	 */
	private JPanel panelContenuSelection;
	
	/**
	 * Attribut qui stocke l'indice du dernier message lu parmi la liste listeMessagesAAfficher de la classe Partie.
	 */
	private int indiceMessageDansListe;

	/**
	 * D�marre une partie de Witch Hunt avec les deux vues focntionelles en m�me temps (vue graphique et vue texte)
	 */
	public static void main(String[] args) {
		//on lance la vue graphique
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VueGraphique window = new VueGraphique();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		//on lance la vue texte
		@SuppressWarnings("unused")
		VueTexte v = new VueTexte();
		
		//on lance le mod�le
		Main.lancerWitchHunt();
	}

	/**
	 * Constructeur de la classe VueGraphique.
	 * Il permet d'initialiser l'indice du dernier message affich� � l'�cran.
	 * Ajouter la vue en tant qu'observer de l'instance de la partie.
	 * Ce constructeur permet �galement d'intialiser les diff�rents composants qui seront affich�s � l'�cran.
	 */
	public VueGraphique() {
		//on initialise l'indice des messages dans la liste
		this.indiceMessageDansListe=0;
		//on intialise la fenetre
		this.initialize();
		//on ajoute cette vue en tant qu'observer de la partie
		Partie.getPartie().addObserver(this);
	}

	/**
	 * Initialisation du contenue pr�sent dans l'interface graphique.
	 * Modification de certaine propri�t� de la fen�tre : d�sactiver le redimensionnement, ...
	 * Cr�ation des diff�rents containers.
	 * Affichage du container menu � l'�cran.
	 */
	private void initialize() {	
		//cr�ation de la fen�tre
		this.frame = new JFrame();
		//d�sactivation du redimensionage de la fenetre
		this.frame.setResizable(false);
		//d�finition de la taille de la fenetre
		this.frame.setBounds(0, 0, 1280, 720);
		//fermer la fenetre via le clic sur la croix rouge
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//mise en position absolute des diff�rents composants enfant � la fen�tre
		this.frame.getContentPane().setLayout(null);
		
		//cr�ation du container qui sera affich� lors du menu
		this.creationContainerMenu();
		//cr�ation du container qui sera affich� lors de la partie
		this.creationContainerJeu();
		
		//on affiche ans un premier temps le container du menu lors du d�marrage du jeu
		this.frame.getContentPane().add(this.containerMenu);
	}

	@Override
	/**
	 * M�thode qui permet de mettre � jour l'affichage de la fen�tre en fonction du statut de la partie.
	 * Elle permet de passer d'un container � un autre.
	 * Quand le joueur retourne sur le menu du jeu, on r�initialise l'indice des messages.
	 * Cette m�thode permet �galement de mettre � jour le container du jeu.
	 * Elle affiche le container appropri� � la situation.
	 */
	public void update(Observable o, Object arg) {
		//on retire tout les composants appartenant � la fenetre
		this.frame.getContentPane().removeAll();
		
		if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_MENU)) {
			//on regarde si on doit afficher la partie
			//on dit � la fen�tre d'afficher le composant du menu
			this.frame.getContentPane().add(containerMenu);
			//on r�intialise l'indice des messages dans la liste, car on supprime tout les messages lors du menu
			this.indiceMessageDansListe=0;
		}else {
			//on regarde si on doit afficher le jeu
			//on mets � jour les composants du container de jeu
			this.updateContainerJeu();
			//on dit � la fen�tre d'afficher ce composant ci
			this.frame.getContentPane().add(containerJeu);
		}
		//on repaint l'�cran pour que le joueur voit les modifications apport�.
		frame.repaint(1, 0, 0, 1280, 720);
		
	}
	
	/**
	 * M�thode qui permet de mettre � jour le contenue du container de jeu en fonction du statut de la partie.
	 * Par exemple, elle affiche les nouveaux messages � l'�cran, affiche les diff�rents composant pour r�aliser la s�lection attendue...
	 */
	public void updateContainerJeu() {
		//on mets les instructions pour calculer les dimenssions d'une chaine de caract�res en pixel sous certaine police d'�criture
		AffineTransform affinetransform = new AffineTransform();     
	    FontRenderContext frc = new FontRenderContext(affinetransform,true,true);  
	    
	    //initialisation des polices d'�criture
	    Font POLICE_CALIBRI = new Font("Calibri", Font.PLAIN, 13);
	    Font POLICE_CALIBRI_GROS = new Font("Calibri", Font.PLAIN, 20);
	    
	    //on mets � jours les messages de la boite de dialogue
		this.textAreaBoiteDialogue.setText(this.conversionListeDesMessagesEnChaineDeCaracteres());
		
		//liste de l'ensemble des statut de la partie qui demande une s�lection d'une carte rumeur
		ArrayList<EnumStatutPartie> listeStatutSelectionCarteRumeur = new ArrayList<>(Arrays.asList(EnumStatutPartie.SELECTION_CARTE_RUMEUR_UTILISATION_EFFET,EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_DEFAUSSER,EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_RECUPERER)); 
		//liste de l'ensemble des statut de la partie qui demande une s�lection d'un joueur
		ArrayList<EnumStatutPartie> listeStatutSelectionJoueur = new ArrayList<>(Arrays.asList(EnumStatutPartie.SELECTION_JOUEUR_SUIVANT,EnumStatutPartie.SELECTION_JOUEUR_ACCUSATION));  
		
		if(Partie.getPartie().getNumeroManche() == 0) {
			//on regarde si on doit afficher Initialisation de la partie, tant que la partie n'a pas commence (saisi des joueurs)
			this.labelNumeroManche.setText("Initialisation de la partie");
			//on n'affiche donc pas les informations de la manche car celle ci n'a pas commenc�
			this.textAreaInformationsManche.setText("");
		}else {
			//si la partie � d�j� commenc�, on met � jour le num�ro de la manche 
			this.labelNumeroManche.setText("Manche n�"+Partie.getPartie().getNumeroManche());
			//et on mets � jour les informations associ� � la manche, donne des informations �galement � propos de la partie
			this.textAreaInformationsManche.setText(this.conversionListeDesInformationsMancheEnChaineDeCaracteres());
		}
		
		if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_NOMBRE_JOUEUR)) {
			//si l'utilisateur doit saisir le nombre de joueur r�el ou virtuel
			//on supprime tout les composants enfant du panel de s�lection
			this.panelContenuSelection.removeAll();
			//on calcul les bornes inf�rieur, sup�rieur et la valeur moyenne des bornes
			int borneSuperieur = Partie.getPartie().getSelectionEntierIntervale().get(1);
			int borneInferieur = Partie.getPartie().getSelectionEntierIntervale().get(0);
			int valeurParDefaut = borneInferieur+(borneSuperieur-borneInferieur)/2;
			
			//on ajoute un slider qui va permettre au joueur de saisir le nombre de joueur compris parmi les borne de l'intervalle d�finit pr�c�demment
			JSlider slider = new JSlider(JSlider.HORIZONTAL,borneInferieur,borneSuperieur,valeurParDefaut);
			slider.setBounds(333,50,600,90);
			slider.setPaintLabels(true);
			slider.setPaintTicks(true);
			slider.setPaintTrack(true);
			slider.setMinorTickSpacing(1);
		    slider.setMajorTickSpacing(1);
		    slider.setBackground(new Color(255,255,255));
		    
		    //on ajoute le bouton confirmer et on le dimenssione
		    JButton btnConfirmer = new JButton("Confirmer");
		    btnConfirmer.setBounds(534,200,200,50);
		    
		    //on ajoute un controlleur qui va permettre de d�tecter un clic sur le bouton confirmer, il va permettre de valider le nombre saisi sur le sldier
		    new ControleurSlider(slider,btnConfirmer);
		    
		    //on ajoute les diff�rents composant au panel de contenu de s�lection
		    this.panelContenuSelection.add(btnConfirmer);
			this.panelContenuSelection.add(slider);
			
			this.panelContenuSelection.add(slider);
		}else if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_CHAINE_CARACTERE_NOM_JOUEUR)) {
			//si le joueur doit s�lection le nom d'un joueur
			//on supprime tout les composants enfant du panel de s�lection
			this.panelContenuSelection.removeAll();
			//on ajoute un texte pour lui dire de saiisir le nom d'un joueur dans le panel de s�lection
			JLabel labelDescription = new JLabel("Veuillez saisir le nom du joueur");
			labelDescription.setFont(POLICE_CALIBRI);
			labelDescription.setBounds((frame.getWidth()-15-(int)(POLICE_CALIBRI.getStringBounds(labelDescription.getText(), frc).getWidth())+1)/2,50,(int)(POLICE_CALIBRI.getStringBounds(labelDescription.getText(), frc).getWidth())+10,(int)(POLICE_CALIBRI.getStringBounds(labelDescription.getText(), frc).getHeight())+2);
			
			//on ajoute un champ de texte o� il va saisir le nom
			JTextField textFieldSaisiNomJoueur = new JTextField();
			textFieldSaisiNomJoueur.setBounds(534,100,200,40);
			
			//on ajoute le bouton confirmer pour valider la saisi
			JButton btnValiderNomSaisi = new JButton("Valider");
			btnValiderNomSaisi.setBounds(534,160,200,40);
			
			//on ajoute un controlleur qui va permettre de d�tecter un clic sur le bouton confirmer, et donc envoyer le nom
			new ControleurSaisiNomJoueur(textFieldSaisiNomJoueur,btnValiderNomSaisi);
			
			//on ajoute les diff�rents composant au panel de contenu de s�lection
			this.panelContenuSelection.add(btnValiderNomSaisi);
			this.panelContenuSelection.add(textFieldSaisiNomJoueur);
			this.panelContenuSelection.add(labelDescription);
		}else if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION)) {
			//si le joueur doit appuyer sur le bouton confirmer de l'�cran
			//on supprime tout les composants enfant du panel de s�lection
			this.panelContenuSelection.removeAll();
			//on affiche le bouton confirmer
			JButton btnConfirmer = new JButton("CONFIRMER");
			btnConfirmer.setBounds((this.frame.getWidth()-218)/2, 100, 218, 75);
			
			//on ajoute les diff�rents composant au panel de contenu de s�lection
			this.panelContenuSelection.add(btnConfirmer);
			//on ajoute un controlleur qui va permettre de d�tecter un clic sur le bouton confirmer, et donc envoyer l'information de confirmation
			new ControleurClicEntier<JButton>(0, btnConfirmer);
		}else if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE)) {
			//s'il doit valider qu'il a bien vue la liste de carte
			//on supprime tout les composants enfant du panel de s�lection
			this.panelContenuSelection.removeAll();
			///on lui affiche les cartes qu'il peut jouer
			this.afficherListeCarteRumeurGraphiquement();
			//on ajoute un bouton pour qu'il puisse confirmer qu'il a bien regard� les cartes rumeurs qu'il poss�de
			JButton btnConfirmer = new JButton("CONFIRMER");
			btnConfirmer.setBounds(524, 305, 218, 30);
			//on ajoute les diff�rents composant au panel de contenu de s�lection
			this.panelContenuSelection.add(btnConfirmer);
			//on ajoute un controlleur qui va permettre de d�tecter un clic sur le bouton confirmer, et donc passer � la suite
			new ControleurClicEntier<JButton>(0, btnConfirmer);
		}else if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_ROLE)) {
			//on regarde s'il doit choisir son r�le
			//on supprime tout les composants enfant du panel de s�lection
			this.panelContenuSelection.removeAll();
			
			//on d�finit le fond graphique de la carte identit� en fonction du r�le du joueur
			//le r�le s�lectionn� se situe vers le haut
			JLabel background = new JLabel();
			if(Partie.getPartie().getJoueurQuiDoitJouer().getCarteIdentite().getRole().equals(EnumRole.SORCIERE)) {
				background.setIcon(new ImageIcon(getClass().getResource("/carteIdentiteSorciere.png")));
			}else {
				background.setIcon(new ImageIcon(getClass().getResource("/carteIdentiteVillageois.png")));
			}
			background.setBounds(334, 15, 198, 283);//on le positionne
			
			//on ajoute un bouton, quand on clique dessus, cela change le r�le du joueur et l'orientation de la carte
			JLabel labelRotationCarte = new JLabel(new ImageIcon(getClass().getResource("/imageRotation.png")));
			labelRotationCarte.setBounds(802, 75, 64, 64);
			//on l'associe � un controleur pour d�tecter le clique sur le bouton de rotation
			new ControleurRotationIdentite(labelRotationCarte);
			
			//on ajoute le bouton confirmer, quand le joueur va appuyer dessus, cela va envoyer l'information sur le nouveau r�le du joueur, 
			JButton btnConfirmer = new JButton("CONFIRMER");
			btnConfirmer.setBounds(734, 150, 200, 50);
			//on ajoute les diff�rents composant au panel de contenu de s�lection
			this.panelContenuSelection.add(labelRotationCarte);
			this.panelContenuSelection.add(background);
			this.panelContenuSelection.add(btnConfirmer);
			
			new ControleurClicEntier<JButton>(0, btnConfirmer);
		}else if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_ACCUSATION_OU_EFFET_CHASSEUR)) {
			//si le joueur doit choisir entre accuser ou utiliser l'effet chasseur d'une carte rumeur
			//on supprime tout les composants enfant du panel de s�lection
			this.panelContenuSelection.removeAll();
			//on ajoute un bouton pour accuser une personne d'�tre une sorci�re
			JButton btnAccuser = new JButton("Accuser un autre joueur d'�tre une sorci�re");
			btnAccuser.setBounds(434, 75, 400, 50);
			btnAccuser.setFont(POLICE_CALIBRI_GROS);
			//on ajoute un bouton pour qu'il utilise l'effet chasseur d'une carte rumeur
			JButton btnCarte = new JButton("Utiliser l'effet chasseur d'une carte rumeur");
			btnCarte.setBounds(434, 200, 400, 50);
			btnCarte.setFont(POLICE_CALIBRI_GROS);
			//on ajoute les diff�rents composant au panel de contenu de s�lection
			this.panelContenuSelection.add(btnAccuser);
			this.panelContenuSelection.add(btnCarte);
			//on ajoute les controleur pour d�tecter les clic sur les boutons et envoyer l'informations ad�quat au mod�le
			new ControleurClicEntier<JButton>(1, btnAccuser);
			new ControleurClicEntier<JButton>(2, btnCarte);
		}else if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_EFFET_SORCIERE)) {
			//on regarde si le joueur doit choisir entre r�v�ler son identit� ou utiliser l'effet sorci�re d'une de ces cartes rumeur
			//on supprime tout les composants enfant du panel de s�lection
			this.panelContenuSelection.removeAll();
			//on aojute le bouton pour qu'il r�v�le son identit�
			JButton btnReveler = new JButton("R�v�ler votre identit�");
			btnReveler.setBounds(434, 75, 400, 50);
			btnReveler.setFont(POLICE_CALIBRI_GROS);
			//on ajoute le bouton pour qu'il utilise l'effet sorci�re d'une carte rumeur
			JButton btnCarte = new JButton("Utiliser l'effet sorci�re d'une carte rumeur");
			btnCarte.setBounds(434, 200, 400, 50);
			btnCarte.setFont(POLICE_CALIBRI_GROS);
			//on ajoute les diff�rents composant au panel de contenu de s�lection
			this.panelContenuSelection.add(btnReveler);
			this.panelContenuSelection.add(btnCarte);
			//on associe les boutons � l'action qu'ils doivent r�aliser
			new ControleurClicEntier<JButton>(1, btnReveler);
			new ControleurClicEntier<JButton>(2, btnCarte);
		}else if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_DEFAUSSER)) {
			//si le joueur doit choisir entre r�v�ler son identit� ou d�fausser une carte rumeur
			//on supprime tout les composants enfant du panel de s�lection
			this.panelContenuSelection.removeAll();
			//on ajoute un bouton pour r�v�ler son identit�
			JButton btnReveler = new JButton("R�v�ler votre identit�");
			btnReveler.setBounds(434, 75, 400, 50);
			btnReveler.setFont(POLICE_CALIBRI_GROS);
			//on ajoute un bouton pour qu'il d�fausse une carte rumeur
			JButton btnCarte = new JButton("D�fausser une carte rumeur de votre main");
			btnCarte.setBounds(434, 200, 400, 50);
			btnCarte.setFont(POLICE_CALIBRI_GROS);
			//on ajoute les diff�rents composant au panel de contenu de s�lection
			this.panelContenuSelection.add(btnReveler);
			this.panelContenuSelection.add(btnCarte);
			//on associe les boutons a un controleur, pour identitifier qu'elle op�ration doit �tre effectu�
			new ControleurClicEntier<JButton>(1, btnReveler);
			new ControleurClicEntier<JButton>(2, btnCarte);
		}else if(listeStatutSelectionJoueur.contains(Partie.getPartie().getStatutPartie())) {
			//si le joueur doit choisir parmi une liste de joueur
			//on supprime tout les composants enfant du panel de s�lection
			this.panelContenuSelection.removeAll();
			//on parcours toute la liste de joueur qu'il peut choisir parmi
			Iterator<Joueur> it = Partie.getPartie().getSelectionJoueurParmiListe().iterator();
			int indice = 0;
			while(it.hasNext()) {
				//on r�cup�re le joueur de l'it�ration
				Joueur joueurIteration = it.next();
				//on cr�e un bouton avec le nom du joueur
				JButton btnJoueur = new JButton(joueurIteration.getNomJoueur());
				btnJoueur.setBounds(534,30+indice*50,200,40);
				//on ajoute les diff�rents composant au panel de contenu de s�lection
				this.panelContenuSelection.add(btnJoueur);
				//on associe le bouton au joueur de l'it�ation par l'interm�diaire d'une controleur
				new ControleurClicJoueur<JButton>(joueurIteration, btnJoueur);
				indice++;
			}
		}else if(listeStatutSelectionCarteRumeur.contains(Partie.getPartie().getStatutPartie())) {
			//si le joueur doit choisir parmi une liste de carte rumeur
			//on supprime tout les composants enfant du panel de s�lection
			this.panelContenuSelection.removeAll();
			//on affiche la liste de carte rumeur
			this.afficherListeCarteRumeurGraphiquement();
		}
	}
	
	/**
	 * M�thode qui permet d'afficher une liste de cartes rumeur � l'�cran, centr�, espac� de la m�me distantes les unes des autres. Les cartes rumeurs doivent �tre stock�es dans l'attribut selectionCarteRumeurParmiListe de la classe Partie.
	 */
	public void afficherListeCarteRumeurGraphiquement() {
		//on instancie un it�rateur sur la liste des carteurs rumeurs que l'on veut afficher
		Iterator<CarteRumeur> it = Partie.getPartie().getSelectionCarteRumeurParmiListe().iterator();
		int i=0;
		//on calcul la distance entre les cartes, proportionelle on nombre de carte
		int distanceEntreLesCartes = (frame.getWidth()-15-198*Partie.getPartie().getSelectionCarteRumeurParmiListe().size())/(Partie.getPartie().getSelectionCarteRumeurParmiListe().size()+1);
		//on parcours toute les cartes
		while(it.hasNext()) {
			//on affiche la carte de l'it�ration
			this.afficherCarteRumeur(distanceEntreLesCartes*(i+1)+i*198, 15, it.next());
			i++;
		}
	}
	
	/**
	 * M�thode qui permet de cr�e le container contenant l'ensemble des composants affich� lors du menu. C'est-�-dire le fond du menu avec les auteurs, et les boutons pour jouer.
	 */
	public void creationContainerMenu() {
		//initialisation des police d'�criture
		Font POLICE_SEGOE = new Font("Segoe UI", Font.PLAIN, 20);
		//on instancie le container
		this.containerMenu = new JPanel();
		//on d�ifnit sa taille, qui prend tout l'�cran
		this.containerMenu.setBounds(0,0,frame.getWidth(),frame.getHeight());
		//on mets les enfants en position absolute
		this.containerMenu.setLayout(null);
	    
	    //on d�finit le fond graphique du menu
		JLabel background = new JLabel(new ImageIcon(getClass().getResource("/menuFond.jpg")));
		background.setBounds(0,0,frame.getWidth(),frame.getHeight());//on le positionne
	    background.setLayout( null );//position absolute
	    
	    //on affiche les auteurs du programme
	    JLabel labelAuteur = new JLabel("Par Robin Leduc & Antoine-Valentin Charpentier");
		labelAuteur.setBounds(20, 600, 475, 40);
		labelAuteur.setFont(POLICE_SEGOE);
		
		//on ajoute le bouton jouer
		JButton btnJouer = new JButton("JOUER");
		btnJouer.setBounds(125, 375, 218, 60);
		
		//on ajoute le bouton quitter
		JButton btnQuitter = new JButton("QUITTER");
		btnQuitter.setBounds(125, 475, 218, 60);
		
		//on ajoute les diff�rents composants instancier pr�c�dement au fond pour pouvoir les superposer
		background.add(labelAuteur);
		background.add(btnJouer);
		background.add(btnQuitter);
		
		//on ajoute tout les composants superpos� au container du menu
		this.containerMenu.add(background);

		//on ajoute les controleur permettant de d�tecter des clic sur les diff�rents boutons
		new ControleurClicEntier<JButton>(0, btnQuitter);
		new ControleurClicEntier<JButton>(1, btnJouer);
	}
	
	/**
	 * M�thode qui permet de r�cup�rer tous les nouveaux messages qui se sont rajout�s dans l'attribut listeMessagesAAfficher de la classe Partie sous la forme d'une cha�ne de caract�res.
	 * @return Une cha�ne de caract�res comportant tous les nouveaux messages les uns � la suite des autres. Il y a un retour � la ligne entre chaque message.
	 */
	public String conversionListeDesMessagesEnChaineDeCaracteres() {
		//on saute une ligne d�s la premi�re ligne
		String res = "\n";
		//tant qu'il y a un nouveau message
		while(Partie.getPartie().getListeMessagesAAfficher().size()>this.indiceMessageDansListe) {
			//on rajoute le nouveau message � la suite de l'ancien, avec un retour � la ligne apr�s
    		res+="    "+Partie.getPartie().getListeMessagesAAfficher().get(this.indiceMessageDansListe)+"\n";
    		//on incr�mente l'indice du message regard�
    		this.indiceMessageDansListe++;
    	}
		//on retourne la chaine de caract�res comportant tous les messages
		return res;
	}
	
	/**
	 * Retourne une chaine de caract�res qui contient l'ensemble des informations relative � une manche.
	 * C'est-�-dire :
	 * <ul>
	 * <li>Le joueur qui doit jouer</li>
	 * <li>Le joueur qui a d�but� le tour</li>
	 * <li>Les joueurs encore en vie durant la manche</li>
	 * <li>Le score de chaque joueurs</li>
	 * <li>le r�le de la personne qui doit joueur</li>
	 * </ul>
	 * @return
	 */
	public String conversionListeDesInformationsMancheEnChaineDeCaracteres() {
		String res = "\n";
		//on affiche le nom du joueur qui doit joueur actuellement
		res+="    Le joueur qui doit jouer:\n";
		res+="         - "+Partie.getPartie().getJoueurQuiDoitJouer().getNomJoueur()+"\n";
		//on affiche le nom du joueur qui a d�but� le tour (accuser ou utiliser l'effet chasseur d'une de ces cartes rumeurs)
		res+="    Le joueur qui a d�but� le tour:\n";
		res+="         - "+Partie.getPartie().getJoueurDebutTour().getNomJoueur()+"\n";
		//on affiche l'ensemble des joueurs non �limin� de la manche
		//c'est a dire tout les joueurs sauf les joueurs r�v�l� en tant que sorci�re
		res+="    Joueurs encore en vie:\n";
		Iterator<Joueur> itJoueurEnVie = Partie.getPartie().getListeJoueurNonEliminee(null).iterator();
		while(itJoueurEnVie.hasNext()) {
			//on affiche le nom
			Joueur joueurIteration = itJoueurEnVie.next();
			res+="         - "+joueurIteration.getNomJoueur();
			//si c'est une IA ou non
			if(joueurIteration.getEstJoueurVirtuel()) {
				res+=" (IA)";
			}
			//s'il est r�v�l� villageois 
			if(joueurIteration.getEstRevele()) {
				res+=" ["+joueurIteration.getCarteIdentite().getRole()+"]";
			}
			//retour � la ligne
			res+="\n";
		}
		res+="    Score de chaque joueurs:\n";
		//on affiche le score de chaque joueur de la partie
		Iterator<Joueur> itJoueur = Partie.getPartie().getListeJoueurs().iterator();
		while(itJoueur.hasNext()) {
			Joueur joueurIteration = itJoueur.next();
			res+="         - "+joueurIteration.getNomJoueur();
			res+=" => "+joueurIteration.getScore()+"pts\n";
		}
		//on affiche le score de la personne qui doit joueur
		//on regarde si le joueur appartient � la partie, et si ce n'est donc pas un joueur temporaire
		if(Partie.getPartie().getListeJoueurs().contains(Partie.getPartie().getJoueurQuiDoitJouer())) {
			//on regarde maintenant si ce joueur n'est pas un joueur virtuel
			//on ne va pas afficher le r�le d'un jouur virtuel mais uniquement ceux des joueurs r�els
			if(!Partie.getPartie().getJoueurQuiDoitJouer().getEstJoueurVirtuel()) {
				//on n'affiche pas le r�le tant que la premi�re manche n'a pas commenc�
				res+="    Votre r�le:\n";
				res+="         - "+Partie.getPartie().getJoueurQuiDoitJouer().getCarteIdentite().getRole();
			}
			
		}
		
		return res;
	}
	
	/**
	 * M�thode qui permet de cr�e le container contenant l'ensemble des composants affich� lors de la partie.
	 * Ce container se divise en deux sous-containers : panelInformations, panelContenuSelection.
	 * panelInformations d�signe le JPanel qui contient l'ensemble des informations d'une manche, c'est-�-dire la partie sup�rieure de l'�cran encadr� avec du orange : boite de dialogue, num�ro de manche, informations sur la partie.
	 * panelContenuSelection d�signe l'emplacement sur la fen�tre o� le joueur pourra cliquer sur des composants pour effectuer les diff�rents choix.
	 */
	public void creationContainerJeu() {
		//on intialise les polices d'�criture
		Font POLICE_SEGOE = new Font("Segoe UI", Font.BOLD, 30);
		Font POLICE_NYALA = new Font("Nyala", Font.PLAIN, 22);
		Font POLICE_CALIBRI = new Font("Calibri", Font.PLAIN, 13);
		
		//on instancie le panel le plus g�n�ral
		this.containerJeu = new JPanel();
		//on d�finit sa taille sur l'�crean
		this.containerJeu.setBounds(0,0,frame.getWidth(),frame.getHeight());
		//on positionne les enfants en position absolute
		this.containerJeu.setLayout(null);
	    
		//on d�finit les diff�rentes couleur 
	    Color COULEUR_ORANGE = new Color(255,106,0);
	    Color COULEUR_BLANCHE = new Color(255,255,255);
	    Color COULEUR_GRISE = new Color(64,64,64);
	    Color COULEUR_GRISE_CLAIRE = new Color(204,204,204);
	    
		//PANEL AVEC TOUTE LES INFORMATIONS DE LA PARTIE
	    
		JPanel panelInformations = new JPanel();
		panelInformations.setBounds(0,0,frame.getWidth(),340);
		panelInformations.setLayout(null);
		panelInformations.setBackground(COULEUR_ORANGE);
		
		//nom du jeu
		JLabel labelNomJeu = new JLabel("WITCH  HUNT");
		labelNomJeu.setBounds(0, 0, 1000, 50);
		labelNomJeu.setFont(POLICE_SEGOE);
		labelNomJeu.setBackground(COULEUR_BLANCHE);
		labelNomJeu.setHorizontalAlignment(JLabel.CENTER);
		labelNomJeu.setOpaque(true);
		
		//num�ro de manche
		this.labelNumeroManche = new JLabel("Initialisation de la partie");
		this.labelNumeroManche.setBounds(0, 50+10, 1000, 50);
		this.labelNumeroManche.setFont(POLICE_NYALA);
		this.labelNumeroManche.setBackground(COULEUR_GRISE);
		this.labelNumeroManche.setForeground(COULEUR_BLANCHE);
		this.labelNumeroManche.setHorizontalAlignment(JLabel.CENTER);
		this.labelNumeroManche.setOpaque(true);
		
		//Ajout de la boite de dialogue
		this.textAreaBoiteDialogue = new JTextArea();
		this.textAreaBoiteDialogue.setEditable(false);//on dit que l'utilisateur ne peut pas modifier son contenu
		this.textAreaBoiteDialogue.setLineWrap(true);//on indique que l'on veut un retour � la ligne automatiquement
		this.textAreaBoiteDialogue.setBackground(COULEUR_BLANCHE);//on lui attribut un fond blanc
		this.textAreaBoiteDialogue.setWrapStyleWord(true);//�viter d'avoir des mots coup�
		this.textAreaBoiteDialogue.setText(this.conversionListeDesMessagesEnChaineDeCaracteres());//on lui attribut son texte
		this.textAreaBoiteDialogue.setFont(POLICE_CALIBRI);//on modifie la police d'�criture
		this.textAreaBoiteDialogue.setBounds(0,120,1000,210);//on le positionne sur la carte et on d�finit sa taille
		this.textAreaBoiteDialogue.setHighlighter(null);
		
		//ajout informations de la manche
		this.textAreaInformationsManche = new JTextArea();
		this.textAreaInformationsManche.setEditable(false);//on dit que l'utilisateur ne peut pas modifier son contenu
		this.textAreaInformationsManche.setLineWrap(true);//on indique que l'on veut un retour � la ligne automatiquement
		this.textAreaInformationsManche.setBackground(COULEUR_GRISE_CLAIRE);//on lui attribut un fond gris clair
		this.textAreaInformationsManche.setWrapStyleWord(true);//�viter d'avoir des mots coup�
		this.textAreaInformationsManche.setText("");//on lui attribut son texte vide par d�faut, que l'on va remplir une fois que la partie est commenc�
		this.textAreaInformationsManche.setFont(POLICE_CALIBRI);//on modifie la police d'�criture
		this.textAreaInformationsManche.setBounds(1010,10,246,210+50+10+50);//on le positionne sur la carte et on d�finit sa taille
		this.textAreaInformationsManche.setHighlighter(null);
		
		//intialisation du panel qui va contenur tout les composants pours les diverses s�lections
		this.panelContenuSelection = new JPanel();
		this.panelContenuSelection.setBackground(COULEUR_BLANCHE);
		this.panelContenuSelection.setLayout(null);
		this.panelContenuSelection.setBounds(0,340,frame.getWidth(),720-340);
				
		//ajout des diff�rents composants au panel informations
		panelInformations.add(textAreaInformationsManche);
		panelInformations.add(this.textAreaBoiteDialogue);
		panelInformations.add(this.labelNumeroManche);
		panelInformations.add(labelNomJeu);
		
		//ajout des composants au JPanel de la fenetre
		this.containerJeu.add(panelInformations);
		this.containerJeu.add(panelContenuSelection);
	}
	
	/**
	 * M�thode qui permet d'afficher une carte rumeur sous la forme graphique. Cette carte sera affich� sur l'�cran � des coordonn�e renseign� par l'utilisateur.
	 * @param x Placement de la carte rumeur sur l'axe des abscisses 
	 * @param y Placement de la carte rumeur sur l'axe des ordonn�es. Remarque : le 0 se situe en haut de l'�cran.
	 * @param carteRumeur La carte rumeur que l'on souhaite affich� � l'�cran. 
	 */
	public void afficherCarteRumeur(int x, int y, CarteRumeur carteRumeur) {
		//on d�finit les diff�rentes polices utilis� pour afficher la carte rumeur
		Font POLICE_TAHOMA = new Font("Tahoma", Font.PLAIN, 22);
		Font POLICE_CALIBRI = new Font("Calibri", Font.PLAIN, 13);
		Font POLICE_CALIBRI_PETITE = new Font("Calibri", Font.PLAIN, 9);
		Font POLICE_CAMBRIA = new Font("Cambria", Font.PLAIN, 10);
		//on mets les instructions pour calculer les dimenssions d'une chaine de caract�res en pixel sous certaine police d'�criture
		AffineTransform affinetransform = new AffineTransform();     
	    FontRenderContext frc = new FontRenderContext(affinetransform,true,true);  
	    
	    //on d�finit les couleurs de texte utilis�
	    Color COULEUR_BLEU = new Color(0, 0, 125);
	    Color COULEUR_ORANGE = new Color(251,151,50);
	    Color COULEUR_VERT = new Color(51,153,51);
	    Color COULEUR_BLANCHE = new Color(255,255,255);
	    
	    //ON DEFINIT UN JPANEL, qui va nous permettre de d�tecter un clic dessus et de permettre de stocker tout les composants utilis� pour afficher la carte rumeur
		JPanel panel = new JPanel();
		panel.setBounds(x, y, 198, 284);//on d�finit l'emplacement du composant ainsi que �a taille 
		panel.setLayout(null); //on le mets en position absolute
		
		//on d�finit le fond graphique de la carte rumeur
		JLabel background = new JLabel(new ImageIcon(getClass().getResource("/carteRumeurFond.png")));
		background.setBounds(0, 0, 198, 284);//on le positionne
	    background.setLayout( null );//position absolute
	    
	    //NOM de la carte rumeur
	    String nomCarte = carteRumeur.getNomCarte();//on r�cup�re le nom de la carte
	    JLabel labelNomCarteRumeur = new JLabel(nomCarte);//on cr�e le JLabel qui va permettre de l'afficher
	    labelNomCarteRumeur.setBounds((198-(int)(POLICE_TAHOMA.getStringBounds(nomCarte, frc).getWidth()))/2, 42, (int)(POLICE_TAHOMA.getStringBounds(nomCarte, frc).getWidth())+5, (int)(POLICE_TAHOMA.getStringBounds(nomCarte, frc).getHeight())+1); //on le positionne en le centrant sur la carte horizontealement
	    labelNomCarteRumeur.setForeground(COULEUR_BLEU);//change la couleur du texte
	    labelNomCarteRumeur.setFont(POLICE_TAHOMA);//change la police d'�criture et la taille
	    
	    //PROTECTION CONTRE AUTRE CARTE
	    if(carteRumeur.getProtectionContreAutreCarte()!="") {
	    	//si la carte prot�ge le joueur contre les effets d'une autre carte rumeur alors on l'affiche
	    	//on d�place le nom de la carte pour pouvoir laisser de la place sur la carte pour afficher le nom de la carte contre qui les effets ne peuvent pas �tre appliqu�
	    	labelNomCarteRumeur.setBounds((198-(int)(POLICE_TAHOMA.getStringBounds(nomCarte, frc).getWidth()))/2, 22, (int)(POLICE_TAHOMA.getStringBounds(nomCarte, frc).getWidth())+1, (int)(POLICE_TAHOMA.getStringBounds(nomCarte, frc).getHeight())+1);
	    	//on g�n�re le texte qui sera affich�
	    	String protectionDescription = "Une fois r�v�l�e, vous ne pouvez pas �tre choisi par "+carteRumeur.getProtectionContreAutreCarte();
	    	//on cr�e un text area qui va contenir le texte. Cela permettra d'avoir un retour � la ligne automatiquement
	    	JTextArea textAreaProtection = new JTextArea();
	    	textAreaProtection.setEditable(false);//on dit que l'utilisateur ne peut pas modifier son contenu
	    	textAreaProtection.setLineWrap(true);//on indique que l'on veut un retour � la ligne automatiquement
	    	textAreaProtection.setOpaque(false);//on retire le fond du textearea
	    	textAreaProtection.setWrapStyleWord(true);//�viter d'avoir des mots coup�
	    	textAreaProtection.setText(protectionDescription);//on lui attribut son texte
	    	textAreaProtection.setFont(POLICE_CALIBRI);//on modifie la police d'�criture
	    	textAreaProtection.setBounds(5,47,198-10,35);//on le positionne sur la carte et on d�finit sa taille
	    	textAreaProtection.setHighlighter(null);
	    	
	    	 //on utilise le controleur suivant pour d�tecter un clic sur l'affichage de la carte
		    if(!Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE)) {
		    	//il ne doit pas pouvoir cliquer sur les cartes quand on lui montre ces cartes lors de la distribution
		    	new ControleurClicCarteRumeur<JTextArea>(carteRumeur,textAreaProtection);
		    }
	    	background.add(textAreaProtection);//on l'ajoute au font de la carte poru �tre superpos�
	    }
	    
	    //CONDITION JOUABILITE effet sorci�re : fond orange
	    if(!carteRumeur.getConditionJouabiliteEffetsSorciere().equals(EnumJouabiliteEffetCarteRumeur.AUCUNE)) {
	    	//si la carte rumeur � une condition de jouabilit� sur l'effet sorci�re, alors on l'affiche
	    	//on g�n�re le texte qui sera affich�
	    	String conditionJouabiliteDescription = "";
	    	if(carteRumeur.getConditionJouabiliteEffetsSorciere().equals(EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_CARTE_RUMEUR_REVELEE)) {
	    		conditionJouabiliteDescription = "Jouable si carte rumeur r�v�l�e";
	    	}else if(carteRumeur.getConditionJouabiliteEffetsSorciere().equals(EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS)) {
	    		conditionJouabiliteDescription = "Jouable si vous �tes r�v�l� Villageois";
	    	}else {
	    		conditionJouabiliteDescription = "Jouable si carte rumeur et r�v�l� villageois";
	    	}
	    	//on cr�e un text area qui va contenir le texte. Cela permettra d'avoir un retour � la ligne automatiquement
	    	JTextArea textAreaConditionJouabilite = new JTextArea();
	    	textAreaConditionJouabilite.setEditable(false);//on dit que l'utilisateur ne peut pas modifier son contenu
	    	textAreaConditionJouabilite.setLineWrap(true);//on indique que l'on veut un retour � la ligne automatiquement
	    	textAreaConditionJouabilite.setBackground(COULEUR_ORANGE);//on lui attribut un fond orange
	    	textAreaConditionJouabilite.setWrapStyleWord(true);//�viter d'avoir des mots coup�
	    	textAreaConditionJouabilite.setText(conditionJouabiliteDescription);//on lui attribut son texte
	    	textAreaConditionJouabilite.setFont(POLICE_CAMBRIA);//on modifie la police d'�criture
	    	textAreaConditionJouabilite.setForeground(COULEUR_BLANCHE);
	    	textAreaConditionJouabilite.setBounds(90,80,198-90-10,30);//on le positionne sur la carte et on d�finit sa taille
	    	textAreaConditionJouabilite.setHighlighter(null);
	    	
	    	 //on utilise le controleur suivant pour d�tecter un clic sur l'affichage de la carte
		    if(!Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE)) {
		    	//il ne doit pas pouvoir cliquer sur les cartes quand on lui montre ces cartes lors de la distribution
		    	new ControleurClicCarteRumeur<JTextArea>(carteRumeur,textAreaConditionJouabilite);
		    }
		    
	    	background.add(textAreaConditionJouabilite);//on l'ajoute au font de la carte poru �tre superpos�
	    }
	    
	    //CONDITION JOUABILITE effet chasseur : fond vert
	    if(!carteRumeur.getConditionJouabiliteEffetsChasseur().equals(EnumJouabiliteEffetCarteRumeur.AUCUNE)) {
	    	//si la carte rumeur � une condition de jouabilit� sur l'effet chassseur, alors on l'affiche
	    	//on g�n�re le texte qui sera affich�
	    	String conditionJouabiliteDescription = "";
	    	if(carteRumeur.getConditionJouabiliteEffetsChasseur().equals(EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_CARTE_RUMEUR_REVELEE)) {
	    		conditionJouabiliteDescription = "Jouable si carte rumeur r�v�l�e";
	    	}else if(carteRumeur.getConditionJouabiliteEffetsChasseur().equals(EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS)) {
	    		conditionJouabiliteDescription = "Jouable si vous �tes r�v�l� Villageois";
	    	}else {
	    		conditionJouabiliteDescription = "Jouable si carte rumeur et r�v�l� villageois";
	    	}
	    	//on cr�e un text area qui va contenir le texte. Cela permettra d'avoir un retour � la ligne automatiquement
	    	JTextArea textAreaConditionJouabilite = new JTextArea();
	    	textAreaConditionJouabilite.setEditable(false);//on dit que l'utilisateur ne peut pas modifier son contenu
	    	textAreaConditionJouabilite.setLineWrap(true);//on indique que l'on veut un retour � la ligne automatiquement
	    	textAreaConditionJouabilite.setBackground(COULEUR_VERT);//on lui attribut un fond vert
	    	textAreaConditionJouabilite.setWrapStyleWord(true);//�viter d'avoir des mots coup�
	    	textAreaConditionJouabilite.setText(conditionJouabiliteDescription);//on lui attribut son texte
	    	textAreaConditionJouabilite.setFont(POLICE_CAMBRIA);//on modifie la police d'�criture
	    	textAreaConditionJouabilite.setForeground(COULEUR_BLANCHE);
	    	textAreaConditionJouabilite.setBounds(90,182,198-90-10,30);//on le positionne sur la carte et on d�finit sa taille
	    	textAreaConditionJouabilite.setHighlighter(null);
	    	
	    	 //on utilise le controleur suivant pour d�tecter un clic sur l'affichage de la carte
		    if(!Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE)) {
		    	//il ne doit pas pouvoir cliquer sur les cartes quand on lui montre ces cartes lors de la distribution
		    	new ControleurClicCarteRumeur<JTextArea>(carteRumeur,textAreaConditionJouabilite);
		    }
	    	background.add(textAreaConditionJouabilite);//on l'ajoute au font de la carte poru �tre superpos�
	    	
	    }
	    
	    //EFFET SORCIERE
	    //on r�cup�re l'enssemble des descriptions des effets sorci�re que l'on stocke dans une chaine de caract�res. 
	    //chaque effets sont les uns en dessous des autres
	    String effetsSorciereDescriptions = "";
	    //on parcours tout les effets sorci�re de la carte rumeur pass� en param�tre
	    Iterator<Effet> itSorciere = carteRumeur.getListeEffetsSorciere().iterator();
	    while(itSorciere.hasNext()) {
	    	//on ajoute a la chaine de caract�re la description de l'effet
	    	effetsSorciereDescriptions+=" - "+itSorciere.next().getDescriptionEffet();
	    	//s'il y a encore un effet, on rajoute alors un retour � la ligne forc�
	    	if(itSorciere.hasNext()) {
	    		effetsSorciereDescriptions+="\n";
	    	}
	    }
	    //on cr�e un text area qui va contenir le texte. Cela permettra d'avoir un retour � la ligne automatiquement
    	JTextArea textAreaEffetsSorciere = new JTextArea();
    	textAreaEffetsSorciere.setEditable(false);//on dit que l'utilisateur ne peut pas modifier son contenu
    	textAreaEffetsSorciere.setLineWrap(true);//on indique que l'on veut un retour � la ligne automatiquement
    	textAreaEffetsSorciere.setOpaque(false);//on retire le fond
    	textAreaEffetsSorciere.setWrapStyleWord(true);//�viter d'avoir des mots coup�
    	textAreaEffetsSorciere.setText(effetsSorciereDescriptions);//on lui attribut son texte
    	if(effetsSorciereDescriptions.length() > 130){//on regarde s'il faut r�duire la taille
    		textAreaEffetsSorciere.setFont(POLICE_CALIBRI_PETITE);//on modifie la police d'�criture
    	}else {
    		textAreaEffetsSorciere.setFont(POLICE_CALIBRI);//on modifie la police d'�criture
    	}
    	textAreaEffetsSorciere.setBounds(5,110,198-10,65);//on le positionne sur la carte et on d�finit sa taille
    	textAreaEffetsSorciere.setHighlighter(null);
    	
    	
    	//EFFET CHASSEUR
	    //on r�cup�re l'enssemble des descriptions des effets chasseurs que l'on stocke dans une chaine de caract�res. 
	    //chaque effets sont les uns en dessous des autres
	    String effetsChasseurDescriptions = "";
	    //on parcours tout les effets chasseur de la carte rumeur pass� en param�tre
	    Iterator<Effet> itChasseur = carteRumeur.getListeEffetsChasseur().iterator();
	    while(itChasseur.hasNext()) {
	    	//on ajoute a la chaine de caract�re la description de l'effet
	    	effetsChasseurDescriptions+=" - "+itChasseur.next().getDescriptionEffet();
	    	//s'il y a encore un effet, on rajoute alors un retour � la ligne forc�
	    	if(itChasseur.hasNext()) {
	    		effetsChasseurDescriptions+="\n";
	    	}
	    }
	    //on cr�e un text area qui va contenir le texte. Cela permettra d'avoir un retour � la ligne automatiquement
    	JTextArea textAreaEffetsChasseur = new JTextArea();
    	textAreaEffetsChasseur.setEditable(false);//on dit que l'utilisateur ne peut pas modifier son contenu
    	textAreaEffetsChasseur.setLineWrap(true);//on indique que l'on veut un retour � la ligne automatiquement
    	textAreaEffetsChasseur.setOpaque(false);//on retire le fond
    	textAreaEffetsChasseur.setWrapStyleWord(true);//�viter d'avoir des mots coup�
    	textAreaEffetsChasseur.setText(effetsChasseurDescriptions);//on lui attribut son texte
    	if(effetsChasseurDescriptions.length() > 130){//on regarde s'il faut r�duire la taille
    		textAreaEffetsChasseur.setFont(POLICE_CALIBRI_PETITE);//on modifie la police d'�criture
    	}else {
    		textAreaEffetsChasseur.setFont(POLICE_CALIBRI);//on modifie la police d'�criture
    	}
    	textAreaEffetsChasseur.setBounds(5,212,198-10,65);//on le positionne sur la carte et on d�finit sa taille
    	textAreaEffetsChasseur.setHighlighter(null);
	    
	    //on ajoute tout les composants de la carte au fond, pour qu'ils soient superpos�
	    background.add(labelNomCarteRumeur);
	    background.add(textAreaEffetsSorciere);
	    background.add(textAreaEffetsChasseur);
	    
	    //on ajoute le fond au Jpanel pour d�tecter un clic par la suite sur ce dernier
	    panel.add(background);
		
	    //on affiche � l'�cran le contenu du JPanel, soit celui de la carte rumeur
	    this.panelContenuSelection.add(panel);
		
	    //on utilise le controleur suivant pour d�tecter un clic sur l'affichage de la carte
	    if(!Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE)) {
	    	//il ne doit pas pouvoir cliquer sur les cartes quand on lui montre ces cartes lors de la distribution
	    	new ControleurClicCarteRumeur<JPanel>(carteRumeur,panel);
	    	
		    new ControleurClicCarteRumeur<JTextArea>(carteRumeur,textAreaEffetsChasseur);
		    new ControleurClicCarteRumeur<JTextArea>(carteRumeur,textAreaEffetsSorciere);
	    }
		
	}
}
