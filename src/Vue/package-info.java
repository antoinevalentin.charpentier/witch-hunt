/**
 * Package qui regroupe les diff�rentes Vues du jeu.
 * Deux vues ont �t� impl�ment�es :
 * <ul>
 * <li>VueTexte : Tous les messages sont affich�s dans un terminale, et les saisies sont effectu�e dans le terminal �galement � l'aide du clavier.</li>
 * <li>VueGraphique : tous les messages, les s�lections,.. sont effectu�es par l'interm�diaire d'une interface graphique.</li>
 * </ul>
 * Remarque : ces deux vues fonctionnent en m�me temps. Si on r�alise une saisie sur l'une, alors cela indiquera la s�lection sur la deuxi�me.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
package Vue;