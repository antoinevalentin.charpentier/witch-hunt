package Modele.effet;

import java.util.ArrayList;
import java.util.Arrays;

import Modele.CarteRumeur;
import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond � l'effet : "Prenez une carte de la main du joueur qui vous a accus�"
 * Cet effet permet au joueur qui utilise l'effet de r�cup�rer une carte rumeur pr�cise du joueur qui l'a accus� d'�tre une sorci�re. C'est le joueur qui utilise l'effet qui choisit la carte, la carte n'est pas choisie de mani�re al�atoire.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class PrendreCarteJoueurAccusateur extends Effet{
	/**
	 * Constructeur de la classe PrendreCarteJoueurAccusateur, il permet de modifier la description de l'effet pr�sent dans la classe parent.
	 */
	public PrendreCarteJoueurAccusateur() {
		super.setDescriptionEffet("Prenez une carte de la main du joueur qui vous a accus�");
	}
	
	/**
	 * Impl�mentation de l'effet "Take one card from the hand of the player who accused you"
	 * Cet effet permet au joueur qui utilise l'effet de r�cup�rer une carte rumeur pr�cise du joueur qui l'a accus� d'�tre une sorci�re. C'est le joueur qui utilise l'effet qui choisit la carte, la carte n'est pas choisie de mani�re al�atoire.
	 * M�thode qui permet d'activer la m�canique de cet effet.
	 * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas o� le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet.
	 */
	public void activerEffet(Joueur joueurQuiUtiliseEffet) {
		//impl�mentation de l'effet Take one card from the hand of the player who accused you
		//on affiche qu'elle effet vient d'�tre lanc�
		Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
		Partie.getPartie().ajouterUnMessage(2,"Ainsi, "+joueurQuiUtiliseEffet.getNomJoueur()+" vous allez r�cup�rer une carte dans votre main provenant du joueur qui vous a accus�.");
		//on r�cup�re l'ensemble des cartes de la main de l'accusateur qui ne sont pas r�v�l�
		ArrayList<CarteRumeur> listeCarteRumeurAccusateurNonRevele = Partie.getPartie().getJoueurDebutTour().listeCarteRumeurNonReveleEnMain();
		//on regarde si l'accusateur poss�de des carte rumeur non r�v�l� dont le joueur qui utilise l'effet peut r�cup�rer
		if(listeCarteRumeurAccusateurNonRevele.size() != 0) {
			//on demande au joueur qui utilise l'effet de chosiir parmi l'une des cartes
			
			//on d�finit la liste des cartes rumeurs qu'il peut choisir entre
			Partie.getPartie().setSelectionCarteRumeurParmiListe(listeCarteRumeurAccusateurNonRevele);
			//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
			Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_RECUPERER);
			//on r�cup�re la carte rumeur du choix r�alis�, correspond � la carte rumeur que le joueur veut r�cup�rer
			CarteRumeur carteRecup = Partie.getPartie().getCarteRumeurSelectionne();
			
			//on la mets dans la main du joueur qui a utiliser l'effet
			joueurQuiUtiliseEffet.ajouterCarteRumeurEnMain(carteRecup);
			//on la retire de la main de son accusateur
			Partie.getPartie().getJoueurDebutTour().getListeCartesRumeurEnMain().remove(carteRecup);
			//on affiche la carte qu'il vient de r�cup�rer
			if(!joueurQuiUtiliseEffet.getEstJoueurVirtuel()){
				Partie.getPartie().ajouterUnMessage(2,joueurQuiUtiliseEffet.getNomJoueur()+", tu viens de remporter la carte rumeur suivante :");
				Partie.getPartie().ajouterUnMessage(1,carteRecup);
				Partie.getPartie().setSelectionCarteRumeurParmiListe(new ArrayList<>(Arrays.asList(carteRecup)));
				Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE);
				Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
			}else {
				Partie.getPartie().ajouterUnMessage(2,joueurQuiUtiliseEffet.getNomJoueur()+", tu viens de remporter une carte rumeur du joueur qui t'a accus�");
				Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
				Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
			}
			
		}else {
			Partie.getPartie().ajouterUnMessage(2,joueurQuiUtiliseEffet.getNomJoueur()+", vous ne pouvez pas r�cup�rer de carte rumeur de "+Partie.getPartie().getJoueurDebutTour().getNomJoueur());
			Partie.getPartie().ajouterUnMessage(2,"Car il n'a pas de carte rumeur non r�v�l�e.");
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
		}
		
	}

}

