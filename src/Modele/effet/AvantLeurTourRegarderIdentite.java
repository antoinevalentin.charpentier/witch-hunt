package Modele.effet;

import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumRole;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond � l'effet : "Avant leur tour, regardez secr�tement leur identit�" = "Before their turn, secretly look at their indentity".
 * Effet qui permet de conna�tre l'identit� du joueur qui va d�buter le tour suivant, sans le dire aux autres.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class AvantLeurTourRegarderIdentite extends Effet{
	
	/**
	 * Constructeur de la classe AvantLeurTourRegarderIdentite, il permet de modifier la description de l'effet pr�sent dans la classe parent.
	 */
	public AvantLeurTourRegarderIdentite() {
		super.setDescriptionEffet("Avant leur tour, regardez secr�tement leur identit�");
	}
	
	/**
	 * Impl�mentation de l'effet "Before their turn, secretly look at their indentity"
	 * Cet effet consiste � regarder l'identit� de la personne suivante. (modifier via un effet pr�c�dement, il s'agit donc de la nouvelle personne en cours)
	 * M�thode qui permet d'activer la m�canique de cet effet.
	 * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas o� le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet.
	 */
	public void activerEffet(Joueur joueurQuiUtiliseEffet) {
		//impl�mentation de l'effet Before their turn, secretly look at their indentity
		//Cette effet consiste a regarder l'identit� de la personne suivaante (modifier via un effet pr�c�dement, il s'agit donc de la nouvelle personne en cours)
		//on affiche ce que fait l'effet
		Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
		Partie.getPartie().ajouterUnMessage(2,"Ainsi, "+joueurQuiUtiliseEffet.getNomJoueur()+" vous allez pouvoir regarder l'identit� de "+Partie.getPartie().getJoueurDebutTour().getNomJoueur()+".");
		//on affiche � joueurQuiUtiliseEffet le role du prochain joueur en cours, qui a choisi pr�c�demnt
		if(Partie.getPartie().getJoueurDebutTour().getCarteIdentite().getRole() == EnumRole.VILLAGEOIS) {
			Partie.getPartie().ajouterUnMessage(2,Partie.getPartie().getJoueurDebutTour().getNomJoueur()+ " est un villageois.");
		}else if(Partie.getPartie().getJoueurDebutTour().getCarteIdentite().getRole() == EnumRole.SORCIERE) {
			Partie.getPartie().ajouterUnMessage(2,Partie.getPartie().getJoueurDebutTour().getNomJoueur()+ " est une sorci�re.");
		}
		Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
		Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
	}

}

