package Modele.effet;

import java.util.ArrayList;
import java.util.Arrays;

import Modele.CarteRumeur;
import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond � l'effet : "Ajoutez une carte d�fauss�e � votre main, puis d�faussez la carte dont vous utilisez l'effet" = "Add one discarded card to your hand, and then discard this card".
 * Effet qui permet de r�cup�rer si possible une carte rumeur pr�sent dans la pile des carte rumeur d�fauss�e de la partie puis de d�fausser la carte rumeur dont cet effet appartient et qui vient d'�tre utilis�e � l'instant.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class AjouterCarteDefausserPuisDefausser extends Effet{
	
	/**
	 * Constructeur de la classe AjouterCarteDefausserPuisDefausser, il permet de modifier la description de l'effet pr�sent dans la classe parent.
	 */
	public AjouterCarteDefausserPuisDefausser() {
		super.setDescriptionEffet("Ajoutez une carte d�fauss�e � votre main, puis d�faussez la carte dont vous utilisez l'effet");
	}
	
	/**
	 * Impl�mentation de l'effet "Add one discarded card to your hand, and then discard this card"
	 * M�thode qui permet d'activer la m�canique de cet effet.
	 * Cet effet consiste � r�cup�rer si possible une carte d�fausse (choisie par le joueur qui utilise l'effet), puis d�fausser la carte qui utilise l'effet en cours
	 * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas o� le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet.
	 */
	public void activerEffet(Joueur joueurQuiUtiliseEffet) {
		//impl�mentation de l'effet Add one discarded card to your hand, and then discard this card
		//Cette effet consiste � �rcup�rer si possible une carte d�fausse (choisi par le joueur qui utilise l'effet), puis d�fausser la carte qui utilise l'effet en cours
		Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
		Partie.getPartie().ajouterUnMessage(2,"Ainsi, "+joueurQuiUtiliseEffet.getNomJoueur()+" vous allez pouvoir r�cup�rer une carte d�fauss�e.");
		//on regarde s'il reste au moins une carte rumeur d�fauss�
		if(Partie.getPartie().getListeCarteRumeurDefausser().size()>0) {
			//s'il y a au moins une carte rumeur
			//on demande au joueurQuiUtiliseEffet de choisir parmit l'une de ces cartes pour qu'il la r�cup�re
			
			//on d�finit la liste des cartes rumeurs qu'il peut choisir entre
			Partie.getPartie().setSelectionCarteRumeurParmiListe(Partie.getPartie().getListeCarteRumeurDefausser());
			//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
			Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_RECUPERER);
			//on r�cup�re la carte rumeur du choix r�alis�, correspond � la carte rumeur que le joueur veut r�cup�rer
			CarteRumeur carteRecup = Partie.getPartie().getCarteRumeurSelectionne();
			
			//on la recache des yeux des autres
			carteRecup.setFaceCache(true);
			//on la rajoute � la main du joueur qui utilise l'effet
			joueurQuiUtiliseEffet.ajouterCarteRumeurEnMain(carteRecup);
			//on la retire de la liste des carte d�fauss�
			Partie.getPartie().retirerCarteRumeurDefausser(carteRecup);
			//on l'affiche � l'utilisateur
			if(joueurQuiUtiliseEffet.getEstJoueurVirtuel() == false) {
				Partie.getPartie().ajouterUnMessage(2,joueurQuiUtiliseEffet.getNomJoueur()+", vous venez de r�cup�rer la carte suivante");
				Partie.getPartie().ajouterUnMessage(1,carteRecup);
				Partie.getPartie().setSelectionCarteRumeurParmiListe(new ArrayList<>(Arrays.asList(carteRecup)));
			}else {
				Partie.getPartie().ajouterUnMessage(2,joueurQuiUtiliseEffet.getNomJoueur()+", vient de r�cup�rer une carte rumeur d�fauss�e");
			}
			
		}else {
			Partie.getPartie().setSelectionCarteRumeurParmiListe(new ArrayList<>());
			Partie.getPartie().ajouterUnMessage(2,"Il n'y a pas de carte de d�fauss�, vous ne pouvez donc pas en r�cup�rer");
		}
		
		//dans tout les cas, m�me s'il n'a pas r�cup�rer de carte, on lui d�fausse la carte actuel
		//on cherche maintenant � d�fauss� la carte qui a utilis� cette effet.
		//on lui affiche quelle carte il va devoir d�fausser
	
		Partie.getPartie().ajouterUnMessage(1,"Vous venez de d�fausser la carte suivante (du fait de  l'effet)");
		Partie.getPartie().ajouterUnMessage(1,joueurQuiUtiliseEffet.getDerniereCarteRumeurJouee());
		Partie.getPartie().ajouterUnMessage(0,"Et, vous venez de d�fausser la carte qui a utilis� l'effet, c'est � dire la carte ayant pour nom : "+joueurQuiUtiliseEffet.getDerniereCarteRumeurJouee().getNomCarte());
		
		//on ajoute la carte qui a utilis�e l'effet � la liste des cartes d�fauss� de la partie
		Partie.getPartie().ajouterCarteRumeurDefausser(joueurQuiUtiliseEffet.getDerniereCarteRumeurJouee());
		//on la retire de la main du joueur
		joueurQuiUtiliseEffet.retirerCarteRumeurEnMain(joueurQuiUtiliseEffet.getDerniereCarteRumeurJouee());
		//on demande une confirmation pour passer � la suite du programme
		if(joueurQuiUtiliseEffet.getEstJoueurVirtuel()) {
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
		}else {
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
		}

	}

}

