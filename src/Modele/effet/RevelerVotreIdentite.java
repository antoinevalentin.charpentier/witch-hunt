package Modele.effet;

import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumRole;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond � l'effet : "R�v�lez votre identit� (Sorci�re�: le joueur � votre gauche prend le prochain tour, Villageois�: choisissez le joueur suivant)"
 * La personne qui utilise cet effet est oblig�e de r�v�ler son identit� (m�me s'il l'a d�j� fait). La personne qui va d�buter le prochain tour d�pend du r�le du joueur qui utilise l'effet.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class RevelerVotreIdentite extends Effet{
	/**
	 * Constructeur de la classe RevelerVotreIdentite, il permet de modifier la description de l'effet pr�sent dans la classe parent.
	 */
	public RevelerVotreIdentite() {
		super.setDescriptionEffet("R�v�lez votre identit� (Sorci�re�: le joueur � votre gauche prend le prochain tour, Villageois�: choisissez le joueur suivant)");
	}
	
	/**
	 * Impl�mentation de l'effet "Reveal your identity (Witch : Player to your left takes next turn, Villager : Choose next player)"
	 * La personne qui utilise cet effet est oblig�e de r�v�ler son identit� (m�me s'il l'a d�j� fait). La personne qui va d�buter le prochain tour d�pend du r�le du joueur qui utilise l'effet.
	 * M�thode qui permet d'activer la m�canique de cet effet.
	 * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas o� le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet.
	 */
	public void activerEffet(Joueur joueurQuiUtiliseEffet) {
		//impl�mentation de l'effet Reveal your identity (Witch : Player to your left takes next turn, Villager : Choose next player)
		
		//on affiche qu'elle effet vient d'�tre lanc�
		Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
		Partie.getPartie().ajouterUnMessage(2,"Ainsi, "+joueurQuiUtiliseEffet.getNomJoueur()+", vous allez devoir r�v�ler votre identit�.");
		
		//on mets a jour la valeur de son attribut
		joueurQuiUtiliseEffet.setEstRevele(true);
		//on affiche a tout le monde le r�le de la personne
		Partie.getPartie().ajouterUnMessage(2,"Je m'appelle "+joueurQuiUtiliseEffet.getNomJoueur()+" et je suis ...");
		
		
		//on r�cup�re le role du joueur qui doit r�v�ler son identit�
		EnumRole role = joueurQuiUtiliseEffet.getCarteIdentite().getRole();
		//on regarde si c'�tait un villageois : Il choisit le prochain joueur
		if(role == EnumRole.VILLAGEOIS) {
			//on affiche a tout le monde que c'�tais un villageois
			Partie.getPartie().ajouterUnMessage(2,"... Un villageois");
			//Il choisit alors quelle va �tre la personne suivante
			Partie.getPartie().ajouterUnMessage(2,joueurQuiUtiliseEffet.getNomJoueur()+", vous devez choisir le joueur suivant");
					
			//on d�finit la liste des joueur qu'il peut choisir entre
			Partie.getPartie().setSelectionJoueurParmiListe(Joueur.intersectionEntreListes(Partie.getPartie().getListeJoueurNonEliminee(null), joueurQuiUtiliseEffet.getListeJoueurContreQuiCarteRumeurJouable(joueurQuiUtiliseEffet.getDerniereCarteRumeurJouee())));
			//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
			Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_JOUEUR_SUIVANT);
			//on r�cup�re la valeur du choix r�alis�
			Joueur joueurSuivant = Partie.getPartie().getJoueurSelectionne();
			
			//on indique le choix � la partie
			Partie.getPartie().setJoueurDebutTour(joueurSuivant);
			
			Partie.getPartie().ajouterUnMessage(2, joueurQuiUtiliseEffet.getNomJoueur()+", vous avez choisi que "+Partie.getPartie().getJoueurDebutTour().getNomJoueur()+" va commencer le prochain tour.");
			
		//on regarde si c'�tais une sorci�re : il est �liminer d�finitivement et c'est au joueur de gauche de jouer
		}else if(role == EnumRole.SORCIERE){
			//on affiche a tout le monde que c'�tait une sorci�re
			Partie.getPartie().ajouterUnMessage(2,"... Une sorci�re");
			//on d�finit le joueur suivant, vu que c'est une sorci�re, c'est au tour du joueur de gauche (donc dans le sens horaire de la table) par rapport au joueur qui a utilis� l'effet
			//on doit donc d�placer le joueur accusateur au joueur qui a utilis� l'effet. Le joueur qui utilise l'effet est diff�rent de celui du joueur en cours dans le cas o� l'effet est d�clencher suite � la r�ponse a une accusation
			Partie.getPartie().setJoueurDebutTour(joueurQuiUtiliseEffet);
			//puis passer au joueur suivant dans la liste apr�s le joueur qui a utilis� l'effet
			Partie.getPartie().passerAuJoueurSuivant();
			Partie.getPartie().ajouterUnMessage(2, "Ainsi, "+Partie.getPartie().getJoueurDebutTour().getNomJoueur()+" sera la joueur qui va commencer le prochain tour.");
		}
		
		Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
		Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
	}

}

