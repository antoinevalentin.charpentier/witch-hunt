package Modele.effet;

import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond � l'effet : "D�faussez une carte rumeur de votre main (non r�v�l�e)"
 * Cet effet force le joueur qui utilise l'effet � d�fausser l'une de ces cartes rumeur, encore non utilis�e, ou du moins appartenant � la main du joueur. La carte qu'il doit d�fausser appartient donc � sa main.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class DefausserCarte extends Effet{
	/**
	 * Constructeur de la classe DefausserCarte, il permet de modifier la description de l'effet pr�sent dans la classe parent.
	 */
	public DefausserCarte() {
		super.setDescriptionEffet("D�faussez une carte rumeur de votre main (non r�v�l�e)");
	}
	
	/**
	 * Impl�mentation de l'effet "Discard a card from your hand"
	 * Cet effet force le joueur qui utilise l'effet � d�fausser l'une de ces cartes rumeur, encore non utilis�e, ou du moins appartenant � la main du joueur. La carte qu'il doit d�fausser appartient donc � sa main.
	 * M�thode qui permet d'activer la m�canique de cet effet.
	 * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas o� le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet.
	 */
	public void activerEffet(Joueur joueurQuiUtiliseEffet) {
		//impl�mentation de l'effet Discard a card from your hand
		//Cette effet consiste a retirer l'une des carte rumeur non r�v�l� pr�sent dans la main du joueur
		//on affiche qu'elle effet vient d'�tre lanc�
		Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
		Partie.getPartie().ajouterUnMessage(2,"Ainsi, "+joueurQuiUtiliseEffet.getNomJoueur()+" vous devez d�fausser une carte de votre main.");
		//on regarde si le joueur a au moins une carte rumeur qu'il peut se s�parer
		if(joueurQuiUtiliseEffet.listeCarteRumeurNonReveleEnMain().size() > 0) {
			joueurQuiUtiliseEffet.choisirCarteRumeurPuisDefausser(joueurQuiUtiliseEffet.listeCarteRumeurNonReveleEnMain());
		}else {
			//si le joueur ne peut pas se s�parer d'une carte rumeur
			Partie.getPartie().ajouterUnMessage(2,"Vous n'avez pas de carte rumeur qui peut �tre d�fauss�");
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
		}
		
	}

}

