package Modele.effet;

import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond � l'effet : "Choisissez le joueur suivant"
 * Cet effet permet au joueur qui l'utilise de choisir le joueur qui va d�buter le tour suivant. Il doit �tre diff�rent de lui-m�me.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class ChoisirJoueurSuivant extends Effet{
	
	/**
	 * Constructeur de la classe ChoisirJoueurSuivant, il permet de modifier la description de l'effet pr�sent dans la classe parent.
	 */
	public ChoisirJoueurSuivant() {
		super.setDescriptionEffet("Choisissez le joueur suivant");
	}
	
	/**
	 * Impl�mentation de l'effet "Choose next player"
	 * Cet effet permet au joueur qui l'utilise de choisir le joueur qui va d�buter le tour suivant. Il doit �tre diff�rent de lui-m�me.
	 * M�thode qui permet d'activer la m�canique de cet effet.
	 * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas o� le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet.
	 */
	public void activerEffet(Joueur joueurQuiUtiliseEffet) {
		//impl�mentation de l'effet Choose next player
		//on affiche qu'elle effet vient d'�tre lanc�
		Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
		Partie.getPartie().ajouterUnMessage(1,"Comme le nom de l'effet indique, vous devez saisir le nom d'un autre joueur(pas vous) �tant encore en vie.");
		Partie.getPartie().ajouterUnMessage(0,"Comme le nom de l'effet indique, vous devez s�lectionner un autre joueur(pas vous) �tant encore en vie.");
		
		//on demande au joueur qui utilise l'effet de choisir le prochain joueur qui va soit accuser une personne soit utiliser une carte rumeur effet sorci�re
		//on d�finit la liste des joueur qu'il peut choisir entre
		Partie.getPartie().setSelectionJoueurParmiListe(Joueur.intersectionEntreListes(Partie.getPartie().getListeJoueurNonEliminee(joueurQuiUtiliseEffet), joueurQuiUtiliseEffet.getListeJoueurContreQuiCarteRumeurJouable(joueurQuiUtiliseEffet.getDerniereCarteRumeurJouee())));
		//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
		Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_JOUEUR_SUIVANT);
		//on r�cup�re la valeur du choix r�alis�
		Joueur joueurSuivant = Partie.getPartie().getJoueurSelectionne();
		
		Partie.getPartie().ajouterUnMessage(2,"Vous avez choisi que le joueur "+joueurSuivant.getNomJoueur()+" va jouer le prochain tour.");
		//on modifie le prochain joueur qui va devoir d�buter le tour suivant
		Partie.getPartie().setJoueurDebutTour(joueurSuivant);
		Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
		Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
	}

}

