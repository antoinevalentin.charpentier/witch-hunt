package Modele.effet;

import java.util.ArrayList;

import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond � l'effet : "A leur tour, ils doivent accuser un autre joueur que vous, si possible" = "On their turn they must accuse a player other than you, if possible".
 * Comme son nom l'identique, la personne qui a utilis� l'effet a d� auparavant choisir une autre personne. Cette derni�re doit accuser une autre personne d'�tre une sorci�re.
 * Dans le cas o� cette autre personne ne peut pas accuser une autre personne que celle qui a utilis� l'effet, alors elle doit l'accuser car elle n'a pas d'autre choix.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class AccuserUneAutrePersonne extends Effet{
	
	/**
	 * Constructeur de la classe AccuserUneAutrePersonne, il permet de modifier la description de l'effet pr�sent dans la classe parent.
	 */
	public AccuserUneAutrePersonne() {
		super.setDescriptionEffet("A leur tour, ils doivent accuser un autre joueur que vous, si possible");
	}
	
	/**
	 * Impl�mentation de l'effet "On their turn they must accuse a player other than you, if possible"
	 * M�thode qui permet d'activer la m�canique de cet effet.
	 * Cet effet consiste � forcer le joueur suivant a accuser une autre personne.
	 * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas o� le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet. 
	 */
	public void activerEffet(Joueur joueurQuiUtiliseEffet) {
		//impl�mentation de l'effet On their turn they must accuse a player other than you, if possible
		//Cette effet consiste a forcer le joueur suivant a accuser une autre personne
		//on affiche qu'elle effet vient d'�tre lanc�
		Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
		Partie.getPartie().ajouterUnMessage(2,"Ainsi, "+joueurQuiUtiliseEffet.getNomJoueur()+" vous allez forcer "+Partie.getPartie().getJoueurDebutTour().getNomJoueur()+" d'accuser une autre personne.");
		//on r�cup�re le joueur qui va devoir accuser au prochain tour
		Joueur joueurSuivant = Partie.getPartie().getJoueurDebutTour();
		//on r�initialise le bool�en qui nous permet de savoir que le joueur suivant � �tait d�termin� lors du tour
		Partie.getPartie().setChangementJoueurAccusateurEffectue(false);
		
		Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
		Partie.getPartie().setJoueurSelectionne(joueurSuivant);
		//on affiche comme quoi c'est le d�but d'un tour
		Partie.getPartie().ajouterUnMessage(1,"'''''''''''''''''''''''' NOUVEAU TOUR | Accusateur : "+joueurSuivant.getNomJoueur()+" [Score = "+joueurSuivant.getScore()+"] ''''''''''''''''''''''''''''''''''");
		//on modifie le jouerqui doit jouer
		Partie.getPartie().setJoueurQuiDoitJouer(joueurSuivant);
		//on affiche comme quoi il est contraint d'accuser une autre personne
		Partie.getPartie().ajouterUnMessage(2,joueurSuivant.getNomJoueur()+", vous �tre contraint d'accuser un autre joueur d'�tre une sorci�re du fait de l'effet pr�c�demment utilis�.");
		//on r�cup�re l'ensemble des personne non r�v�l� sauf le joueur qui va devoir accuser
		ArrayList<Joueur> listeJoueurPossibleAccuser = Partie.getPartie().getListeJoueurAvecIdentiteNonRevele(joueurSuivant);
		//on regarde s'il y a assez de personne pour qu'il ne puisse pas accuser la personne qui a utilis� l'effet de la carte
		//le if correspond � la traduction du "other than you, if possible"
		if(listeJoueurPossibleAccuser.size() > 1) {
			//si la liste comporte plus de deux personne, 
			//alors on retire la personne qui a utilis� l'effet des choix possible du joueur accusateur
			listeJoueurPossibleAccuser.remove(joueurQuiUtiliseEffet);
			//on affiche comme quoi il ne peut pas accuser le joueur qui vient d'utiliser l'effet
			Partie.getPartie().ajouterUnMessage(2,"Vous ne pouvez pas accuser "+joueurQuiUtiliseEffet.getNomJoueur()+" du fait de l'effet pr�c�dant.");
		}
		//on lance l'accusation
		joueurSuivant.accuser(listeJoueurPossibleAccuser);
		//s'il n'y a pas de changement de joueur suivant suite � cette accusation
		//alors le joueur suivant sera d�termin� � la fin de l'ex�cution de cette effet (la fin du tour o� l'effet a �tait activ�), ce qui va marquer la fin du tour pr�c�dant ou le joueur a activ� l'effet
		//on remet la valeur par d�faut pour le joueur qui doit jouer
		//la meme qu'au d�but de cette m�thode
		Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
	}

}

