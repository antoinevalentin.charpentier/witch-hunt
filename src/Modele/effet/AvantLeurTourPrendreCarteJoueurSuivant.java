package Modele.effet;

import java.util.ArrayList;
import java.util.Arrays;

import Modele.CarteRumeur;
import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond � l'effet : "Avant leur tour, prenez une carte au hasard de leur main et ajoutez-la � votre main" = "Before their turn, take a random card from their hand and add it to your hand".
 * Effet qui permet de r�cup�rer si possible une carte rumeur non r�v�l�e appartenant � la main du joueur suivant.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class AvantLeurTourPrendreCarteJoueurSuivant extends Effet{
	
	/**
	 * Constructeur de la classe AvantLeurTourPrendreCarteJoueurSuivant, il permet de modifier la description de l'effet pr�sent dans la classe parent.
	 */
	public AvantLeurTourPrendreCarteJoueurSuivant() {
		super.setDescriptionEffet("Avant leur tour, prenez une carte au hasard de leur main et ajoutez-la � votre main");
	}
	
	/**
	 * Impl�mentation de l'effet "Before their turn, take a random card from their hand and add it to your hand"
	 * Cet effet consiste � r�cup�rer une carte rumeur non r�v�l� de mani�re al�atoire de la prochaine personne (la personne qu'il a choisie pr�c�demment qui est donc le nouveau joueur en cours),
	 * M�thode qui permet d'activer la m�canique de cet effet.
	 * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas o� le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet.
	 */
	public void activerEffet(Joueur joueurQuiUtiliseEffet) {
		//impl�mentation de l'effet Before their turn, take a random card from their hand and add it to your hand
		//Cette effet consiste a r�cup�rer une carte rumeur non r�v�l� de mani�re al�atoire de la prochaine personne (la personne qu'il a choisi pr�c�dement qui est docn le joueur en cours),
		Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
		Partie.getPartie().ajouterUnMessage(2,"Ainsi, "+joueurQuiUtiliseEffet.getNomJoueur()+" vous allez pouvoir r�cup�rer une carte rumeur de "+Partie.getPartie().getJoueurDebutTour().getNomJoueur()+" de mani�re al�atoire.");
		
		//on r�cup�re toute les cartes rumeurs non r�v�l� de la personne en cours
		ArrayList<CarteRumeur> listeCarteEnMainDuProchainJoueur = Partie.getPartie().getJoueurDebutTour().listeCarteRumeurNonReveleEnMain();
		//on regarde s'il poss�de une carte rumeur
		if(listeCarteEnMainDuProchainJoueur.size() != 0) {
			//on en choisit une au hasard
			CarteRumeur carteRumeurARecuperer = listeCarteEnMainDuProchainJoueur.get((int)(Math.random()*(listeCarteEnMainDuProchainJoueur.size())));
			//on la rajoute � la main du joueur qui a utilis� l'effet
			joueurQuiUtiliseEffet.ajouterCarteRumeurEnMain(carteRumeurARecuperer);
			//on la retire de la main de la personne suivante, a qui le joueur qui a utilis� la carte a prit la carte
			Partie.getPartie().getJoueurDebutTour().retirerCarteRumeurEnMain(carteRumeurARecuperer);
			//on affiche � la personne qui a r�cup�rer la carte qu'elle carte il vient de r�cup�rer.
			Partie.getPartie().ajouterUnMessage(2,joueurQuiUtiliseEffet.getNomJoueur()+", vous venez de r�cup�rer la carte suivante de "+Partie.getPartie().getJoueurDebutTour().getNomJoueur());
			Partie.getPartie().ajouterUnMessage(1,carteRumeurARecuperer);
			Partie.getPartie().setSelectionCarteRumeurParmiListe(new ArrayList<>(Arrays.asList(carteRumeurARecuperer)));
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
		}else {
			Partie.getPartie().ajouterUnMessage(2,joueurQuiUtiliseEffet.getNomJoueur()+", vous ne pouvez finalement pas r�cup�rer de carte de "+Partie.getPartie().getJoueurDebutTour().getNomJoueur());
			Partie.getPartie().ajouterUnMessage(2,"Car celui ne dispose pas de carte rumeur non r�v�l�.");
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
		}
		
	}


}
