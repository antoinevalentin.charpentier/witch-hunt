package Modele.effet;

import java.util.ArrayList;
import java.util.Arrays;

import Modele.CarteRumeur;
import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond � l'effet : "Le joueur qui vous a accus� se d�fausse d'une carte rumeur al�atoirement de sa main"
 * Cet effet force le joueur qui a accus� le joueur qui utilise la carte avec cet effet, � d�fausser l'une de ces cartes rumeur de mani�re al�atoire. Il ne choisit donc pas laquelle de ces cartes, il va devoir se s�parer. La carte rumeur appartient � la liste des cartes rumeurs pas r�v�l�es.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class DefausserCarteJoueurAccusateur extends Effet{
	/**
	 * Constructeur de la classe DefausserCarteJoueurAccusateur, il permet de modifier la description de l'effet pr�sent dans la classe parent.
	 */
	public DefausserCarteJoueurAccusateur() {
		super.setDescriptionEffet("Le joueur qui vous a accus� se d�fausse d'une carte rumeur al�atoirement de sa main");
	}
	
	/**
	 * Impl�mentation de l'effet "The player who accused you discards a random card from their hand"
	 * Cet effet consiste � retirer l'une des cartes rumeur non r�v�l� pr�sent dans la main du joueur qui vous a accus� de mani�re al�atoire.
	 * M�thode qui permet d'activer la m�canique de cet effet.
	 * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas o� le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet.
	 */
	public void activerEffet(Joueur joueurQuiUtiliseEffet) {
		//impl�mentation de l'effet The player who accused you discards a random card from their hand
		//Cette effet consiste a retirer l'une des carte rumeur non r�v�l� pr�sent dans la main du joueur qui vous a accus� de mani�re al�atoire
		//on affiche qu'elle effet vient d'�tre lanc�
		Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
		Partie.getPartie().ajouterUnMessage(2,"Ainsi, "+joueurQuiUtiliseEffet.getNomJoueur()+" vous allez forcer votre accusateur de d�fausser l'une de ces carte  rumeur de mani�re al�atoire.");
	
		Joueur joueurAccusateur = Partie.getPartie().getJoueurDebutTour();
		//on regarde si le joueur accusateur a au moins une carte rumeur qu'il peut se s�parer
		if(joueurAccusateur.listeCarteRumeurNonReveleEnMain().size() > 0) {
			//on r�cup�re de mani�re al�atoire la carte que le joueur va d�fausser
			CarteRumeur carteADefausser = joueurAccusateur.listeCarteRumeurNonReveleEnMain().get((int)(Math.random()*joueurAccusateur.listeCarteRumeurNonReveleEnMain().size()));
			//on ajoute cette carte � la liste des cartes d�fauss� pr�sent dans la classe partie
			Partie.getPartie().ajouterCarteRumeurDefausser(carteADefausser);
			//on retire de la main du joueur, la carte
			joueurAccusateur.retirerCarteRumeurEnMain(carteADefausser);
			//on affiche qu'elle carte il vient de d�fausser
			Partie.getPartie().ajouterUnMessage(2,joueurAccusateur.getNomJoueur()+", vous venez de d�fausser la carte:");
			Partie.getPartie().ajouterUnMessage(1,carteADefausser);
			Partie.getPartie().setSelectionCarteRumeurParmiListe(new ArrayList<>(Arrays.asList(carteADefausser)));
			Partie.getPartie().setJoueurQuiDoitJouer(joueurAccusateur);
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
		}else {
			//si le joueur accusateur ne peut pas se s�parer d'une carte rumeur
			Partie.getPartie().ajouterUnMessage(2,joueurAccusateur.getNomJoueur()+" n'a pas de carte rumeur qui peut �tre d�fauss�");
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
		}
	}

}
