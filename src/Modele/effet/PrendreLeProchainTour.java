package Modele.effet;

import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond � l'effet : "Prendre le prochain tour"
 * La personne qui utilise cet effet va �tre le joueur qui va d�buter le tour suivant.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class PrendreLeProchainTour extends Effet{
	/**
	 * Constructeur de la classe PrendreLeProchainTour, il permet de modifier la description de l'effet pr�sent dans la classe parent.
	 */
	public PrendreLeProchainTour() {
		super.setDescriptionEffet("Prendre le prochain tour");
	}
	
	/**
	 * Impl�mentation de l'effet "Take next turn"
	 * La personne qui utilise cet effet va �tre le joueur qui va d�buter le tour suivant.
	 * M�thode qui permet d'activer la m�canique de cet effet.
	 * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas o� le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet.
	 */
	public void activerEffet(Joueur joueurQuiUtiliseEffet) {
		//impl�mentation de l'effet Take next turn
		//on affiche qu'elle effet vient d'�tre lanc�
		Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
		Partie.getPartie().ajouterUnMessage(2,"Ainsi, "+joueurQuiUtiliseEffet.getNomJoueur()+" sera le joueur qui va d�buter le prochain tour.");
		//on modifie le prochain jour en cours
		Partie.getPartie().setJoueurDebutTour(joueurQuiUtiliseEffet);
		Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
		Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
	}

}
