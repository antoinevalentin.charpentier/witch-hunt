package Modele.effet;

import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond à l'effet : "R�v�lez l'identit� d'un autre joueur (Sorci�re : Vous gagnez 2pts. Vous jouez le prochain tour; Villageois : Vous perdez 2pts. Il prend le prochain tour)"
 * La personne qui utilise cet effet va choisir un joueur (qui ne peut pas �tre lui-m�me). Le joueur choisi va �tre contraint de r�v�ler son identit�.
 * Il n'a pas d'autre choix que de r�v�ler son identit�. Ainsi, la personne choisit ne doit pas avoir son identit� de r�v�l�.
 * Le joueur qui utilise l'effet va alors pouvoir gagner ou perdre des points en fonction du r�le de la personne qu'il accuse d'�tre une sorci�re.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class RevelerIdentiteAutreJoueur extends Effet{
  /**
   * Constructeur de la classe RevelerIdentiteAutreJoueur, il permet de modifier la description de l'effet pr�sent dans la classe parent.
   */
  public RevelerIdentiteAutreJoueur() {
    super.setDescriptionEffet("R�v�lez l'identit� d'un autre joueur (Sorci�re : Vous gagnez 2pts. Vous jouez le prochain tour; Villageois : Vous perdez 2pts. Il prend le prochain tour)");
  }
  
  /**
   * Impl�mentation de l'effet "Reveal another player's identity (Witch : You gain 2pts. You take next turn, Villager:You lose 2pts. They take next turn)"
   * La personne qui utilise cet effet va choisir un joueur (qui ne peut pas �tre lui-m�me). Le joueur choisi va �tre contraint de r�v�ler son identit�.
   * Il n'a pas d'autre choix que de r�v�ler son identit�. Ainsi, la personne choisit ne doit pas avoir son identit� de r�v�l�.
   * Le joueur qui utilise l'effet va alors pouvoir gagner ou perdre des points en fonction du r�le de la personne qu'il accuse d'�tre une sorci�re.
   * M�thode qui permet d'activer la m�canique de cet effet.
   * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas où le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet.
   */
  public void activerEffet(Joueur joueurQuiUtiliseEffet) {
    //impl�mentation de l'effet Reveal another player's identity (Witch : You gain 2pts. You take next turn, Villager:You lose 2pts. They take next turn)
    
    //on affiche qu'elle effet vient d'�tre lanc�
    Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
    Partie.getPartie().ajouterUnMessage(2,"Ainsi, "+joueurQuiUtiliseEffet.getNomJoueur()+" vous allez r�v�ler l'identit� d'une autre personne.");
    
    //on demande au joueur qui utilise cette effet, de choisir un joueur qui doit r�v�ler son identit�
    //Partie.getPartie().getListeJoueurAvecIdentiteNonRevelee(joueurQuiUtiliseEffet) contient au moins deux personne car s'il ne reste qu'une personne alors la manche est termin�
    
    //on d�finit la liste des joueur qu'il peut choisir entre
    Partie.getPartie().setSelectionJoueurParmiListe(Joueur.intersectionEntreListes(Partie.getPartie().getListeJoueurAvecIdentiteNonRevele(joueurQuiUtiliseEffet), joueurQuiUtiliseEffet.getListeJoueurContreQuiCarteRumeurJouable(joueurQuiUtiliseEffet.getDerniereCarteRumeurJouee())));
    //on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
    Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_JOUEUR_ACCUSATION);
    //on r�cup�re la valeur du choix r�alis�
    Joueur joueurQuiDoitRevelerIdentite = Partie.getPartie().getJoueurSelectionne();
    
    //on demande a ce qu'il r�v�le son identit� selon les directives de l'effets, (en consid�rant le cahngement de personne ad�quat et l'attribution ou la soustraction de point)
    //le joueur cible r�v�le son identit�
    //si villageois, il prend le tour suivant et le joueur qui a utilis� l'effet perd 2 pts
    //si sorci�re, le sorci�re qui utilise l'effet va prendre le tour suivant et gagne 2 pts
    joueurQuiDoitRevelerIdentite.revelerIdentiteAvecAjoutPertePoint(joueurQuiUtiliseEffet, 2, 2);
    Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
    Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
  }

}
