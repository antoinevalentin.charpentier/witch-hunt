package Modele.effet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import Modele.CarteRumeur;
import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond � l'effet : "Prenez une carte Rumeur r�v�l�e de n'importe quel autre joueur dans votre main"
 * La personne qui utilise cet effet peut r�cup�rer une carte rumeur r�v�l�e de n'importe quel joueur de la partie (m�me �limin�, sauf lui-m�me). Il choisit la carte rumeur via son nom et non choisit de mani�re al�atoire.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class PrendreCarteRumeurDUnePersonne extends Effet{
	/**
	 * Constructeur de la classe PrendreCarteRumeurDUnePersonne, il permet de modifier la description de l'effet pr�sent dans la classe parent.
	 */
	public PrendreCarteRumeurDUnePersonne() {
		super.setDescriptionEffet("Prenez une carte Rumeur r�v�l�e de n'importe quel autre joueur dans votre main");
	}
	
	/**
	 * Impl�mentation de l'effet "Take a revealed rumour card from any other player into your hand"
	 * La personne qui utilise cet effet peut r�cup�rer une carte rumeur r�v�l�e de n'importe quel joueur de la partie (m�me �limin�, sauf lui-m�me). Il choisit la carte rumeur via son nom et non choisit de mani�re al�atoire.
	 * M�thode qui permet d'activer la m�canique de cet effet.
	 * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas o� le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet.
	 */
	public void activerEffet(Joueur joueurQuiUtiliseEffet) {
		//impl�mentation de l'effet Take a revealed rumour card from any other player into your hand
		//La personne qui utilise cet effet peut r�cup�rer une carte rumeur r�v�l�e de n'importe quel joueur de la partie (m�me �limin�, sauf lui-m�me). Il choisit la carte rumeur via son nom et non choisit de mani�re al�atoire.
		//on affiche qu'elle effet vient d'�tre lanc�
		Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
		Partie.getPartie().ajouterUnMessage(2,"Ainsi, "+joueurQuiUtiliseEffet.getNomJoueur()+" vous allez pouvoir r�cup�rer une carte rumeur r�v�l�e de n'importe qu'elle joueur (sauf vous) dans votre main.");
		
		//on r�cup�re toute les cartes rumeur r�v�l�e des autre joueur que le joueur joueurQuiUtiliseEffet, m�me ceux �l�min�
		//on parcours tout les joueurs de la partie
		Iterator<Joueur> itJoueur = Partie.getPartie().getListeJoueurs().iterator();
		//on d�finit la variable qui va stocker le joueur a chaque it�rations parmit la liste des joueurs
		Joueur joueurIteration;
		//on stocke chaque carte rumeur avec sont propri�taire dans les listes suivantes
		//la carte rumeur stocker � la position x de la premi�re liste poss�de comme propri�taire le joueur � la position x de la deuxi�me liste
		ArrayList<CarteRumeur> listeCarteRumeurAssociationListeJoueur = new ArrayList<>();
		ArrayList<Joueur> listeJoueurAssociationListeCarteRumeur = new ArrayList<>();
		while(itJoueur.hasNext()) {
			//on r�cup�re le joueur de l'itaration dans la liste
			joueurIteration = itJoueur.next();
			//on regarde si le joueur est diff�rent de celui qui a utilis� l'effet, car il ne peut pas r�cup�rer ces propres cartes rumeur r�v�l�
			if(!joueurIteration.equals(joueurQuiUtiliseEffet)) {
				//on parcours toutes les cartes rumeur r�v�l�e du joueur de l'it�ration
				Iterator<CarteRumeur> itCarteRumeur = joueurIteration.getListeCarteRumeurRevele().iterator();
				while(itCarteRumeur.hasNext()) {
					//on ajoute les cartes r�v�l� dans l'hashmap avec le joueur qui la poss�de, c'est � dire le joueur de l'it�ration
					listeCarteRumeurAssociationListeJoueur.add(itCarteRumeur.next());
					listeJoueurAssociationListeCarteRumeur.add(joueurIteration);
				}
			}
		}
		
		//on regarde ss'il y a au moins une carte rumeur qu'il peut r�cup�rer
		if(listeCarteRumeurAssociationListeJoueur.size() == 0) {
			//s'il n'y a pas de carte qu'il peut r�cup�rer
			Partie.getPartie().ajouterUnMessage(2,"Vous ne pouvez pas r�cup�rer de carte rumeur r�v�l�e d'un autre joueur.");
			Partie.getPartie().ajouterUnMessage(2,"Ils n'ont pas de carte rumeur r�v�l�e.");
			
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
		}else {
			//dans le cas o� il peut r�cup�rer une carte rumeur
			//on lui demande de saisir le nom de la carte rumeur dont il veut r�cup�rer
			
			//on d�finit la liste des cartes rumeurs qu'il peut choisir entre
			Partie.getPartie().setSelectionCarteRumeurParmiListe(listeCarteRumeurAssociationListeJoueur);
			//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
			Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_RECUPERER);
			//on r�cup�re la carte rumeur du choix r�alis�, correspond � la carte rumeur que le joueur veut r�cup�rer
			CarteRumeur carteRecup = Partie.getPartie().getCarteRumeurSelectionne();
			
			//on la recache
			carteRecup.setFaceCache(true);
			//on la met dans la main du joueur qui a utiliser l'effet
			joueurQuiUtiliseEffet.ajouterCarteRumeurEnMain(carteRecup);
			//on la retire ce celle du joueur qui l'avait
			listeJoueurAssociationListeCarteRumeur.get(listeCarteRumeurAssociationListeJoueur.indexOf(carteRecup)).retirerCarteRumeurEnMain(carteRecup);
			//on affiche au joueur qui a r�cup�rer la carte, celle-ci
			Partie.getPartie().setSelectionCarteRumeurParmiListe(new ArrayList<>(Arrays.asList(carteRecup)));
			Partie.getPartie().ajouterUnMessage(2,joueurQuiUtiliseEffet.getNomJoueur()+", vous avez bien r�cup�r� la carte "+carteRecup.getNomCarte());
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
		}
	}

}

