package Modele.effet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import Modele.CarteRumeur;
import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond � l'effet : "Prenez en main l'une de vos propres cartes Rumeur r�v�l�es"
 * La personne qui utilise cet effet va avoir l'opportunit� de r�cup�rer l'une de ces cartes rumeurs qu'il a r�v�l�es. Il va ainsi avoir de nouveau en main une carte rumeur qui n'est pas face cach�e.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class RecupererCarteRumeurRevele extends Effet{
	/**
	 * Constructeur de la classe RecupererCarteRumeurRevele, il permet de modifier la description de l'effet pr�sent dans la classe parent.
	 */
	public RecupererCarteRumeurRevele() {
		super.setDescriptionEffet("Prenez en main l'une de vos propres cartes Rumeur r�v�l�es");
	}
	
	/**
	 * Impl�mentation de l'effet "Take one of your own revealed rumour cards into your hand"
	 * Cet effet consiste � r�cup�rer l'une de vos cartes rumeur r�v�l�e (pour la remettre dans votre main) et donc plus visible aux yeux de tous.
	 * La personne qui utilise cet effet va avoir l'opportunit� de r�cup�rer l'une de ces cartes rumeurs qu'il a r�v�l�es. Il va ainsi avoir de nouveau en main une carte rumeur qui n'est pas face cach�e.
	 * M�thode qui permet d'activer la m�canique de cet effet.
	 * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas o� le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet.
	 */
	public void activerEffet(Joueur joueurQuiUtiliseEffet) {
		//impl�mentation de l'effet Take one of your own revealed rumour cards into your hand
		//Cette effet consiste a r�cup�rer l'une de vos carte rumeur r�v�l�, (pour la remettre dans votre main) et donc plus visible aux yeux de tous
		Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
		Partie.getPartie().ajouterUnMessage(2,"Ainsi, "+joueurQuiUtiliseEffet.getNomJoueur()+" vous allez pouvoir r�cup�rer une carte rumeur que vous avez d�j� utilis�e.");
		//on regarde si le joueur a au moins une carte rumeur qu'il a utilis�
		//pour ce faire on r�cup�re la liste des carte r�v�l� du joueur hormis la carte qui utilise l'effet actuellement
		//on initialise la liste qui va contenir l'ensemble des carte rumeurs ormis la carte qui utilise l'effet actuellement
		ArrayList<CarteRumeur> listeCarteReveleSaufActuel = new ArrayList<>();
		//on parcours l'ensemble des cartes rumeur r�v�l� du joueur
		Iterator<CarteRumeur> it = joueurQuiUtiliseEffet.getListeCarteRumeurRevele().iterator();
		while(it.hasNext()) {
			//on r�cup�re la carte de l'it�ration
			CarteRumeur c = it.next();
			//on regarde si elle ne correspond pas a celle en cours
			if(!c.equals(joueurQuiUtiliseEffet.getDerniereCarteRumeurJouee())) {
				//s'il ne s'agit pas de la carte qui utilise l'effet actuellement
				listeCarteReveleSaufActuel.add(c);
				//on l'ajoute � la liste des carte dont il pourra r�cup�rr par la suite
			}
		}
		if(listeCarteReveleSaufActuel.size() > 0) {
			//on r�cup�re la carte que le joueur veut r�cup�rer
			
			//on d�finit la liste des cartes rumeurs qu'il peut choisir entre
			Partie.getPartie().setSelectionCarteRumeurParmiListe(listeCarteReveleSaufActuel);
			//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
			Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_RECUPERER);
			//on r�cup�re la carte rumeur du choix r�alis�, correspond � la carte rumeur que le joueur veut d�fausser
			CarteRumeur carteARecuperer = Partie.getPartie().getCarteRumeurSelectionne();
			
			Partie.getPartie().ajouterUnMessage(2, joueurQuiUtiliseEffet.getNomJoueur()+", vous venez de r�cup�rer la carte "+carteARecuperer.getNomCarte());
			
			//la carte qu'il veut r�cup�rer se situe d�j� dans sa main (carte utilis� + celles cach�)
			//il nous faut simplement la remasquer
			carteARecuperer.setFaceCache(true);
			Partie.getPartie().setSelectionCarteRumeurParmiListe(new ArrayList<>(Arrays.asList(carteARecuperer)));
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
		}else {
			//si le joueur ne peut pas se s�parer d'une carte rumeur
			Partie.getPartie().ajouterUnMessage(2,"Vous n'avez pas de carte rumeur de retourn�e. Vous ne pouvez donc pas en r�cup�rer");
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
		}
	}

}

