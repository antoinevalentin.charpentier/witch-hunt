package Modele.effet;

import java.util.ArrayList;
import java.util.Arrays;

import Modele.Effet;
import Modele.Joueur;
import Modele.Partie;
import Modele.enums.EnumStatutPartie;

/**
 * Classe qui permet d'impl�menter un effet particulier.
 * Cet effet correspond � l'effet : "Choisissez un joueur. Il doit r�v�ler son identit� ou d�fausser une carte de sa main. (Sorci�re : vous gagnez 1 pt. Vous prenez le prochain tour, Villageois : vous perdez 1 pt. Il prend le prochain tour. S'il d�fausse : il prend le prochain tour)"
 * Cet effet permet au joueur qui l'utilise de forcer un autre joueur de r�v�ler son identit� ou de d�fausser une de ces cartes rumeurs.
 * Il peut alors gagner ou perdre des points en fonction du r�le de la personne accus�.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class ChoisirJoueurReverlerSonIdentiteOuDefausser extends Effet{
	
	/**
	 * Constructeur de la classe ChoisirJoueurRevelerSonIdentiteOuDefausser, il permet de modifier la description de l'effet pr�sent dans la classe parent.
	 */
	public ChoisirJoueurReverlerSonIdentiteOuDefausser() {
		super.setDescriptionEffet("Choisissez un joueur. Il doit r�v�ler son identit� ou d�fausser une carte de sa main. (Sorci�re : vous gagnez 1 pt. Vous prenez le prochain tour, Villageois : vous perdez 1 pt. Il prend le prochain tour. S'il d�fausse : il prend le prochain tour)");
	}
	
	/**
	 * Impl�mentation de l'effet "Choose a player. They must reveal their identity or discard a card from their hand. (Witch : You gain 1pt. You take next turn, Villager:You lose 1pt.They take next turn. If they discard:they take next turn)"
	 * Cet effet consiste � Choisir un joueur. Il doivent r�v�ler son identit� ou d�fausser une carte de sa main. (Sorci�re : vous gagnez 1 pt. Vous prenez le prochain tour, Villageois : vous perdez 1 pt. Il prend le prochain tour. S'il d�fausse : il prend le prochain tour)
	 * M�thode qui permet d'activer la m�canique de cet effet.
	 * @param joueurQuiUtiliseEffet d�signe le joueur qui utilise l'effet. Il peut �tre diff�rent de celui qui doit jouer durant l'execution d'un effet. Dans le cas o� le joueur qui utilise l'effet choisi un autre joueur qui doit a son tour r�alsier une op�ration, il nous faut alors dans ce cas si avoir le joueur s�lectionn� et celui qui utilise l'effet.
	 */
	public void activerEffet(Joueur joueurQuiUtiliseEffet) {
		//impl�mentation de l'effet Choose a player. They must reveal their identity or discard a card from their hand. (Witch : You gain 1pt. You take next turn, Villager:You lose 1pt.They take next turn. If they discard:they take next turn)
		// Description de l'effet cf le constructeur
		//on affiche qu'elle effet vient d'�tre lanc�
		Partie.getPartie().ajouterUnMessage(2,">> Effet : "+super.getDescriptionEffet());
		
		//on lui demande dans un premier temps de joueur la personne sur laquelle va se rediriger cette effet parmis la liste des personnes encore non r�v�l�
		//selon les r�gles du jeu, il y a tout le temps au moins 2 personne qui ne sont pas r�v�l� (si le joueur qui utilise l'effet n'est pas r�v�l�, alors il reste au moins une autre personne autre que lui m�me)
		Partie.getPartie().ajouterUnMessage(2,"Ainsi, "+joueurQuiUtiliseEffet.getNomJoueur()+" vous allez devoir dans un premier temps choisir une personne (autre que vous).");
		//on intiialise une liste de joueur qui va accueillir tout les joueurs dont il peut choisir
		ArrayList<Joueur> listeJoueurCible = joueurQuiUtiliseEffet.getListeJoueurContreQuiCarteRumeurJouable(joueurQuiUtiliseEffet.getDerniereCarteRumeurJouee());;
		
		//on lui demande de choisir le joueur cible parmi la liste des joueurs pr�c�dante		
		//on demande au joueur qui utilise l'effet de choisir le prochain joueur qui va soit accuser une personne soit utiliser une carte rumeur effet sorci�re
		//on d�finit la liste des joueur qu'il peut choisir entre
		Partie.getPartie().setSelectionJoueurParmiListe(listeJoueurCible);
		//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
		Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_JOUEUR_ACCUSATION);
		//on r�cup�re la valeur du choix r�alis�
		Joueur joueurCible = Partie.getPartie().getJoueurSelectionne();
		
		
		//on modifie le joueur en cours par le joueur qui a �t� choisi
		Partie.getPartie().setJoueurQuiDoitJouer(joueurCible);
		//une fois que l'on a d�termin� le joueur sur lequel va s'appliquer l'effet, 
		//on regarde s'il peut d�fausser une carte de sa main
		int choixEntreRevelerDefausser = 1;
		if(joueurCible.listeCarteRumeurNonReveleEnMain().size() != 0) {
			//dans le cas o� la persone peut d�fausser une carte rumeur de sa main
			//on regarde maintenant s'il n'aurait pas d�j� r�v�l� son identit�
			if(joueurCible.getEstRevele() == false) {
				//s'il n'est pas r�v�l� et qu'il peut d�fausser des carte
				//on lui demande ce qu'il pr�f�re
				if(joueurCible.getEstJoueurVirtuel() == false) {
					//on affiche les propositions
					Partie.getPartie().ajouterUnMessage(1,joueurCible.getNomJoueur()+", vous allez devoir choisir entre : (veuillez saisir un nombre 1 ou 2)");
					Partie.getPartie().ajouterUnMessage(1,"     1 - R�v�ler votre identit�");
					Partie.getPartie().ajouterUnMessage(1,"     2 - D�fausser une carte rumeur de votre main");
					Partie.getPartie().ajouterUnMessage(0,joueurCible.getNomJoueur()+", vous allez devoir choisir entre : ");
					Partie.getPartie().ajouterUnMessage(0,"     - R�v�ler votre identit�");
					Partie.getPartie().ajouterUnMessage(0,"     - D�fausser une carte rumeur de votre main");
				}else {
					Partie.getPartie().ajouterUnMessage(2,joueurCible.getNomJoueur()+", doit choisir entre r�v�ler son identit� ou d�fauss� une carte");
				}
				
				//on modifie les bornes de l'intervalle. Il doit choisir enrte 1 et 2
				Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(1,2))); 
				//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
				Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_DEFAUSSER);
				//on r�cup�re la valeur du choix r�alis�
				choixEntreRevelerDefausser = Partie.getPartie().getEntierSelectionne();
				
				
			}else {
				//dans le cas o� il peut d�fausser une carte et qu'il a d�j� r�v�l� son identit�
				//on le force alors � d�fausser une de ces carte
				Partie.getPartie().ajouterUnMessage(2,joueurCible.getNomJoueur()+" a d�j� r�v�ler son identit� et il poss�de des cartes rumeurs jouables.");
				Partie.getPartie().ajouterUnMessage(2,"Vous allez donc en utiliser une.");
				choixEntreRevelerDefausser=2;
			}
			
		}else {
			//dans le cas o� il n'a pas de carte rumeur non r�v�l�
			//il doti alors forc�ment r�v�ler son identit�
			//on lui indique alors
			if(joueurCible.getEstJoueurVirtuel() == false) {
				Partie.getPartie().ajouterUnMessage(2,joueurCible.getNomJoueur()+", vous ne pouvez pas d�fausser de carte rumeur.");
				Partie.getPartie().ajouterUnMessage(2,"Vous allez devoir r�v�ler votre identit�");
				if(joueurCible.getEstRevele() == true) {
					Partie.getPartie().ajouterUnMessage(2,"M�me s'il vous l'avez d�j� fait.");
				}
			}else {
				Partie.getPartie().ajouterUnMessage(2,joueurCible.getNomJoueur()+", ne peut pas d�fausser de carte rumeur.");
				Partie.getPartie().ajouterUnMessage(2,"Il doit alors r�v�ler son identit�.");
				//on regarde s'il a d�j� r�v�l� son identit�
				if(joueurCible.getEstRevele() == true) {
					Partie.getPartie().ajouterUnMessage(2,"M�me s'il l'a d�j� fait.");
				}
			}
			
		}
		
		if(choixEntreRevelerDefausser == 1) {
			//dans le cas o� il a choisit de r�v�ler son identit�
			//le joueur cible r�v�le son identit�
			//si villageois, il prend le tour suivant et le joueur qui a utilis� l'effet perd 1 pts
			//si sorci�re, le sorci�re qui utilise l'effet va prendre le tour suivant et gagne 1 pts
			joueurCible.revelerIdentiteAvecAjoutPertePoint(joueurQuiUtiliseEffet, 1, 1);
		}else {
			//dans le cas o� il a choisit de d�fausser une carte rumeur de sa main
			//on lui demande de choisir la carte a defausser puis de s'en s�parer
			joueurCible.choisirCarteRumeurPuisDefausser(joueurCible.listeCarteRumeurNonReveleEnMain());
			//il prend alors le tour suivant
			Partie.getPartie().setJoueurDebutTour(joueurCible);
		}
		
		//on remet la valeur par d�faut pour le joueur qui doit jouer
		//la meme qu'au d�but de cette m�thode
		
		Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
		Partie.getPartie().setJoueurQuiDoitJouer(joueurQuiUtiliseEffet);
	}
	

}

