/**
 * Package destin� � regrouper l'ensemble des diff�rents fichiers �num�ration utilis� lors du jeu.
 * <ul>
 * 		<li>EnumStatutPartie : Permet de savoir l'avancement dans la partie. Dans qu'elle phase du jeu, on se situe. Connaitre ce qui est attendu des joueurs. Cette �num�ration permet aux joueurs virtuel d'adapter leur choix (Joueurs, carte rumeurs, entier, ...) en fonction du contexte du jeu.</li>
 * 		<li>EnumJouabiliteEffetCarteRumeur :  Conditions appliqu�es � un effet d'une carte rumeur. Exemple : effet jouable seulement si r�v�l� villageois.</li>
 * 		<li>EnumRole : Ensemble des r�les qu'un joueur peut incarner durant la partie</li>
 * </ul>
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
package Modele.enums;