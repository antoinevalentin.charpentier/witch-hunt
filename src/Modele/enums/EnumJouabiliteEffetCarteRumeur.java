package Modele.enums;

/**
 * Conditions appliqu�es � un effet d'une carte rumeur. Exemple : effet jouable seulement si r�v�l� villageois.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public enum EnumJouabiliteEffetCarteRumeur {
	AUCUNE, JOUABLE_SI_REVELE_VILLAGEOIS, JOUABLE_SI_CARTE_RUMEUR_REVELEE, JOUABLE_SI_REVELE_VILLAGEOIS_ET_CARTE_RUMEUR_REVELEE
}
