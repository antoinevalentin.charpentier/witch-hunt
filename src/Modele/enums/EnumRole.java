package Modele.enums;

/**
 * Ensemble des r�les qu'un joueur peut incarner durant la partie
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public enum EnumRole {
	VILLAGEOIS,SORCIERE
}
