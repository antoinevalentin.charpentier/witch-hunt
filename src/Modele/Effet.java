package Modele;

/**
 * Repr�sente un effet, qu'une carte peut avoir soit dans ces effets sorci�res ou dans ces effets chasseurs.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public abstract class Effet {
	//les attributs
	/**
	 * Un attribut qui d�crit ce que fait l'effet. Il stocke un r�capitulatif de ce que l'effet r�alise.
	 */
	private String descriptionEffet;
	
	//constructeur
	
	//getter
	/**
	 * M�thode qui permet de r�cup�rer la description de l'effet, c'est-�-dire ce que fait l'effet.
	 * @return La description de l'effet sous la forme d'une cha�ne de caract�res.
	 */
	public String getDescriptionEffet() {
		return this.descriptionEffet;
	}
	
	//setter
	/**
	 * M�thode qui permet de modifier la description d'un effet. Elle remplace l'ancienne description par une nouvelle pass� en param�tre.
	 * @param descriptionEffet La nouvelle description de l'effet.
	 */
	public void setDescriptionEffet(String descriptionEffet) {
		this.descriptionEffet=descriptionEffet;
	}
	
	
	//les autres m�thodes
	/**
	 * M�thode qui r�capitule ce que r�alise l'effet.
	 */
	public String toString() {
		return "Effet("+this.getDescriptionEffet()+")";
	}
	
	/**
	 * M�thode qui permet d'appliquer concr�tement l'effet.
	 * @param joueurQuiUtiliseEffet D�signe le joueur qui a utilis� la carte poss�dant l'effet qui doit �tre appliqu�.
	 */
	public abstract void activerEffet(Joueur joueurQuiUtiliseEffet);
	
}
