package Modele;

import java.util.ArrayList;
import java.util.Arrays;

import Modele.enums.EnumStatutPartie;
import Modele.strategie.StrategieAdaptative;
import Modele.strategie.StrategieAgressive;
import Modele.strategie.StrategieAleatoire;
import Modele.strategie.StrategieJoueurVirtuel;

/**
 * Repr�sente un joueur virtuel qui joue � une partie de Witch Hunt
 * @author Charpentier
 */
public class JoueurVirtuel extends Joueur{
	//les attributs
	/**
	 * La strat�gie associ�e au joueur virtuel.
	 */
	private StrategieJoueurVirtuel strategieJoueurVirtuel;
	
	//le constructeur
	/**
	 * Constructeur de la classe JoueurVirtuel.
	 * Ellle permet d'instancier un joueur virtuel avec un certain nom.
	 * @param nomJoueur Le nom du joueur virtuel.
	 */
	public JoueurVirtuel(String nomJoueur) {
		super(nomJoueur,true);
		this.strategieJoueurVirtuel = new StrategieAleatoire();
	}
	
	//les getter
	/**
	 * M�thode qui permet de r�cup�rer la strat�gie du joueur virtuel.
	 * @return
	 */
	public StrategieJoueurVirtuel getStrategieJoueurVirtuel() {
		return this.strategieJoueurVirtuel;
	}
	
	//les setter
	/**
	 * M�thode qui permet de d�finir la strat�gie du joueur virtuel � partir de celle pass� en param�tre.
	 * @param strategieJoueurVirtuel La nouvelle strat�gie du joueur.
	 */
	public void setStrategieJoueurVirtuel(StrategieJoueurVirtuel strategieJoueurVirtuel) {
		this.strategieJoueurVirtuel=strategieJoueurVirtuel;
	}
	
	//les autres m�thodes
	/**
	 * M�thode qui permet d'assigner de mani�re al�atoire une strat�gie � un joueur virtuel
	 */
	public void attributionStrategie() {
		//m�thode qui permet d'assigner de mani�re al�atoire une strat�gie
		ArrayList<StrategieJoueurVirtuel> strategie = new ArrayList<>(Arrays.asList(new StrategieAgressive(), new StrategieAleatoire(), new StrategieAdaptative()));
		
		this.setStrategieJoueurVirtuel(strategie.get((int)(Math.random()*strategie.size())));
	}
	
	//red�finition des m�thodes
	/**
	 * M�thode qui permet � un joueur virtuel de choisir son r�le par l'interm�diaire de �a strat�gie.
	 */
	public void choisirRole() {
		//red�finition de la m�thode qui permet � une IA de choisir son r�le en fonction de sa strat�gie
		this.strategieJoueurVirtuel.choisirRole();
	}
	
	/**
	 * M�thode qui permet � un joueur virtuel d'accuser un autre joueur.
	 */
	public void accuser(ArrayList<Joueur> listeJoueurPossibleAccuser) {
		//m�thode qui permet � une IA de lancer une accusation contre un autre joueur
		//c'est le joueur virtuel qui doit jouer
		Partie.getPartie().setJoueurQuiDoitJouer(this);
		
		//on r�cup�re un joueur � partir de son nom (celui ci ne doit pas etre r�v�l�, ni �tre le joueur qui le choisi)
		//on d�finit la liste des joueur qu'il peut choisir entre
		//le joueur virtuel devra choisir un joueur parmi cette liste
		Partie.getPartie().setSelectionJoueurParmiListe(listeJoueurPossibleAccuser);
		//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
		Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_JOUEUR_ACCUSATION);
		//on r�cup�re la valeur du choix r�alis�
		Joueur joueurAccuse = Partie.getPartie().getJoueurSelectionne();
		
		Partie.getPartie().ajouterUnMessage(2,">>> "+this.getNomJoueur()+" a choisi d'accuser le joueur "+joueurAccuse.getNomJoueur());
		//le joueur qui se fait accuser doit alors r�pondre � l'accusation
		Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
		joueurAccuse.repondreAccusation();

	}
	
	/**
	 * M�thode qui permet � un joueur virtuel de r�pondre � l'accusation d'un autre joueur.
	 */
	public void repondreAccusation() {
		//m�thode qui permet � une IA de r�pondre � une accusation en fonction de sa strat�gie
		//on modifie le joueur qui doit jouer
		Partie.getPartie().setJoueurQuiDoitJouer(this);
		
		Partie.getPartie().ajouterUnMessage(2,">>> C'est au tour de l'accus� : "+this.getNomJoueur());
		Partie.getPartie().ajouterUnMessage(2,"Il doit r�pondre � l'accusation provenant de "+Partie.getPartie().getJoueurDebutTour().getNomJoueur());

		
		int choix = 1;//R�v�ler son identit� =1, effet sorci�re =2
		//on regarde s'il peut utiliser l'effet chasseur d'une de ces cartes rumeurs
		if(this.listeCarteRumeurJouableEnMain(true,false).size() != 0) {
			//si le joueur dispose de carte rumeur non r�l�v�, 
			//on regarde s'il n'a pas d�j� r�v�l� son identit�
			if(this.getEstRevele() == false) {
				//s'il n'est pas r�v�l� et qu'il peut utiliser des cartes rumeurs
				//on lui demande alors de choisir entre r�v�ler son identit� ou d'utiliser l'effet sorci�re d'une de ces carte rumeur
				//si choix 1 : r�v�ler identit�
				//si choix 2 : utiliser une carte rumeur effet sorci�re
				
				//on demande � l'utilisateur de choisir entre ces deux options 
				//on d�finit les bornes de l'intervalle de la s�lection de l'entier
				Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(1,2)));
				//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
				Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_EFFET_SORCIERE);
				//on r�cup�re la valeur du choix r�alis�
				choix = Partie.getPartie().getEntierSelectionne();
				
			}else {
				//dans le cas o� il est r�v�l� et qu'il a des carte rumeur qu'il peut utiliser
				choix = 2;
				//on lui force d'utiliser ces cartes rumeurs, car il ne va pas pouvoir r�v�ler une autre fois son identit�
				Partie.getPartie().ajouterUnMessage(2,this.getNomJoueur()+" a d�j� r�v�ler son identit� et il poss�de des cartes rumeurs jouables.");
				Partie.getPartie().ajouterUnMessage(2,"Il va donc en utiliser une.");
			}
			
		}else {
			//si le joueur n'a pas le choix que de r�v�ler son identit� car il n'a plus de carte rumeur jouable
			Partie.getPartie().ajouterUnMessage(2,this.getNomJoueur()+" n'a plus de carte rumeur en main non r�v�l�e dont il peut utiliser son effet sorci�re");
			Partie.getPartie().ajouterUnMessage(2,"Il doit alors r�v�ler son identit�.");
			//dans le cas o� il ne peut pas jouer de carte rumeur et qu'il a d�j� r�v�l� son identit� dans le pass�, 
			//il va devoir r�v�ler son identit� une deuxi�me fois
			if(this.getEstRevele() == true) {
				Partie.getPartie().ajouterUnMessage(2,"M�me s'il l'a d�j� fait.");
			}
		}
		
		
				
		if(choix == 1) {
			//si le joueur � d�cider de r�v�ler son identit�
			Partie.getPartie().ajouterUnMessage(2,this.getNomJoueur()+" a d�cid� de r�v�ler son identit�");
			this.revelerIdentiter();
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
			Partie.getPartie().setJoueurQuiDoitJouer(this);
		}else {
			//si le joueur � d�cid� d'utiliser l'effet sorciere d'une de ces carte rumeurs
			Partie.getPartie().ajouterUnMessage(2,this.getNomJoueur()+" a d�cid� d'utiliser l'effet sorci�re d'une de ces carte rumeur");
			//On demande de confirmer pour marquer une pause suite � la r�ponse,
			Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
			Partie.getPartie().setJoueurQuiDoitJouer(this);
			this.utiliserCarteRumeur(true,false);
		}
		
		
	}
	
	/**
	 * M�thode qui permet d'afficher � l'�cran une liste de carte rumeur d'un joueur virtuel
	 * Il s'agit d'une red�finition de la m�thode issu de la classe Joueur, pour ne pas les afficher.
	 * Pour que les joueurs physique ne puisse pas voir les cartes du joueur virtuel
	 * C'est pour �a que la m�thode est vide, elle ne doit rien afficher
	 */
	public void affichageListeCarte(ArrayList<CarteRumeur> listeCarteRumeur) {
		//m�thode qui permet d'afficher � l'�cran une liste de carte rumeur
		//Il s'agit d'une red�finition de la m�thode issu de la classe Joueur
		//pour ne pas les afficher
		//pour que les joueurs physique ne puisse pas voir les cartes du joueur virtuel
		//c'est pour �a que la m�thode est vide, elle ne doit rien afficher
	}
	
	/**
	 * M�thode qui permet � un joueur virtuel de choisir une carte parmit une liste et de la d�fausser par la suite.
	 * La s�lection de la carte rumeur est effectu� par l'interm�diaire de la strat�gie.
	 * M�thode utilis�e essentiellement par les effets :
	 *      - Discard a card from your hand
	 *		- Choose a player. They must reveal their identity or discard a card from their hand. (Witch : You gain 1pt. You take next turn, Villager:You lose 1pt.They take next turn. If they discard:they take next turn)
	 */
	public void choisirCarteRumeurPuisDefausser(ArrayList<CarteRumeur> listeCarteADefausser) {
		//m�thode qui permet de choisir al�atoirement une carte parmit une liste et de la d�fausser par la suite
		//utiliser pour ne pas r�p�ter de code entre deux effets
		//     - Discard a card from your hand
		//     - Choose a player. They must reveal their identity or discard a card from their hand. (Witch : You gain 1pt. You take next turn, Villager:You lose 1pt.They take next turn. If they discard:they take next turn)
		//on r�cup�re la carte que le joueur veut d�fausser
		
		//on d�finit la liste des cartes rumeurs qu'il peut choisir entre
		Partie.getPartie().setSelectionCarteRumeurParmiListe(listeCarteADefausser);
		//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
		Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_DEFAUSSER);
		//on r�cup�re la carte rumeur du choix r�alis�, correspond � la carte rumeur que le joueur veut d�fausser
		CarteRumeur carteADefausser = Partie.getPartie().getCarteRumeurSelectionne();
		
		//on ajoute cette carte � la liste des cartes d�fauss� pr�sent dans la classe partie
		Partie.getPartie().ajouterCarteRumeurDefausser(carteADefausser);
		//on retire de la main du joueur, la carte
		this.retirerCarteRumeurEnMain(carteADefausser);
		//on lui indique qu'il a bien d�fausser la carte s�lectionn�e
		Partie.getPartie().ajouterUnMessage(2,this.getNomJoueur()+" a bien d�fauss� une carte rumeur non r�v�l�e");
		Joueur joueurTemporaire = Partie.getPartie().getJoueurQuiDoitJouer();
		Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE);
		Partie.getPartie().setJoueurQuiDoitJouer(joueurTemporaire);
	}

	/**
	 * M�thode qui permet de bloquer l'ex�cution du programme principale le temps qu'une saisi est demand�e.
	 * Pour le cas des joueurs virtuels, on fait appel aux m�thodes des s�lections de la strat�gie du joueur virtuel.
	 * 	@param nouveauStatutPartie D�signe le nouveau statut que la partie va prendre. Pour que les joueurs connaissent la raison de l'attente.
	 */
	public void enAttenteDeSelection(EnumStatutPartie nouveauStatutPartie) {
		//m�thode qui permet de lancer les m�thode de s�lections associ� � la strat�gie du joueur virtuel
		//en fonction de ce que le jeu veut
		//ici on ne bloque pas l'execution du programme car les joueurs virtuels r�alisent leur d�cision instant
		
		//on modifie le sattut de la partie par le statut pass� en param�tre.
		//le statut de la partie � une influence sur le choix r�alis� par le joueur virtuel
		Partie.getPartie().setStatutPartie(nouveauStatutPartie);
		
		//liste de l'ensemble des statut de la partie qui demande une s�lection d'une carte rumeur
		ArrayList<EnumStatutPartie> listeSelectionCarteRumeur = new ArrayList<>(Arrays.asList(EnumStatutPartie.SELECTION_CARTE_RUMEUR_UTILISATION_EFFET,EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_DEFAUSSER,EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_RECUPERER)); 
		//liste de l'ensemble des statut de la partie qui demande une s�lection d'un joueur
		ArrayList<EnumStatutPartie> listeSelectionJoueur = new ArrayList<>(Arrays.asList(EnumStatutPartie.SELECTION_JOUEUR_SUIVANT,EnumStatutPartie.SELECTION_JOUEUR_ACCUSATION));  
		//liste de l'ensemble des statut de la partie qui demande une s�lection d'un entier
		ArrayList<EnumStatutPartie> listeSelectionEntier = new ArrayList<>(Arrays.asList(EnumStatutPartie.SELECTION_ENTIER_MENU,EnumStatutPartie.SELECTION_ENTIER_ACCUSATION_OU_EFFET_CHASSEUR,EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_EFFET_SORCIERE, EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION,EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_DEFAUSSER,EnumStatutPartie.SELECTION_ENTIER_ROLE,EnumStatutPartie.SELECTION_ENTIER_NOMBRE_JOUEUR,EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE)); 
		
		//si la partie demande au joueur virtuel de choisir une carte rumeur stock�e dans un attribut de la classe partie : selectionCarteRumeurParmiListe
		if(listeSelectionCarteRumeur.contains(Partie.getPartie().getStatutPartie())) {
			this.strategieJoueurVirtuel.choisirCarteRumeurParmiListe();
		//si la partie demande au joueur virtuel de choisir un joueur stock� dans un attribut de la classe partie : selectionJoueurParmiListe
		}else if(listeSelectionJoueur.contains(Partie.getPartie().getStatutPartie())) {
			this.strategieJoueurVirtuel.choisirJoueurParmiListe();
		//si la partie demande au joueur virtuel de choisir un entier compris entre les bornes d'un intervalle qui sont stock� dans un attribut de la classe partie : selectionEntierParmiListe
		}else if(listeSelectionEntier.contains(Partie.getPartie().getStatutPartie())) {
			this.strategieJoueurVirtuel.choisirParmiIntervalleEntier();
		}
	}
	

}
