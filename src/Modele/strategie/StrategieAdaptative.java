package Modele.strategie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import Modele.CarteRumeur;
import Modele.Joueur;
import Modele.JoueurVirtuel;
import Modele.Partie;
import Modele.enums.EnumRole;
import Modele.enums.EnumStatutPartie;

/**
 * Classe d�signant une des strat�gie qu'un joueur virtuel peut poss�der. Les choix que ce joueur effectue d�pendront de la strat�gie qu'il poss�de.
 * Il s'agit de la strat�gie qui se rapproche le plus possible de ce qu'un joueur r�el fairait. Il r�alise des choix en fonction de l'environnement du jeu et du contexte.
 * Par exemple, il va tout faire pour ne pas r�v�ler son identit� alors que c'est une sorci�re, pour rallonger le plus possible le temps de jeu.
 * @author Charpentier
 */
public class StrategieAdaptative implements StrategieJoueurVirtuel{
	/**
	 * M�thode qui retourne le premier joueur avec pour r�le celui pass� en param�tre qu'il croise dans la liste, ou sinon le premier joueur dans le cas qu'il n'y a pas de joueur avec le r�le demand�
	 * On r�cup�re forc�ment un joueur, s'il n'y a pas le joueur correspondant aux crit�res, on retourne le premier de la liste.
	 * @param listeJoueur La liste de joueur parmi laquelle on doit retourner un joueur
	 * @param role Le r�le d�sir� que l'on veut que le joueur retourn� poss�de, si possible.
	 * @return Le joueur qui a le r�le voulu, si possible.
	 */
	public Joueur rechercherJoueurAvecUnCertainRole(ArrayList<Joueur> listeJoueur,EnumRole role) {
		
		if(listeJoueur.isEmpty()) {
			return null;
		}
		Iterator<Joueur> it = listeJoueur.iterator();
		while(it.hasNext()) {
			Joueur j = it.next();
			if(j.getCarteIdentite().getRole().equals(role)) {
				return j;
			}
		}
		return listeJoueur.get(0);
	}
	
	/**
	 * M�thode permettant � un joueur virtuel de choisir un joueur parmi la liste pr�sente dans l'attribut selectionJoueurParmiListe de la classe Partie.
	 * Le joueur est choisi en fonction du statut de la partie. 
	 * S'il doit accuser une personne, on le fait choisir si possible une sorci�re.
	 * S'il doit choisir le joueur suivant, alors on lui fait choisir une personne qui a le m�me r�le que lui.
	 * Sinon on retourne un joueur al�atoirement.
	 * Une fois le joueur choisi, il est stock� dans l'attribut joueurSelectionne de la classe Partie.
	 */
	public void choisirJoueurParmiListe() {
		//m�thode qui permet de choisir un joueur de mani�re al�atoire parmi une liste
		//on r�cup�re la liste parmi laquelle le joueur virtuel peut choisir
		ArrayList<Joueur> listeJoueur = Partie.getPartie().getSelectionJoueurParmiListe();
		//on regarde que la liste n'est pas vide,
		if(!listeJoueur.isEmpty()) {
			//dans le cas qu'il doit choisir un joueur parmi la liste pour l'accuser d'�tre une sorci�re
			if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_JOUEUR_ACCUSATION)) {
				//dans ce cas si, on recherche s'il y a un joueur parmi la liste qui est une sorci�re pour pouvoir l'accuser, sinon il est forc� d'accuser un villageosi
				//c'est comme si le joueur virtuel sait qui qu'il faut accuser d'�tre une srci�re sachant qu'il peut tr�s bien ne pas y en avoir
				//oui l'IA triche
				Partie.getPartie().setJoueurSelectionne(this.rechercherJoueurAvecUnCertainRole(listeJoueur,EnumRole.SORCIERE));
			}else if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_JOUEUR_SUIVANT)) {
				//dans ce cas si, l'IA veut choisir un joeuru qui a le meme r�le que lui
				Partie.getPartie().setJoueurSelectionne(this.rechercherJoueurAvecUnCertainRole(listeJoueur,Partie.getPartie().getJoueurQuiDoitJouer().getCarteIdentite().getRole()));
			}else {
				//Dans tout les autres cas, on tire un joueur al�atoire parmit la liste
				Partie.getPartie().setJoueurSelectionne(listeJoueur.get((int)(Math.random()*listeJoueur.size())));
			}
		}
	}
	
	/**
	 * M�thode qui permet de retourner la premi�re carte rumeur de la liste qui est soit jouable ou non, selon la valeur du bool�en pass� en param�tre
	 * Il y a forc�ment une carte rumeur de retourn�. S'il n'y a pas � la carte souhait�, alors on en prend une autre.
	 * @param listeCarteRumeur La liste des cartes rumeurs parmi laquelle il faut regarder
	 * @param jouable Bool�en si on veut r�cup�rer si possible une carte rumeur jouable ou non.
	 * @return La carte rumeur souhait�e si possible, sinon une autre de la liste.
	 */
	public CarteRumeur rechercheCarteRumeurJouableOuNonParmiListe(ArrayList<CarteRumeur> listeCarteRumeur, boolean jouable){
		
		//on parcours toute les carte de la liste
		Iterator<CarteRumeur> it = listeCarteRumeur.iterator();
		while(it.hasNext()) {
			//on r�cup�re la carte de l'it�ration
			CarteRumeur c = it.next();
			//on regarde si la carte �tait face cach�e ou non
			boolean faceCache = c.getFaceCache();
			//on cache la carte
			c.setFaceCache(true);
			//on compte le nombre de carte jouable dans la main du joueur
			int nbCarteJouableEnMain = Partie.getPartie().getJoueurQuiDoitJouer().listeCarteRumeurJouableEnMain(true, true).size();
			//on ajoute la carte dans la main du joueur
			Partie.getPartie().getJoueurQuiDoitJouer().ajouterCarteRumeurEnMain(c);
			//on regarde si on veut que la carte soit jouable ou non
			if(jouable == true) {
				//si on veut que la carte soit joauble
				//on regarde si le nouveau nombre de carte jouable est sup�rieur � celui avant d'avoir prit la carte
				if(nbCarteJouableEnMain<Partie.getPartie().getJoueurQuiDoitJouer().listeCarteRumeurJouableEnMain(true, true).size()) {
					//si cette carte est jouable et que l'on veut une carte jouable, alors on la retire de sa main
					Partie.getPartie().getJoueurQuiDoitJouer().retirerCarteRumeurEnMain(c);
					//et on lui remets sa face cach�
					c.setFaceCache(faceCache);
					//et on la retourne
					return c;
				}
			}else {
				//dans le cas o� nous voulons une carte qui n'est pas jouable
				if(nbCarteJouableEnMain<Partie.getPartie().getJoueurQuiDoitJouer().listeCarteRumeurJouableEnMain(true, true).size()) {
					//on regarde si le nombre de carte jouable n'a pas augment�e suite � l'ajout de la carte dans la main
					//s'il n'a pas boug� �a veut dire que la carte n'est pas jouable
					//on peut alors la retirer de la main du joueur et la retourner
					Partie.getPartie().getJoueurQuiDoitJouer().retirerCarteRumeurEnMain(c);
					//et on lui remets sa face cach�
					c.setFaceCache(faceCache);
					return c;
				}
			}
			//on retire la carte de la main du joueur
			Partie.getPartie().getJoueurQuiDoitJouer().retirerCarteRumeurEnMain(c);
			//et on lui remets sa face cach�
			c.setFaceCache(faceCache);
		}
		return listeCarteRumeur.get(0);
	}
	
	/**
	 * M�thode permettant � un joueur virtuel de choisir une carte rumeur parmi la liste pr�sente dans l'attribut selectionCarteRumeurParmiListe de la classe Partie.
	 * Si le joueur virtuel doit d�fausser une carte rumeur, alors il va d�fausser une carte rumeur qu'il ne peut pas jouer.
	 * Au contraire, s'il doit r�cup�rer une carte rumeur parmi une liste, alors il va en choisir une qui peut utiliser.
	 * Sinon il en choisit une al�atoirement.
	 * Une fois la carte rumeur choisie, elle est stock�e dans l'attribut carteRumeurSelectionne de la classe Partie.
	 */
	public void choisirCarteRumeurParmiListe() {
		//on r�cup�re la liste parmi laquelle le joueur virtuel peut choisir
		ArrayList<CarteRumeur> listeCarteRumeur = Partie.getPartie().getSelectionCarteRumeurParmiListe();
		//on regarde que la liste n'est pas vide, 
		if(!listeCarteRumeur.isEmpty()) {
			//on regarde si la s�lection est d� pour d�fausser une carte rumeur
			if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_DEFAUSSER)) {
				//s'il doit d�fausser une carte, on en profite pour se s�parer d'une carte rumeur dont il ne peut pas utiliser
				Partie.getPartie().setCarteRumeurSelectionne(rechercheCarteRumeurJouableOuNonParmiListe(listeCarteRumeur, false));
			//on regarde si la s�lection permet de r�cup�rer une carte
			}else if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_RECUPERER)){
				//si elle permet de r�cup�rer une carte, alors dans ce cas si, on lui retourne si possible une carte qu'il peut jouer
				Partie.getPartie().setCarteRumeurSelectionne(rechercheCarteRumeurJouableOuNonParmiListe(listeCarteRumeur, true));
			}else {
				//pour tout les autres cas, on tire au hasard une carte rumeur parmit la liste
				Partie.getPartie().setCarteRumeurSelectionne(listeCarteRumeur.get((int)(Math.random()*listeCarteRumeur.size())));
			}
		}

	}
	
	/**
	 * M�thode permettant � un joueur virtuel de choisir un entier compris entre les bornes de l'intervalle stock� dans l'attribut selectionEntierIntervale de la classe Partie.
	 * L'entier est choisi en fonction du statut de la partie.
	 * Par exemple, s'il doit choisir entre r�v�ler son identit� alors que c'est un villageois et utiliser l'effet sorci�re d'une carte rumeur.
	 * Alors on regarde, s'il a un avantage a r�v�l� son identit� tout de suite.
	 * S'il ne peut pas utiliser davantage de carte rumeur une fois qu'il r�v�le son identit�. Le joueur n'a alors aucun int�r�t � r�v�ler son identit�.
	 * Dans le cas o� r�v�ler son identit� serait utilie pour le joueur pour pouvoir d�bloquer certaine carte.
	 * Dans ce cas si, le joueur villageois a une chance sur deux de r�v�ler son identit�, on laisse la part d'al�atoire pour ce cas si.
	 * Un autre exemple, s'il a le choix entre accuser ou utiliser l'effet chasseur. Alors dans ce cas-si, pour qu'il conserve le maximum de carte rumeur, il va alors accuser une autre personne d'�tre une sorci�re.
	 * Une fois l'entier choisi, il est stock� dans l'attribut entierSelectionne de la classe Partie.
	 */
	public void choisirParmiIntervalleEntier() {
		//m�thode qui permet de choisir un nombre appartenant � un intervalle sous certaine condition. 
		//on r�cup�re l'instance de la partie
		Partie p = Partie.getPartie();
		//on regarde si la liste qui comptient les bornes de l'intervalle n'est pas vide et qu'elle comptient les deux bornes au minimum
		if(p.getSelectionEntierIntervale().size() >=2) {
			//dans le cas o� la liste contient au minimum deux valeur, on r�cup�re les bornes inf�rieur et sup�rieur de l'intervalle
			int borneInferieur = p.getSelectionEntierIntervale().get(0);
			int borneSuperieur = p.getSelectionEntierIntervale().get(1);
			//la borne sup�rieur doit etre sup�ieur ou �gale � la borne inf�rieur
			if(borneInferieur>borneSuperieur) {
				//s'il y a un probl�me avec les bornes de l'intervalle
				//on inverse alors les borne
				//de tel sorte que la borne inf�rieur soit la plus petite des deux valleur
				//et la borne sup�rieur la borne la plus grande
				int buff = borneSuperieur;
				borneSuperieur=borneInferieur;
				borneInferieur=buff;
			}
			
			//le joueur virtuel ne dois pas r�v�ler son identit� s'il s'agit d'une sorci�re
			if((p.getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_EFFET_SORCIERE) ||p.getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_DEFAUSSER) ) && Partie.getPartie().getJoueurQuiDoitJouer().getCarteIdentite().getRole().equals(EnumRole.SORCIERE)) {
				//on regarde si c'est une sorci�re et qu'il doit choisir entre r�v�ler son identit� ou utiliser une carte effet sorci�re
				//si c'est le cas, on retourne alors la deuxi�me valeur qui est d'utiliser une carte
				p.setEntierSelectionne(borneSuperieur);
			}else if(p.getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_EFFET_SORCIERE) ||p.getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_DEFAUSSER)){
				//dans le cas que c'est un villageois et qu'il a le choix de reveler son identite ou avoir recour a une carte (utiliser ou defausser)
				//on regarde s'il a un interet � r�v�ler son identit� vis � vis des effets de cartes que cela va lui procurer
				//on compte combien de carte il peut utiliser effet chasseur et sorci�re
				int nbCarteUtilisableParmitMain = Partie.getPartie().getJoueurQuiDoitJouer().listeCarteRumeurJouableEnMain(true, true).size();
				//on simule qu'il r�v�le son identit�
				Partie.getPartie().getJoueurQuiDoitJouer().setEstRevele(true);
				//on regarde s'il a gagn� la posibilit� d'utiliser des nouvelle carte, si ce n'est pas le cas alors il ne r�v�le pas tout de suite son identit�
				if(nbCarteUtilisableParmitMain<Partie.getPartie().getJoueurQuiDoitJouer().listeCarteRumeurJouableEnMain(true, true).size()) {
					//dans le cas o� r�v�ler son identit� serait utilie pour le joueur pour pouvoir d�bloquer certaine carte
					//dans ce cas si, le joueur villageois a une chance sur deux de r�v�ler son identit�, on laisse la part d'al�atoire pour ce cas si
					Partie.getPartie().getJoueurQuiDoitJouer().setEstRevele(false);
					p.setEntierSelectionne(borneInferieur+(int)(Math.random()*(borneSuperieur+1-borneInferieur)));
				}else {
					//s'il n'a pas gagn� la possibilit� d'utiliser des nouevlle carte
					//il ne r�v�le alors pas maintenant sont identit� mais utilise le second choix
					//car il n'y a aucune utilit� de le r�v�ler maintenant
					Partie.getPartie().getJoueurQuiDoitJouer().setEstRevele(false);
					p.setEntierSelectionne(borneSuperieur);
				}
				
			}else if(p.getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_ACCUSATION_OU_EFFET_CHASSEUR)) {
				//dans le cadre o� il doit choisir entre accuser ou utiliser une carte rumeur effet chasseur
				//dans ce cas si, il d�cide d'accuser une autre personne pour conserver le maximum de carte 
				p.setEntierSelectionne(borneInferieur);
			}else {
				//dans tout les autres cas, il tire une valeur al�atorie compris entre les deux bornes
				p.setEntierSelectionne(borneInferieur+(int)(Math.random()*(borneSuperieur+1-borneInferieur)));
			}
		}
	}
	
	/**
	 * M�thode permettant � un joueur virtuel de choisir son r�le : Villageois ou Sorci�re.
	 * Il le choisi de mani�re al�atoire.
	 * Puis il a une chance sur deux de conserver la m�me strat�gie, sinon il change de mani�re al�atoire de strat�gie.
	 */
	public void choisirRole() {
		//m�thode qui permet au joueur virtuel de choisir son r�le de mani�re al�atoire
		//on tire de mani�re al�atoire le role de l'IA
		ArrayList<EnumRole> listeDesRoles = new ArrayList<>(Arrays.asList(EnumRole.VILLAGEOIS,EnumRole.SORCIERE));
		Partie.getPartie().getJoueurQuiDoitJouer().getCarteIdentite().setRole(listeDesRoles.get((int)(Math.random()*listeDesRoles.size())));
		//on mets a jour comme quoi il n'est plus r�v�l�
		Partie.getPartie().getJoueurQuiDoitJouer().setEstRevele(false);
		//on affiche a tout les joueurs que le joueur virtuel a choisit son role
		Partie.getPartie().ajouterUnMessage(2,"> "+Partie.getPartie().getJoueurQuiDoitJouer().getNomJoueur()+" (IA), vient de choisir son r�le");
		//on modifie la strat�gie du joueur virtuel avec une probabilit� d'une chance sur deux, car il a la meilleur strat�gie
		if(0 == (int)(Math.random()*2)) {
			((JoueurVirtuel)Partie.getPartie().getJoueurQuiDoitJouer()).attributionStrategie();
		}
		
	}
}
