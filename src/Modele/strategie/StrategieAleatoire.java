package Modele.strategie;

import java.util.ArrayList;
import java.util.Arrays;

import Modele.CarteRumeur;
import Modele.Joueur;
import Modele.JoueurVirtuel;
import Modele.Partie;
import Modele.enums.EnumRole;
import Modele.enums.EnumStatutPartie;

/**
 * Classe d�signant une des strat�gie qu'un joueur virtuel peut poss�der. Les choix que ce joueur effectue d�pendront de la strat�gie qu'il poss�de.
 * Cette strat�gie permet au joueur virtuel de r�aliser des choix compl�tement al�atoires, que ce soit un choix de joueur, d'entier, de carte rumeur, ...
 * � l'exception, il va tout faire pour ne pas r�v�ler son identit� alors que c'est une sorci�re, pour rallonger le plus possible le temps de jeu.
 * @author Charpentier
 */
public class StrategieAleatoire implements StrategieJoueurVirtuel{

	/**
	 * M�thode permettant � un joueur virtuel de choisir un joueur parmi la liste pr�sente dans l'attribut selectionJoueurParmiListe de la classe Partie.
	 * Le joueur est choisi de mani�re al�atoire parmi cette liste.
	 * Une fois le joueur choisi, il est stock� dans l'attribut joueurSelectionne de la classe Partie.
	 */
	public void choisirJoueurParmiListe() {
		//m�thode qui permet de choisir un joueur de mani�re al�atoire parmi une liste
		//on r�cup�re la liste parmi laquelle le joueur virtuel peut choisir
		ArrayList<Joueur> listeJoueur = Partie.getPartie().getSelectionJoueurParmiListe();
		//on regarde que la liste n'est pas vide
		if(!listeJoueur.isEmpty()) {
			//on tire un joueur al�atoire parmit la liste et one le stocke dans l'attribut ad�quat de la classe partie
			Partie.getPartie().setJoueurSelectionne(listeJoueur.get((int)(Math.random()*listeJoueur.size())));
		}
		
	}
	
	/**
	 * M�thode permettant � un joueur virtuel de choisir une carte rumeur parmi la liste pr�sente dans l'attribut selectionCarteRumeurParmiListe de la classe Partie.
	 * La carte rumeur est choisie de mani�re al�atoire parmi cette liste.
	 * Une fois la carte rumeur choisie, elle est stock�e dans l'attribut carteRumeurSelectionne de la classe Partie.
	 */
	public void choisirCarteRumeurParmiListe() {
		//m�thode qui permet de choisir une carte rumeur de mani�re al�atoire parmi une liste
		//on r�cup�re la liste parmi laquelle le joueur virtuel peut choisir
		ArrayList<CarteRumeur> listeCarteRumeur = Partie.getPartie().getSelectionCarteRumeurParmiListe();
		//on regarde que la liste n'est pas vide
		if(!listeCarteRumeur.isEmpty()) {
			//on tire une carte rumeur al�atoire parmit la liste et one la stocke dans l'attribut ad�quat de la classe partie
			Partie.getPartie().setCarteRumeurSelectionne(listeCarteRumeur.get((int)(Math.random()*listeCarteRumeur.size())));
		}
	}
	
	/**
	 * M�thode permettant � un joueur virtuel de choisir un entier compris entre les bornes de l'intervalle stock� dans l'attribut selectionEntierIntervale de la classe Partie.
	 * L'entier est choisi de mani�re al�atoire entre ces bornes. Sauf dans le cas o� il doit r�v�ler son identit�, il va tout faire pour l'�viter.
	 * Une fois l'entier choisi, il est stock� dans l'attribut entierSelectionne de la classe Partie.
	 */
	public void choisirParmiIntervalleEntier() {
		//m�thode qui permet de choisir un nombre al�atoire compris dans un intervalle sous certaine condition. 
		//on regarde sir la liste des bornes de l'intervalle n'est pas vide et qu'elle comptient les deux valeur des bornes
		if(Partie.getPartie().getSelectionEntierIntervale().size()>=2) {
			//on r�cup�re les bornes de l'intervalle
			int borneInferieur = Partie.getPartie().getSelectionEntierIntervale().get(0);
			int borneSuperieur = Partie.getPartie().getSelectionEntierIntervale().get(1);
			//la borne sup�rieur doit etre sup�ieur ou �gale � la borne inf�rieur
			if(borneInferieur>borneSuperieur) {
				//s'il y a un probl�me avec les bornes de l'intervalle
				//on inverse alors les borne
				//de tel sorte que la borne inf�rieur soit la plus petite des deux valleur
				//et la borne sup�rieur la borne la plus grande
				int buff = borneSuperieur;
				borneSuperieur=borneInferieur;
				borneInferieur=buff;
			}
			
			//le joueur virtuel ne dois pas r�v�ler son identit� s'il s'agit d'une sorci�re
			if((Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_EFFET_SORCIERE) ||Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_DEFAUSSER) ) && Partie.getPartie().getJoueurQuiDoitJouer().getCarteIdentite().getRole().equals(EnumRole.SORCIERE)) {
				//on regarde si c'est une sorci�re et qu'il doit choisir entre r�v�ler son identit� ou utiliser une carte effet sorci�re
				//si c'est le cas, on retourne alors la deuxi�me valeur qui est d'utiliser une carte
				Partie.getPartie().setEntierSelectionne(borneSuperieur);
			}else {
				//dans tout les autres cas, il tire une valeur al�atorie compris entre les deux bornes
				Partie.getPartie().setEntierSelectionne(borneInferieur+(int)(Math.random()*(borneSuperieur+1-borneInferieur)));
			}
			
		}
		
	}
	
	/**
	 * M�thode permettant � un joueur virtuel de choisir son r�le : Villageois ou Sorci�re.
	 * Il le choisi de mani�re al�atoire.
	 * Puis change de strat�gie de mani�re al�atoire.
	 */
	public void choisirRole() {
		//m�thode qui permet au joueur virtuel de choisir son r�le de mani�re al�atoire
		//on tire de mani�re al�atoire le role de l'IA
		ArrayList<EnumRole> listeDesRoles = new ArrayList<>(Arrays.asList(EnumRole.VILLAGEOIS,EnumRole.SORCIERE));
		Partie.getPartie().getJoueurQuiDoitJouer().getCarteIdentite().setRole(listeDesRoles.get((int)(Math.random()*listeDesRoles.size())));
		//on mets a jour comme quoi il n'est plus r�v�l�
		Partie.getPartie().getJoueurQuiDoitJouer().setEstRevele(false);
		//on affiche a tout les joueurs que le joueur virtuel a choisit son role
		Partie.getPartie().ajouterUnMessage(2,"> "+Partie.getPartie().getJoueurQuiDoitJouer().getNomJoueur()+" (IA), vient de choisir son r�le");
		//on modifie la strat�gie du joueur virtuel a chaque fois qu'il change de r�le
		((JoueurVirtuel)Partie.getPartie().getJoueurQuiDoitJouer()).attributionStrategie();
	}
}
