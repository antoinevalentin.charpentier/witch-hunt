package Modele.strategie;

/**
 * Interface regroupant les diff�rents choix qu'un joueur virtuel peut r�aliser pour simuler le comportement d'un joueur r�el.
 * La mani�re de comment les choix sont effectu�s par un joueur virtuel constituent la strat�gie qui lui est associ�.
 * @author Charpentier
 */
public interface StrategieJoueurVirtuel {
	//les m�thodes de l'interface
	/**
	 * M�thode permettant � un joueur virtuel de choisir un joueur parmi la liste pr�sente dans l'attribut selectionJoueurParmiListe de la classe Partie.
	 */
	public void choisirJoueurParmiListe();
	/**
	 * M�thode permettant � un joueur virtuel de choisir une carte rumeur parmi la liste pr�sente dans l'attribut selectionCarteRumeurParmiListe de la classe Partie.
	 */
	public void choisirCarteRumeurParmiListe();
	/**
	 * M�thode permettant � un joueur virtuel de choisir un entier compris entre les bornes de l'intervalle stock� dans l'attribut selectionEntierIntervale de la classe Partie.
	 */
	public void choisirParmiIntervalleEntier();
	/**
	 * M�thode permettant � un joueur virtuel de choisir son r�le : Villageois ou Sorci�re.
	 */
	public void choisirRole();
}
