package Modele.strategie;

import java.util.ArrayList;

import Modele.CarteRumeur;
import Modele.Joueur;
import Modele.JoueurVirtuel;
import Modele.Partie;
import Modele.enums.EnumRole;
import Modele.enums.EnumStatutPartie;

/**
 * Classe d�signant une des strat�gie qu'un joueur virtuel peut poss�der. Les choix que ce joueur effectue d�pendront de la strat�gie qu'il poss�de.
 * Cette strat�gie permet au joueur virtuel de r�aliser tout le temps le premier choix qu'il peut faire, que ce soit un choix de joueur, d'entier, de carte rumeur, ...
 * Cela a pour impact par exemple d'accuser tout le temps au lieu d'utiliser un effet chasseur d'une carte rumeur, de se focaliser sur la premi�re personne de la liste, ...
 * � l'exception, il va tout faire pour ne pas r�v�ler son identit� alors que c'est une sorci�re, pour rallonger le plus possible le temps de jeu.
 * @author Charpentier
 */
public class StrategieAgressive implements StrategieJoueurVirtuel{
	/**
	 * M�thode permettant � un joueur virtuel de choisir un joueur parmi la liste pr�sente dans l'attribut selectionJoueurParmiListe de la classe Partie.
	 * Le joueur est choisi de mani�re al�atoire parmi cette liste.
	 * Dans le cadre de cette strat�gie, on retourne le premier joueur de la liste � chaque fois.
	 * Choisir tout le temps le premier joueur, le joueur virtuel choisi tout le temps la m�me personne pour une m�me liste.
	 * Par exemple, une cons�quence directe de ce choix, il choisit d'accuser tout le temps le m�me joueur d'�tre une sorci�re tant qu'il n'a pas r�v�l� son identit�.
	 * Il force tout le temps sur le m�me joueur jusqu'� ce qu'il abandonne (tant que c'est possible).
	 * Ainsi, le joueur virtuel s'acharne tout le temps les m�mes personnes selon les circonstances.
	 * Dans le cas o� il devrait choisir le joueur suivant, pour qu'il ne donne pas le tour suivant au joueur qu'il accuse possiblement tout le temps, le joueur virtuel choisi le dernier joueur de la liste pour �viter des repr�sailles.
	 * Une fois le joueur choisi, il est stock� dans l'attribut joueurSelectionne de la classe Partie.
	 */
	public void choisirJoueurParmiListe() {
		//m�thode qui permet de choisir un joueur parmi une liste
		//on r�cup�re la liste des joueurs parmi laquelle le joueur virtuel peut choisir
		ArrayList<Joueur> listeJoueur = Partie.getPartie().getSelectionJoueurParmiListe();
		//on regarde que la liste n'est pas vide, si elle est vide on retourne null
		if(!listeJoueur.isEmpty()) {
			//pour ne pas donner le tour suivant � la personne sur laquelle le joueur virtuel est focus, il choisit alors la derni�re personne de la liste et non la premi�re
			//cela lui �vite d'avoir des repr�sailles au maximum
			//on regarde s'il doit choisir le joueur suivant qui va jouer
			if(Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_JOUEUR_SUIVANT)) {
				//dans ce cas si, on retourne la derni�re personne de la liste pour que la premi�re personne de la liste ne puisse pas se venger en accusant le joueur virtuel qui a la strat�gie aggr�ssive
				Partie.getPartie().setJoueurSelectionne(listeJoueur.get(listeJoueur.size()-1));
			}else {
				//dans les autres cas, on retourne le premier joueur de la liste
				Partie.getPartie().setJoueurSelectionne(listeJoueur.get(0));
			}
		}
	}
	
	/**
	 * M�thode permettant � un joueur virtuel de choisir une carte rumeur parmi la liste pr�sente dans l'attribut selectionCarteRumeurParmiListe de la classe Partie.
	 * La carte rumeur est choisie correspond tout le temps � la premi�re de cette liste.
	 * Une fois la carte rumeur choisie, elle est stock�e dans l'attribut carteRumeurSelectionne de la classe Partie.
	 */
	public void choisirCarteRumeurParmiListe() {
		//m�thode qui permet de choisir une carte rumeur parmi une liste
		//Dans le cas de cette strat�gie, il choisit tout le temps la premi�re carte
		//on r�cup�re la liste des cartes rumeur parmi laquelle le joueur virtuel peut choisir
		ArrayList<CarteRumeur> listeCarteRumeur = Partie.getPartie().getSelectionCarteRumeurParmiListe();
		//on regarde que la liste n'est pas vide
		if(!listeCarteRumeur.isEmpty()) {
			//on tire la premi�re carte de la liste
			Partie.getPartie().setCarteRumeurSelectionne(listeCarteRumeur.get(0));
		}
	}
	
	/**
	 * M�thode permettant � un joueur virtuel de choisir un entier compris entre les bornes de l'intervalle stock� dans l'attribut selectionEntierIntervale de la classe Partie.
	 * Dans le cas de cette strat�gie, on retourne tout le temps la borne inf�rieure comme valeur d'entier.
	 * Sauf dans le cas o� il devrait r�v�ler son identit� et qu'il s'agit d'une sorci�re. Dans ce cas si, on retourne la borne sup�rieure.
	 * Retourner la borne inf�rieure va permettre au joueur virtuel d'accuser tout le temps (car 1 = accuser , 2=carte effet chasseur) et ne jamais utiliser l'effet chasseur d'une de ces cartes rumeur.
	 * Cependant, dans le cas qu'il doit r�v�ler son identit� et que c'est un villageois, alors il r�v�le son identit� pour avoir acc�s � davantage de cartes rumeur.
	 * Il prend ainsi tout le temps les m�mes d�cisions, � la mani�re d'une personne agressive.
	 * Une fois l'entier choisi, il est stock� dans l'attribut entierSelectionne de la classe Partie.
	 */
	public void choisirParmiIntervalleEntier() {
		//m�thode qui permet de choisir un nombre compris dans un intervalle sous certaine condition. 
	
		//on regarde si la liste qui comptient les bornes de l'intervalle n'est pas vide et qu'elle comptient les deux bornes au minimum
		if(Partie.getPartie().getSelectionEntierIntervale().size() >=2) {
			//on r�cup�re les bornes de l'intervalle
			int borneInferieur = Partie.getPartie().getSelectionEntierIntervale().get(0);
			int borneSuperieur = Partie.getPartie().getSelectionEntierIntervale().get(1);
			//la borne sup�rieur doit etre sup�ieur ou �gale � la borne inf�rieur
			if(borneInferieur>borneSuperieur) {
				//s'il y a un probl�me avec les bornes de l'intervalle
				//on inverse alors les borne
				//de tel sorte que la borne inf�rieur soit la plus petite des deux valleur
				//et la borne sup�rieur la borne la plus grande
				int buff = borneSuperieur;
				borneSuperieur=borneInferieur;
				borneInferieur=buff;
			}
			
			//le joueur virtuel ne dois pas r�v�ler son identit� s'il s'agit d'une sorci�re
			if((Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_EFFET_SORCIERE) ||Partie.getPartie().getStatutPartie().equals(EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_DEFAUSSER) ) && Partie.getPartie().getJoueurQuiDoitJouer().getCarteIdentite().getRole().equals(EnumRole.SORCIERE)) {
				//on regarde si c'est une sorci�re et qu'il doit choisir entre r�v�ler son identit� ou utiliser une carte effet sorci�re
				//si c'est le cas, on retourne alors la deuxi�me valeur qui est d'utiliser une carte
				Partie.getPartie().setEntierSelectionne(borneSuperieur);
			}else{
				//dans tout les autres cas, il retourne la valeur de la borne inf�rieur
				Partie.getPartie().setEntierSelectionne(borneInferieur);
			}
		}
		
	}
	/**
	 * M�thode permettant � un joueur virtuel de choisir son r�le : Villageois ou Sorci�re.
	 * Il conserve le m�me r�le mais change de mani�re al�atoire de strat�gie.
	 */
	public void choisirRole() {
		//m�thode qui permet au joueur virtuel de choisir son r�le (il conserve le m�me mais peut changer de strat�gie pour la manche)
		//Dans cette strat�gie, le joueur virtuel conserve le m�me r�le qu'il avait � la manche d'avant (villageois si c'est la premi�re manche)
		//on mets a jour comme quoi il n'est plus r�v�l�, dans le cas des autres manche que la premi�re et qu'il a �tait r�v�l� � la manche pr�c�dente
		Partie.getPartie().getJoueurQuiDoitJouer().setEstRevele(false);
		//on affiche a tout les joueurs que le joueur virtuel a choisit son (nouveau) role
		Partie.getPartie().ajouterUnMessage(2,"> "+Partie.getPartie().getJoueurQuiDoitJouer().getNomJoueur()+" (IA), vient de choisir son r�le");
		//on modifie la strat�gie du joueur virtuel a chaque fois qu'il change de r�le
		((JoueurVirtuel)Partie.getPartie().getJoueurQuiDoitJouer()).attributionStrategie();
	}

}
