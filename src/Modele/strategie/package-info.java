/**
 * Package destin� � regrouper l'ensemble des diff�rents �l�ments en relation avec l'intelligence artificelle des joueurs virtuels.
 * Ils peuvent prendre 3 strat�gies diff�rentes :
 * <ul>
 * 		<li>StrategieAleatoire : Chaque choix est effectu� de mani�re al�atoire..</li>
 * 		<li>StrategieAgressive : Le joueur virtuel choisi tout le temps le premier �l�ment de la liste. Cela a pour impact par exemple d'accuser tout le temps au lieu d'utiliser un effet chasseur d'une carte rumeur.</li>
 * 		<li>StrategieAdaptative : Il s'agit de la strat�gie qui se rapproche le plus possible de ce qu'un joueur r�el fairait. Il r�alise des choix en fonction de l'environnement du jeu et du contexte.</li>
 * </ul>
 * Un point commun a ces trois strat�gie, si un joueur doit r�v�ler son identit� alors que c'est une sorci�re, il va tout faire pour l'�viter.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
package Modele.strategie;