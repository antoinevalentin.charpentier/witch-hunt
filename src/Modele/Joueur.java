package Modele;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;

import Modele.enums.EnumJouabiliteEffetCarteRumeur;
import Modele.enums.EnumRole;
import Modele.enums.EnumStatutPartie;

/**
 * Repr�sente un joueur qui joue � une partie de Witch Hunt
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class Joueur {
	//les attributs
	/**
	 * Nombre de point obtenu par le joueur
	 */
	private int score;
	/**
	 * Bool�en qui indique si le joueur � r�v�ler ou non son identit� au cours de la manche actuelle
	 */
	private boolean estRevele;
	/**
	 * D�signe le nom du joueur. Il ne peut pas y avoir deux joueurs dans la partie avec le m�me nom.
	 */
	private String nomJoueur;
	/**
	 * Bool�en qui permet de savoir si un joueur est un joueur virtuel ou r�el. (True = joueur virtuel, False = joueur r�el)
	 */
	private boolean estJoueurVirtuel;
	
	/**
	 * Liste de cartes rumeur dont le joueur dispose. Ces cartes regroupes � la fois les cartes rumeur r�v�l�es (donc utilis�) du joueur et celle non r�v�l�e.
	 */
	private ArrayList<CarteRumeur> listeCartesRumeurEnMain;
	/**
	 * Permet de stocker la carte identit� du joueur, afin qu'il puisse s�lectionner son identit� en d�but de manche. On peut retrouver le r�le du joueur � travers cette carte.
	 */
	private CarteIdentite carteIdentite;
	/**
	 * On stocke dans cette attribut la derni�re carte rumeur que le joueur � utilis�. Il a normalement d� utiliser des effets de cette carte.
	 */
	private CarteRumeur derniereCarteRumeurJouee;
	
	
	//constructeurs
	/**
	 * Constructeur qui permet d'instancier un Joueur en indiquant son nom (identifiant unique dans la partie), et s'il s'agit ou no d'un joueur virtuel
	 * Exemple : Joueur roger = new Joueur("Roger",False); Permet d'instancier un joueur r�el (controler pour une personne physique) se nommant Roger.
	 * @param nomJoueur On indique le nom du joueur que l'on veut instancier. 
	 * @param estJoueurVirtuel Bool�en qui permet d'indiquer s'il s'agit d'un joueur virtuel ou r�el (True = joueur virtuel, False = joueur r�el)
	 */
	public Joueur(String nomJoueur, boolean estJoueurVirtuel) {
		//constructeur d'un joueur
		//on dit qu'il n'a pas enore gagn� de points
		this.score = 0;
		//il n'a pas encore r�v�l� son identit�
		this.estRevele = false;
		//on modifie son nom 
		this.nomJoueur = nomJoueur;
		//on dit si c'est un joueur r�el ou virtuel
		this.estJoueurVirtuel = estJoueurVirtuel;
		//on dit qu'il n'a ps encore de caret rumeur
		this.listeCartesRumeurEnMain = new ArrayList<>();
		this.carteIdentite = null;
		//on dit qu'il n'a pas encore utilis� de carte rumeur
		this.derniereCarteRumeurJouee = null;
	}
	
	//getter
	/**
	 * M�thode qui permet de r�cup�rer le nombre de point que le joueur � obtenue au cours de la partie en cours.
	 * @return Le nombre de point
	 */
	public int getScore() {
		return this.score;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer la carte rumeur que le joueur � utilis� ces effets. 
	 * @return une carte rumeur. Il y a de forte chance que cette carte soit r�v�l�, � moins qu'il l'a r�cup�r� dans sa main. 
	 */
	public CarteRumeur getDerniereCarteRumeurJouee() {
		return derniereCarteRumeurJouee;
	}

	/**
	 * M�thode qui permet de savoir si un joueur � r�v�ler son identit� ou non.
	 * @return Un bool�en qui prend soit la valeur True si le joueur en question � r�v�ler son identit�, ou soit False s'il n'a pas encore r�v�l� son identit�
	 */
	public boolean getEstRevele() {
		return this.estRevele;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer le nom du joueur  
	 * @return Une chaine de caract�re qui comptient le nom du joueur
	 */
	public String getNomJoueur() {
		return this.nomJoueur;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer l'ensemble des cartes rumeurs du joueur. Ces cartes peuvent appartenir � la main du joueur (carte non r�v�l�) mais �galement celle que le joueur � utilis� mais pas d�fauss� (carte r�v�l�)
	 * @return une liste de cartes rumeur d�tenues par le joueur
	 */
	public ArrayList<CarteRumeur> getListeCartesRumeurEnMain(){
		return this.listeCartesRumeurEnMain;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer la carte d'identit� du joueur. A partir de cette carte, il est possible par exemple de r�cup�rer son r�le ou de le d�finir.
	 * @return la carte d'identit� du joueur
	 */
	public CarteIdentite getCarteIdentite() {
		return carteIdentite;
	}
	
	/**
	 * M�thode qui permet de savoir si le joueur est un joueur r�el ou virtuel
	 * @return Un bool�en qui prend la valeur True s'il s'agit d'un joueur virtuel, False s'il s'agit d'un joueur r�el
	 */
	public boolean getEstJoueurVirtuel() {
		return this.estJoueurVirtuel;
	}
	
	//settter
	/**
	 * M�thode qui permet de modifier le nombre de point associ� au joueur durant la partie
	 * @param score le nouveau nombre de point
	 */
	public void setScore(int score) {
		this.score = score;
	}
	
	/**
	 * M�thode qui permet de modifier l'attribut associ� � la derni�re carte rumeur jou�e par le joueur.
	 * @param derniereCarteRumeurJouee La derni�re carte rumeur jou�e par le joueur
	 */
	public void setDerniereCarteRumeurJouee(CarteRumeur derniereCarteRumeurJouee) {
		this.derniereCarteRumeurJouee = derniereCarteRumeurJouee;
	}
	
	/**
	 * M�thode qui permet de changer l'�tat du joueur. Un joueur peut r�v�ler son identit� durant la partie, il devient alors r�v�l�. De plus, a chaque d�but de manche, son r�le n'est pas r�v�l�.  
	 * @param estRevele Le bool�en � fournir est True dans le cas o� le joueur � r�v�ler son identit�, False = si l'on veut recacher son identit�
	 */
	public void setEstRevele(boolean estRevele) {
		this.estRevele = estRevele;
	}
	
	/**
	 * M�thode qui permet de modifier le nom du joueur
	 * @param nomJoueur le nouveau nom du joueur
	 */
	public void setNomJoueur(String nomJoueur) {
		this.nomJoueur = nomJoueur;
	}
	
	/**
	 * M�thode qui permet de d�finir la liste des cartes rumeurs dont le joueur dispose � partir d'une autre liste pass� en param�tre. 
	 * @param listeCartesEnMain une liste de carte rumeur que le joueur va disposer
	 */
	public void setListeCartesRumeurEnMain(ArrayList<CarteRumeur> listeCartesEnMain) {
		this.listeCartesRumeurEnMain = listeCartesEnMain;
	}
	
	/**
	 * M�thode qui permet de donner une carte identit� particuli�re au joueur
	 * @param carteIdentite la carte identit� � donenr au joueur
	 */
	public void setCarteIdentite(CarteIdentite carteIdentite) {
		this.carteIdentite = carteIdentite;
	}
	
	/**
	 * M�thode qui permet de d�finir s'il s'agit d'une joueur r�el ou virtuel. Par exemple on peut changer le type de joueur en cours de route, si un joueur r�el d�cidequ'un joueur virtuel prend la suite de la partie. 
	 * @param estJoueurVirtuel bool�en indiquant sil s'agit d'un joueur virtuel ou non. Il doit avoir la valeur True dans le cas d'un joueur virtuel, sinon False pour un joueur r�el.
	 */
	public void setEstJoueurVirtuel(boolean estJoueurVirtuel) {
		this.estJoueurVirtuel = estJoueurVirtuel;
	}
	
	/**
	 * M�thode qui retourne l'ensemble des carte rumeur que le joueur � r�v�l�. Ils correspondent � des cartes rumeur utilis� par le joueur
	 * @return Une liste de carte rumeurs que le joueur � r�v�l�
	 */
	public ArrayList<CarteRumeur> getListeCarteRumeurRevele() {
		//m�thode qui permet de r�cup�rer l'ensemble des cartes rumeur r�v�l�e, que l'on stocke dans la liste suivante
		ArrayList<CarteRumeur> listeCarteRumeurRevelee = new ArrayList<>();
		//on parcours l'ensemble des carte que le joueur poss�de en main
		for(CarteRumeur c:this.getListeCartesRumeurEnMain()) {
			//on regarde si la carte n'est pas face cach�, c'est a dire qu'elle est visible par les autres donc r�vel�e
			if(c.getFaceCache() == false) {
				//si c'est le c as alors on l'ajoute dans la liste des cartes rumeurs r�vel�e
				listeCarteRumeurRevelee.add(c);
			}
		}
		//on retourne la liste avec l'ensemble des cartes rumeurs r�vel�e
		return listeCarteRumeurRevelee;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer toute les carte rumeur que le joueur dispose dans sa main, ces cartes ne sont pas r�v�l�es. Il peut encore utilis� les effets de ces cartes.
	 * @return Une liste de carte rumeur encore non r�v�l�.
	 */
	public ArrayList<CarteRumeur> listeCarteRumeurNonReveleEnMain(){
		//m�thode qui permet de r�cup�rer l'enssemble de carte rumeur pas r�v�l�
		//on parcours l'ensemble des carte rumeur de la main du joueur (r�v�l� ou non)
		Iterator<CarteRumeur> it = this.getListeCartesRumeurEnMain().iterator();
		ArrayList<CarteRumeur> listeCarteRumeurNonRevele = new ArrayList<>();
		while(it.hasNext()) {
			//on r�cup�re la carte de l'it�ration
			CarteRumeur c = it.next();
			//on regarde si elle n'est pas r�v�l�
			if(c.getFaceCache() == true) {
				//on l'ajoute � la liste des carte retourn�
				listeCarteRumeurNonRevele.add(c);
			}
		}
		//on retourne la liste des cartes non r�v�l�
		return listeCarteRumeurNonRevele;
	}
	
	/**
	 * M�thode qui retourne la liste des cartes rumeurs jouable par le joueur � un certain moment du jeu en foncion de s'il veut utiliser l'effet chasseur ou l'effet sorci�re de la carte.
	 * Il peut tr�s bien pouvoir utiliser l'effet chasseur d'une carte rumeur sans pouvoir pour autant utiliser son effet sorci�re et vis verca
	 * Quand le joueur ne d�cide par d'accuser une personne d'�tre une sorci�re mais d�cide d'utiliser une carte rumeur, c'est l'effet chasseur qui se d�clenche et non l'effet sorci�re. Il nous faut simpelemnt regarder s'il peut utiliser l'effet chasseur et non l'effet sorci�re
 	 * Quand le joueur se fait accuser et qu'il ne veut pas r�v�ler son identit�, il peut utiliser une carte rumeur, c'est son effet sorci�re qui se d�clenche, donc pas besoin de regarder son effet chasseur
	 * Si les deux bool�ens ont la valeur vrai, on veut alors r�cup�rer toute les cartes rumeur du joueur, non r�v�l�, que le joueur peut utiliser ces effets chasseurs ET sorciere.
	 * Dans cette m�thode, on r�cup�re dans un premier temps toute les cartes que le joueur peut utiliser (condition de jouabilit�; exemple:si r�v�l� villageois) sans se soucier des autres, puis en retirant les cartes qui ne sont finalement pas jouable car certain autre joueur ont utilis� des cartes bloquant d'autre par exemple.
	 * Les cartes retourn�s vont ^tre non r�v�l�e pour qu'il puisse utiliser les effets
	 * @param pourEffetSorciere Si ce bool�en est a vrai, la m�thode regarde alors si le joueur peut utiliser l'effet sorci�re en fonction du contexte du jeu. S'il est � faux, le joueur ne veut pas utiliser ce type d'effet.
	 * @param pourEffetChasseur  Si ce bool�en est a vrai, la m�thode regarde alors si le joueur peut utiliser l'effet chasseur en fonction du contexte du jeu. S'il est � faux, le joueur ne veut pas utiliser ce type d'effet.
	 * @return L'ensemble des cartes rumeurs non r�v�l� que le joueur peut utiliser
	 */
	public ArrayList<CarteRumeur> listeCarteRumeurJouableEnMain(boolean pourEffetSorciere, boolean pourEffetChasseur) {
		
		//la liste suivante sert a stocker les cartes jouable par l'utilisateur, celle si sera retourn� � la fin de la m�thode
		ArrayList<CarteRumeur> listeCarteRumeurJouable = new ArrayList<>();
		
		//On parcours l'ensemble des cartes qui constituent la main du joueur, et on regarde s'il peut les utiliser en fonction de s'il dispose des bonne conditions de jouabilit�
		//c'est a dire par exemple que si les effets chasseurs demande d'etre r�v�l� villageois, on regarde si le joueur et r�v�l� et si c'est un villageois
		//on regarde en suite la condition de jouabilit� sur si il a besoin d'avoir une carte rumeur de r�v�l�
		//cette premi�re �tape consiste donc a regarder s'il peut utiliser chaque carte sans ce soucier des autres joueurs et de leur carte d�voiler
		Iterator<CarteRumeur> itCarteRumeur1 = this.listeCarteRumeurNonReveleEnMain().iterator();
		while(itCarteRumeur1.hasNext()) {
			CarteRumeur c = itCarteRumeur1.next();
			//Si on d�sire savoir si l'effet chasseur de cette carte est utilisable
			if(pourEffetChasseur == true && pourEffetSorciere == false) {
				//on regarde si on peut utiliser l'effet chasseur
				if((c.getConditionJouabiliteEffetsChasseur() == EnumJouabiliteEffetCarteRumeur.AUCUNE)||(this.getCarteIdentite().getRole() == EnumRole.VILLAGEOIS && this.getEstRevele() == true && c.getConditionJouabiliteEffetsChasseur() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS)||(this.getListeCarteRumeurRevele().size() != 0 && c.getConditionJouabiliteEffetsChasseur()==EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_CARTE_RUMEUR_REVELEE)||(this.getListeCarteRumeurRevele().size() != 0 && this.getCarteIdentite().getRole() == EnumRole.VILLAGEOIS && this.getEstRevele() == true && c.getConditionJouabiliteEffetsChasseur() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS_ET_CARTE_RUMEUR_REVELEE)) {
					//si on peut utiliser l'effet chasseur, on l'ajoute alors � la liste des carte rumeur potentiellement utilisable
					listeCarteRumeurJouable.add(c);
				}
			//Si on d�sire savoir si l'effet sorciere de cette carte est utilisable
			}else if(pourEffetChasseur == false && pourEffetSorciere == true) {
				//on regarde si on peut utiliser l'effet sorciere
				if((c.getConditionJouabiliteEffetsSorciere() == EnumJouabiliteEffetCarteRumeur.AUCUNE)||(this.getCarteIdentite().getRole() == EnumRole.VILLAGEOIS && this.getEstRevele() == true && c.getConditionJouabiliteEffetsSorciere() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS)||(this.getListeCarteRumeurRevele().size() != 0 && c.getConditionJouabiliteEffetsSorciere()==EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_CARTE_RUMEUR_REVELEE)||(this.getListeCarteRumeurRevele().size() != 0 && this.getCarteIdentite().getRole() == EnumRole.VILLAGEOIS && this.getEstRevele() == true && c.getConditionJouabiliteEffetsSorciere() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS_ET_CARTE_RUMEUR_REVELEE)) {
					//si on peut utiliser l'effet sorci�re, on l'ajoute alors � la liste des carte rumeur potentiellement utilisable
					listeCarteRumeurJouable.add(c);
				}
			//Si on d�sire savoir si l'effet sorciere et chasseur de cette carte sont utilisable en m�me temps
			}else if(pourEffetChasseur == true && pourEffetSorciere == true) {
				//on regarde si on peut utiliser l'effet chasseur et sorci�re
				//on regarde dans un premier temps si on peut utiliser l'effet sorciere
				if((c.getConditionJouabiliteEffetsSorciere() == EnumJouabiliteEffetCarteRumeur.AUCUNE)||(this.getCarteIdentite().getRole() == EnumRole.VILLAGEOIS && this.getEstRevele() == true && c.getConditionJouabiliteEffetsSorciere() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS)||(this.getListeCarteRumeurRevele().size() != 0 && c.getConditionJouabiliteEffetsSorciere()==EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_CARTE_RUMEUR_REVELEE)||(this.getListeCarteRumeurRevele().size() != 0 && this.getCarteIdentite().getRole() == EnumRole.VILLAGEOIS && this.getEstRevele() == true && c.getConditionJouabiliteEffetsSorciere() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS_ET_CARTE_RUMEUR_REVELEE)) {
					//puis, on regarde si on peut utiliser l'effet chasseur
					if((c.getConditionJouabiliteEffetsChasseur() == EnumJouabiliteEffetCarteRumeur.AUCUNE)||(this.getCarteIdentite().getRole() == EnumRole.VILLAGEOIS && this.getEstRevele() == true && c.getConditionJouabiliteEffetsChasseur() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS)||(this.getListeCarteRumeurRevele().size() != 0 && c.getConditionJouabiliteEffetsChasseur()==EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_CARTE_RUMEUR_REVELEE)||(this.getListeCarteRumeurRevele().size() != 0 && this.getCarteIdentite().getRole() == EnumRole.VILLAGEOIS && this.getEstRevele() == true && c.getConditionJouabiliteEffetsChasseur() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS_ET_CARTE_RUMEUR_REVELEE)) {
						//si on peut utiliser l'effet chasseur et sorci�re, on l'ajoute alors � la liste des carte rumeur potentiellement utilisable
						listeCarteRumeurJouable.add(c);
					}
				}
			}
		}
		
		//on regarde maintenant, si chaque carte jouable est vraiment jouable en ce souciant cette fois si des autres joueurs et de leur carte r�v�l�
		//par exemple il y a des cartes qui ne peuvent pas �tre jou�e contre d'autre personne
		//cela peut poser des probl�mes par exemple s'il ne reste que 2 personne dans la manche et qu'un d�cide d'utiliser une carte qui est bloqu� par une autre carte de l'autre joueur
		//cette carte n'est alors pas jouable, en raison de l'autre joueur et non du joueur qui poss�de la carte
		
		//on d�finit la liste des cartes qui ne sont finalement pas joauble
		//on passe par une liste interm�diaire pour ne pas moidifer la liste de l'it�ration alors qu'il reste encore des it�ration a effectuer
		ArrayList<CarteRumeur> listeCarteFinalementPasJouable = new ArrayList<>();
		//on parcours alors l'ensemble des cartes qu'il peut jouer sans ce soucier des autres joueurs
		Iterator<CarteRumeur> itCarteRumeur2 = listeCarteRumeurJouable.iterator();
		while(itCarteRumeur2.hasNext()) {
			//on r�cup�re la carte dont nous devons v�rifier sa jouabilit� face aux autres joueurs
			CarteRumeur carteVerifierJouabilite = itCarteRumeur2.next();
			if(this.getListeJoueurContreQuiCarteRumeurJouable(carteVerifierJouabilite).size() == 0) {
				listeCarteFinalementPasJouable.add(carteVerifierJouabilite);
			}
		}
		//on retire de la liste des carte jouables celle qu ne le sont pas finalement
		listeCarteRumeurJouable.removeAll(listeCarteFinalementPasJouable);
		
		//on a regard� qu'elle carte pouvait �tre jou�e par le joueur sans ce soucier des autres joueur, puis en se souciant d'eux. 
		//les cartes restantes dans listeCarteRumeurJouable sont alors r�ellement jouable a l'instant t de l'appel de la m�thode quelque soit la situation de la partie
		//on return la liste contenant l'ensemble des cartes rumeurs jouable par le joueur selon si on s'int�resse � son effet chasseur ou sorci�re
		return listeCarteRumeurJouable;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer l'ensemble des joueurs contre qui une carte rumeur sp�cifique peut �tre utilis�.
	 * Cette m�thode retourne l'ensemble des joueurs encore en vie durant la manche, sauf ceux qui ont utilis� une carte rumeur bloquant la carte en question 
	 * @param carteVerifierJouabilite La carte rumeur que l'on souhaite regarder contre qui elle peut �tre utilis�
	 * @return Une liste de joueur contre qui la carte rumeur est jouable
	 */
	public ArrayList<Joueur> getListeJoueurContreQuiCarteRumeurJouable(CarteRumeur carteVerifierJouabilite){
		//m�thode qui permet de retourner une liste de joueur contre qui une carte rumeur peut etre utilis� (gestion des cartes blocage)
		//le this correspond au joueur qui utilise la carte rumeur pass� en param�tre
		ArrayList<Joueur> listeJoueurCarteRumeurJouable = new ArrayList<>();
		
		boolean jouableContreJoueur;
		//on parcours tout les joueur non �limin� sauf celui qui a utilis� la carte rumeur (this)
		Iterator<Joueur> itJoueur = Partie.getPartie().getListeJoueurNonEliminee(this).iterator();
		while(itJoueur.hasNext()) {
			//on r�cup�re le joueur de l'it�ration
			Joueur joueurIteration = itJoueur.next();
			//on consid�re dans un premier temps que la carte est jouable contre ce joueur
			jouableContreJoueur = true;
			//puis on parcours l'ensemble de ces cartes rumeur r�v�l�e de ce joueur pour savoir s'il n'y en a pas une qui bloque la carte dont nous regardons sa validit�
			Iterator<CarteRumeur> itCarteRumeurReveleJoueur = joueurIteration.getListeCarteRumeurRevele().iterator();
			//on sort d�s qu'il y a une carte qui bloque la carte pass� en param�tre
			while(itCarteRumeurReveleJoueur.hasNext() && jouableContreJoueur == true) {
				//on regarde si la carte du joueur bloc la carte dont nous essayons de regarder si elle est jouable contre au moins un joueur
				if(itCarteRumeurReveleJoueur.next().getProtectionContreAutreCarte().equalsIgnoreCase(carteVerifierJouabilite.getNomCarte())) {
					//si la carte est bloqu�, on indique qu'elle n'est pas jouable et on sort de ce joueur pour passer au joueur suivant
					jouableContreJoueur = false;
				}
			}
			//s'il n'y a aucune carte rumeur r�v�l� du joueur qui bloque la carte rumeur pass� en param�tre
			if(jouableContreJoueur == true) {
				//on ajoute le joueur de l'it�ration aux joueur contre qui la carte peut etre jou�e
				listeJoueurCarteRumeurJouable.add(joueurIteration);
			}
			
		}
			
		//on retourne la liste des joueur contre qui la carte peut etre utilis�
		return listeJoueurCarteRumeurJouable;
	}
	
	
	/**
	 * M�thode qui permet de lancer une accusation contre un autre joueur
	 * Dans cette m�thode, le joueur qui lance l'accusation (le joueurEnCours de la partie) choisit le nom d'un joueur n'ayant pas encore r�v�l� son identit�. Puis l'accuse d'�tre une sorci�re.
	 * De plus, cette m�thode demande au joueur accus� de r�pondre � l'accusation
	 * @param listeJoueurPossibleAccuser D�signe la liste des joueur parmit lesquelle le joueur qui lance l'accusation � le droit de choisir
	 */
	public void accuser(ArrayList<Joueur> listeJoueurPossibleAccuser) {
		//m�thode qui permet de lancer une accusation contre un autre joueur
		Partie.getPartie().ajouterUnMessage(2,">>> "+Partie.getPartie().getJoueurQuiDoitJouer().getNomJoueur()+", veuillez saisir le nom du joueur que vous accuser d'�tre une sorci�re");
		//on affiche la liste des noms des joueurs dont leur r�le n'ont pas �t� r�v�l�
		//et on r�cup�re un joueur � partir de son nom (celui ci ne doit pas etre r�v�l�, ni �tre le joueur qui le choisi)
		//on d�finit la liste des joueur qu'il peut choisir entre
		Partie.getPartie().setSelectionJoueurParmiListe(listeJoueurPossibleAccuser);
		//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
		Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_JOUEUR_ACCUSATION);
		//on r�cup�re la valeur du choix r�alis�
		Joueur joueurAccuse = Partie.getPartie().getJoueurSelectionne();
		//on l'affiche dans la console
		Partie.getPartie().ajouterUnMessage(2,Partie.getPartie().getJoueurQuiDoitJouer().getNomJoueur()+", vous avez choisi d'accuser le joueur "+joueurAccuse.getNomJoueur());
		Partie.getPartie().demandeConfirmation(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
		//le joueur qui se fait accuser doit alors r�pondre � l'accusation
		joueurAccuse.repondreAccusation();
	}
	
	/**
	 * M�thode qui permet de r�cup�rer une carte rumeur poss�dant un nom particulier parmit une liste pass� en param�tre.
	 * S'il n'y a pas de carte parmit la liste avec le nom donn�, alors la m�thode retourne la valeur null.
	 * @param nomCarteRumeur Nom de la carte que l'on souhaite r�cup�rer
	 * @param listeCarteRumeur La liste des cartes parmit laquelle on doit regarder que la carte appartient
	 * @return Une carte rumeur avec un nom sp�cifique
	 */
	public static CarteRumeur getCarteRumeurAPartircNom(String nomCarteRumeur, ArrayList<CarteRumeur> listeCarteRumeur) {
		//m�thode qui permet de retourner une carte dans une liste � partir de son nom
		//on parcours toute les cartes rumeurs pr�sentes dans la liste
		for(CarteRumeur c:listeCarteRumeur) {
			//on regarde si le nm de la carte rumeur correspond � celle souhait�
			if(c.getNomCarte().equals(nomCarteRumeur)) {
				//si c'est le cas on la retourne
				return c;
			}
		}
		//si on ne l'a pas trouv� on retourne la valeur null
		return null;
	}
	
	/**
	 * M�thode qui permet d'utiliser les effets sorci�re et chasseurs d'une carte rumeur choisi.
	 * On demande au joueur de choisir une carte rumeur qu'il peut jouer en fonction de s'il veut utiliser l'effet sorci�re ou l'effet chasseur de la carte.
	 * Une fois la carte s�lectionn�, on appliquer les effets sorci�re et/ou chasseur comme souhait�
	 * @param pourEffetSorciere  Si vous voulez utiliser l'effet sorci�re de la carte, vous devez passez ce bool�en � vrai.
	 * @param pourEffetChasseur Si vous voulez utiliser l'effet chasseur de la carte, vous devez passez ce bool�en � vrai.
	 */
	public void utiliserCarteRumeur(boolean pourEffetSorciere, boolean pourEffetChasseur) {
		//m�thode qui permet de s�lectionner une carte rumeur jouable en fonction de l'effet souhait� et d'appliquer ces effets
		
		//on r�cup�re la carte rumeur valide, s�lectionn�e par le joueur
		//on d�finit la liste des cartes rumeurs qu'il peut choisir entre
		Partie.getPartie().setSelectionCarteRumeurParmiListe(this.listeCarteRumeurJouableEnMain(pourEffetSorciere, pourEffetChasseur));
		//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
		Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_CARTE_RUMEUR_UTILISATION_EFFET);
		//on r�cup�re la valeur du choix r�alis�
		CarteRumeur carteRumeurSelectionne = Partie.getPartie().getCarteRumeurSelectionne();
		
		//on indique que le joueur a retourner sa carte: il ne pourra plus utiliser son effet
		carteRumeurSelectionne.setFaceCache(false);
		
		//on indique comme quoi la derni�re carte rumeur jou�e correspond � celle qui vient d'�tre choisi � l'instant
		this.setDerniereCarteRumeurJouee(carteRumeurSelectionne);
		
		//on applique les effets de la cartes
		if(pourEffetChasseur && pourEffetSorciere) {
			//si on veut utiliser les effets chasseur et sorci�re de la carte rumeur s�lectionn�e
			//cas qui n'est pas cens� avoir lui mais possible dans le futur
			carteRumeurSelectionne.appliquerEffetsSorciere();
			carteRumeurSelectionne.appliquerEffetsChasseur();
		}else if(pourEffetChasseur) {
			//si on veut utiliser les effets chasseurs de la carte rumeur s�lectionn�e
			carteRumeurSelectionne.appliquerEffetsChasseur();
		}else if(pourEffetSorciere){
			//si on veut utiliser les effets sorci�re de la carte rumeur s�lectionn�e
			carteRumeurSelectionne.appliquerEffetsSorciere();
		}
	}
	
	/**
	 * M�thode qui permet au joueur de r�v�ler son identit� au pr�s des autre joueur.
	 * Si le joueur accus� est une sorci�re, alors le joueur qui a lanc� l'accusation remporte 1 point et prend le tour suivant.
	 * Si le joueur accus� est un villageois, alors l'accusateur ne gagne pas de point et l'accus� prend le tour suivant.
	 */
	public void revelerIdentiter() {
		//m�thode qui permet de r�vel�r aux yeux de tous l'identit� du joueur, d'attribuer les points au joueur accusateur et de d�finir le joueur accusateur suivant
		//on stocke l'information comme quoi il a �tait rev�l� dans l'attribut estRevel�
		this.setEstRevele(true);
		//on r�cup�re le role du joueur pour pouvoir l'afficher
		EnumRole role = this.getCarteIdentite().getRole();
		//on regarde si c'�tait un villageois
		if(role == EnumRole.VILLAGEOIS) {
			//on affiche a tout le monde que c'�tais un villageois
			Partie.getPartie().ajouterUnMessage(2,"Hey, je me pr�nomme "+this.getNomJoueur()+" et je suis un villageois");
			//on indique au joueur accusateur combien de pts il a gagn�, dans ce cas si 0 car il s'agissait d'un villageois
			Partie.getPartie().ajouterUnMessage(2,Partie.getPartie().getJoueurDebutTour().getNomJoueur()+" je crains que tu te sois tromp�. Tu ne gagnes pas alors de points");
			//vu qu'il n'a pas gagn� de point (0), on ne mets pas a jour son score 
			//on d�finit le joueur suivant apr�s une accusation, qui est lui m�me car c'�tais un villageois
			Partie.getPartie().setJoueurDebutTour(this);
			Partie.getPartie().ajouterUnMessage(2,"Le prochain tour est alors � moi.");
			
		//on regarde si c'�tais une sorci�re
		}else if(role == EnumRole.SORCIERE){
			//on affiche a tout le monde que c'�tait une sorci�re
			Partie.getPartie().ajouterUnMessage(2,"Bien jou�, vous m'avez d�masqu�, je suis bien une sorci�re");
			//on indique au joueur accusateur combien de pts il a gagn�, dans ce cas si 1 car il a bien accus� une sorci�re
			Partie.getPartie().ajouterUnMessage(2,Partie.getPartie().getJoueurDebutTour().getNomJoueur()+", tu as donc raison. Tu gagnes alors 1 point");
			//on mets a jour le score du joueur accusateur
			Partie.getPartie().getJoueurDebutTour().setScore(Partie.getPartie().getJoueurDebutTour().getScore()+1);
			//on d�finit le joueur suivant apr�s une accusation, vu que l'accus� est une sorci�re, c'est au tour de l'accusateur de jouer une fois de plus 
			Partie.getPartie().setChangementJoueurAccusateurEffectue(true);
			Partie.getPartie().ajouterUnMessage(2,"Tu vas pouvoir rejouer une nouvelle fois.");
		}
		//On demande de confirmer pour marquer une pause suite � la r�ponse,
		Partie.getPartie().demandeConfirmation(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
	}
	
	/** 
	 * M�thode qui permet de r�v�ler l'identit� d'une personne en attribuant ou soustrayant une certaine quantit� de point � la personne qui lui a demand� de r�v�ler son identt�
	* Cette m�thode est notemment utilis� pour les effets
	* "Reveal another player's identity (Witch : You gain 2pts. You take next turn, Villager:You lose 2pts. They take next turn)" & "Choose a player. They must reveal their identity or discard a card from their hand. (Witch : You gain 1pt. You take next turn, Villager:You lose 1pt.They take next turn. If they discard:they take next turn)"
	* Si le joueur qui doit r�v�ler son identit� (this) est une sorci�re alors l'accusateur (le joueur pass� en param�tre) prend le prochain tour et gagne un certain nombre de point suppl�mentaire
	* Cependant s'il s'agit d'un villageois, alors le joueurSuiUtiliseEffet perd un certain nombre de point et l'accus� prend le tour suivant
	* @param joueurQuiUtiliseEffet D�signe le joueur qui demande au this de r�v�ler son identit�. L'accusateur ne correspond donc pas au joueur en cours de la classe Partie
	* @param pointSupplementaire D�signe le nombre de point que le joueur accusateur gagne si l'accus� est une sorci�re
	* @param pointARetirer D�signe le nombre de point que l'accusateur perd si l'accus� est un villageois
	*/
	public void revelerIdentiteAvecAjoutPertePoint(Joueur joueurQuiUtiliseEffet, int pointSupplementaire, int pointARetirer) {
		//m�thode qui permet de r�v�ler l'identit� d'une personne en attribuant ou soustrayant une certaine quantit� de point � la personne qui lui a demand� de r�v�ler son identt�
		//cette m�thode est utile pour les effets
		//"Reveal another player's identity (Witch : You gain 2pts. You take next turn, Villager:You lose 2pts. They take next turn)" & "Choose a player. They must reveal their identity or discard a card from their hand. (Witch : You gain 1pt. You take next turn, Villager:You lose 1pt.They take next turn. If they discard:they take next turn)"
		//le this correspond � la personne qui doit r�v�ler son identit�
		//on mets a jour la valeur de son attribut, on indique comme quoi il est r�v�l�e
		this.setEstRevele(true);
		
		//on r�cup�re le role du joueur qui doit r�v�ler son identit�
		EnumRole role = this.getCarteIdentite().getRole();
		//on regarde si c'�tait un villageois : si c'est le cas le joueur qui utilise l'effet perd Xpts. Il prend le prochain tour
		if(role == EnumRole.VILLAGEOIS) {
			//on affiche a tout le monde que c'�tais un villageois
			Partie.getPartie().ajouterUnMessage(2,"Hey, je me pr�nomme "+this.getNomJoueur()+" et je suis un villageois");
			//on indique au joueur qui utilise l'effet combien de pts il a gagn�, dans ce cas si il perd 2pts car il s'agissait d'un villageois
			Partie.getPartie().ajouterUnMessage(2,joueurQuiUtiliseEffet.getNomJoueur()+" je crains que tu te sois tromp�. Tu perds alors "+pointARetirer+"pts.");
			Partie.getPartie().ajouterUnMessage(2,joueurQuiUtiliseEffet.getNomJoueur()+" passe de "+joueurQuiUtiliseEffet.getScore()+" � "+(joueurQuiUtiliseEffet.getScore()-pointARetirer)+" points.");
			//Vu qu'il s'agit d'un villageois, la personne qui doit r�v�ler son identit� prend le tour suivant et la personne qui utilise l'effet perd Xpts
			joueurQuiUtiliseEffet.setScore(joueurQuiUtiliseEffet.getScore()-pointARetirer);
			//on d�finit le joueur suivant qui est la personne qui a du r�v�l�er son identit�
			Partie.getPartie().setJoueurDebutTour(this);
			Partie.getPartie().ajouterUnMessage(2,"Le prochain tour est alors � "+this.getNomJoueur());
			
		//on regarde si c'�tais une sorci�re : si c'est le cas Vous gagnez Xpts. Vous jouez le prochain tour
		}else if(role == EnumRole.SORCIERE){
			//on affiche a tout le monde que c'�tait une sorci�re
			Partie.getPartie().ajouterUnMessage(2,"Bien jou�, je suis "+this.getNomJoueur()+" et vous m'avez d�masqu�, je suis bien une sorci�re");
			//on indique au joueur qui a utilis� l'effet combien de pts il a gagn�, dans ce cas si 2 car il a bien accus� une sorci�re
			Partie.getPartie().ajouterUnMessage(2,joueurQuiUtiliseEffet.getNomJoueur()+", tu as donc raison. Tu gagnes alors "+pointSupplementaire+"pts.");
			Partie.getPartie().ajouterUnMessage(2,joueurQuiUtiliseEffet.getNomJoueur()+" passe de "+joueurQuiUtiliseEffet.getScore()+" � "+(joueurQuiUtiliseEffet.getScore()+pointSupplementaire)+" points.");
			//on mets a jour le score du joueur qui a utiliser l'effet
			joueurQuiUtiliseEffet.setScore(joueurQuiUtiliseEffet.getScore()+pointSupplementaire);
			//on d�finit le joueur suivant, vu que l'accus� est une sorci�re, c'est au tour de joueurQuiUtiliseEffet
			Partie.getPartie().setJoueurDebutTour(joueurQuiUtiliseEffet);
			Partie.getPartie().ajouterUnMessage(2,"Le prochain tour est alors � "+joueurQuiUtiliseEffet.getNomJoueur());
		}
		//On demande de confirmer pour marquer une pause suite � la r�ponse,
		Partie.getPartie().demandeConfirmation(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
	}
	
	/**
	 * M�thode qui permet � un joueur de r�pondre � l'accusation d'un autre joueur.
	 * L'accus� est le this, l'accusateur est le joueurDebutTour pr�sent en attribut de la classe Partie
	 * Si le joueur ne peut pas jouer de carte rumeur effet sorci�re et qu'il n'a pas r�v�ler son identit�, il doit alors la r�v�ler
	 * Si le joueur ne peut pas jouer de carte rumeur effet sorci�re et qu'il a d�j� r�v�ler son identit�, on lui demande alors de r�v�ler une autre fois son identit�
	 * Si le joueur peut utiliser des cartes rumeur effet sorci�re et qu'il a d�j� r�v�ler son identit�, il utilsie alors l'effet sorci�re d'une de ces cartes
	 * Si le joueur peut utiliser des effets sorci�re et qu'il n'a pas r�v�ler son identit�, il a alors le choix entre ces deux posibilit�
	 */
	public void repondreAccusation() {
		//m�thode qui permet � un joueur donn�e de r�podre a une accusation
		//on modifie le joueur qui doit jouer
		Partie.getPartie().setJoueurQuiDoitJouer(this);
		//on lui affiche des message pour qu'il sache quoi faire
		Partie.getPartie().ajouterUnMessage(2,">>> C'est au tour de l'accus� : "+this.getNomJoueur());
		Partie.getPartie().ajouterUnMessage(2,this.getNomJoueur()+",vous venez d'�tre accus� par "+Partie.getPartie().getJoueurDebutTour().getNomJoueur());
		Partie.getPartie().ajouterUnMessage(2,"Vous devez lui r�pondre cordialement");
		
		int choix = 1;//R�v�ler son identit� =1, effet sorci�re =2
		//on regarde s'il peut utiliser l'effet chasseur d'une de ces cartes rumeurs
		if(this.listeCarteRumeurJouableEnMain(true,false).size() != 0) {
			//si le joueur dispose de carte rumeur non r�l�v�, 
			//on regarde s'il n'a pas d�j� r�v�l� son identit�
			if(this.getEstRevele() == false) {
				//s'il n'est pas r�v�l� et qu'il peut utiliser des cartes rumeurs
				//on lui demande alors de choisir entre r�v�ler son identit� ou d'utiliser l'effet sorci�re d'une de ces carte rumeur
				Partie.getPartie().ajouterUnMessage(1,"Veuillez choisir entre 1 ou 2");
				Partie.getPartie().ajouterUnMessage(1,"     - 1 : R�v�ler votre identit�");
				Partie.getPartie().ajouterUnMessage(1,"     - 2 : Utiliser l'effet sorci�re d'une carte rumeur");
				Partie.getPartie().ajouterUnMessage(0,"Veuillez choisir entre :");
				Partie.getPartie().ajouterUnMessage(0,"     - R�v�ler votre identit�");
				Partie.getPartie().ajouterUnMessage(0,"     - Utiliser l'effet sorci�re d'une carte rumeur");
				//on demande � l'utilisateur de choisir entre ces deux options et on v�rifie ce qu'il a saisi
				//on d�finit les bornes de l'intervalle de la s�lection de l'entier
				Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(1,2)));
				//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
				Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_ENTIER_REVELER_IDENTITE_OU_EFFET_SORCIERE);
				//on r�cup�re la valeur du choix r�alis�
				choix = Partie.getPartie().getEntierSelectionne();

			}else {
				//dans le cas o� il est r�v�l� et qu'il a des carte rumeur qu'il peut utiliser
				choix = 2;
				//on lui force d'utiliser ces cartes rumeurs, car il ne va pas pouvoir r�v�ler une autre fois son identit�
				Partie.getPartie().ajouterUnMessage(2,this.getNomJoueur()+" vous avez d�j� r�v�ler votre identit� et vous poss�dez des cartes rumeurs jouables.");
				Partie.getPartie().ajouterUnMessage(2,"Vous allez donc les utiliser.");
			}
			
		}else {
			//si le joueur n'a pas le choix que de r�v�ler son identit� car il n'a plus de carte rumeur jouable
			Partie.getPartie().ajouterUnMessage(2,"Vous n'avez plus de carte rumeur en main non r�v�l�e dont vous pouvez utiliser son effet sorci�re");
			Partie.getPartie().ajouterUnMessage(2,"Vous allez devoir r�v�ler votre identit�.");
			//dans le cas o� il ne peut pas jouer de carte rumeur et qu'il a d�j� r�v�l� son identit� dans le pass�, 
			//il va devoir r�v�ler son identit� une deuxi�me fois
			if(this.getEstRevele() == true) {
				Partie.getPartie().ajouterUnMessage(2,"M�me si vous l'avez d�j� r�v�l�e.");
			}
		}
		
		if(choix == 1) {
			//si le joueur � d�cider de r�v�ler son identit�
			Partie.getPartie().ajouterUnMessage(2,"Vous avez d�cid� de r�v�ler votre identit�");
			this.revelerIdentiter();
		}else {
			//si le joueur � d�cid� d'utiliser l'effet sorciere d'une de ces carte rumeurs
			Partie.getPartie().ajouterUnMessage(2,"Vous avez d�cid� d'utiliser l'effet sorci�re d'une de vos carte rumeur");
			this.utiliserCarteRumeur(true,false);
		}
				
	}
	
	/**
	 * M�thode qui permet d'ajouter une carte rumeur dans la liste des cartes rumeur du joueur.
	 * Cette carte peut �tre r�v�l� ou non.
	 * @param carte Carte rumeur que l'on souhaite ins�r� dans la liste des carte rumeur du joueur
	 */
	public void ajouterCarteRumeurEnMain(CarteRumeur carte) {
		this.listeCartesRumeurEnMain.add(carte);
	}
	
	/**
	 * M�thode qui permet de retirer une carte rumeur de la liste des carte rumeur du joueur
	 * @param carte Carte rumeur qui va etre retir� de la liste des cartes rumeur du joueur s'il la poss�de
	 */
	public void retirerCarteRumeurEnMain(CarteRumeur carte) {
		if(this.listeCartesRumeurEnMain.contains(carte)) {
			this.listeCartesRumeurEnMain.remove(carte);
		}
	}
	
	/**
	 * M�thode qui permet de savoir si un joueur n'est plus en vie durant la manche en cours. 
	 * @return Un bool�en qui prend la valeur True si le joueur est �limin�e de la manche, False si le joueur n'est pas �limin� 
	 */
	public boolean estElemine() {
		if(this.getCarteIdentite().getRole() == EnumRole.SORCIERE && this.getEstRevele() == true) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * M�thode qui permet a un joueur de choisir son r�le.
	 * Il doit saisir 1 pour villageois, 0 pour sorci�re.
	 * Une fois que le r�le s�lectionn� lui convient, il doit le confirmer en appuyant sur la touche 0
	 */
	public void choisirRole() {
		//m�thode qui permet au joueur de choisir son r�le 
		//variable qui permet de r�cup�rer la saisi du joueur lors de la s�lection de son r�le ou de la confirmation
		int choixRole = 1;
		
		//on affiche les diff�rents chiffre qu'il peut saisir pour changer de r�le et le confirmer
		Partie.getPartie().ajouterUnMessage(2,this.getNomJoueur()+" doit choisir son r�le");
		Partie.getPartie().ajouterUnMessage(1,"Quel r�le voulez-vous incarner ? (Saisir 1 ou 2 ou 0)");
		Partie.getPartie().ajouterUnMessage(1,"     1 - Villageois");
		Partie.getPartie().ajouterUnMessage(1,"     2 - Sorci�re");
		Partie.getPartie().ajouterUnMessage(1,"     0 - Confirmation du r�le s�lectionn�");
		Partie.getPartie().ajouterUnMessage(0,"Quel r�le voulez-vous incarner ?");
		Partie.getPartie().ajouterUnMessage(0,"Appuyer sur la fl�che qui tourne pour changer de r�le.");
		Partie.getPartie().ajouterUnMessage(0,"Appuyer sur sur le bouton CONFIRMER pour valider votre r�le.");
		
		do {
			//on affiche le r�le du joueur, par d�faut son r�le est villageois
			if(this.getCarteIdentite().getRole() == EnumRole.VILLAGEOIS) {
				Partie.getPartie().ajouterUnMessage(2,"Votre r�le est : Villageois");
			}else if(this.getCarteIdentite().getRole() == EnumRole.SORCIERE){
				Partie.getPartie().ajouterUnMessage(2,"Votre r�le est : Sorci�re");
			}

			//on demande � l'utilisateur de choisir entre ces options et on v�rifie ce qu'il a saisi
			//on modifie les bornes de l'intervalle. Il doit choisir enrte 0 et 2
			Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(0,2))); 
			//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
			Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_ENTIER_ROLE);
			//on r�cup�re la valeur du choix r�alis�
			choixRole = Partie.getPartie().getEntierSelectionne();
			
			//on actualise le r�le du joueur en fonction de la saisi
			if(choixRole == 1) {
				//s'il a demand� � �tre un villageois, on le d�finit alors en villageois
				Partie.getPartie().ajouterUnMessage(1,"Votre r�le a bien �t� chang� en Villageois. Appuyer sur 0 pour confirmer.");
				Partie.getPartie().ajouterUnMessage(0,"Votre r�le a bien �t� chang� en Villageois. Appuyer sur le bouton CONFIRMER pour continuer.");
				this.getCarteIdentite().setRole(EnumRole.VILLAGEOIS);
			}else if(choixRole == 2) {
				//s'il a demand� a �tre une sorci�re, on le transforme en sorci�re
				Partie.getPartie().ajouterUnMessage(1,"Votre r�le a bien �t� chang� en Sorci�re. Appuyer sur 0 pour confirmer.");
				Partie.getPartie().ajouterUnMessage(0,"Votre r�le a bien �t� chang� en Sorci�re. Appuyer sur le bouton CONFIRMER pour continuer.");
				this.getCarteIdentite().setRole(EnumRole.SORCIERE);
				
			}
		//on r�p�te tant qu'il n'a pas confirm� son r�le
		}while(choixRole != 0);
		
	}
	
	/**
	 * Dans cette m�thode le joueur s�lectionne une carte rumeur parmi une liste de carte possible pass� en param�tre. Puis il doit la d�fausser.
	 * Cette m�thode est utilis�e pour ne pas r�p�ter de code entre deux effets
	 * <ul>
	 * 	<li>Discard a card from your hand</li>
	 * 	<li>Choose a player. They must reveal their identity or discard a card from their hand. (Witch : You gain 1pt. You take next turn, Villager:You lose 1pt.They take next turn. If they discard:they take next turn)</li>
	 * </ul>
	 * @param listeCarteADefausser Liste de carte rumeur parmi laquelle le joueur peut choisir de d�fausser
	 */
	public void choisirCarteRumeurPuisDefausser(ArrayList<CarteRumeur> listeCarteADefausser) {
		//m�thode qui permet de choisir une carte parmit une liste et de la d�fausser par la suite
		//utiliser pour ne pas r�p�ter de code entre deux effets
		//     - Discard a card from your hand
		//     - Choose a player. They must reveal their identity or discard a card from their hand. (Witch : You gain 1pt. You take next turn, Villager:You lose 1pt.They take next turn. If they discard:they take next turn)
		
		//on d�finit la liste des cartes rumeurs qu'il peut choisir entre
		Partie.getPartie().setSelectionCarteRumeurParmiListe(listeCarteADefausser);
		//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
		Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_CARTE_RUMEUR_A_DEFAUSSER);
		//on r�cup�re la carte rumeur du choix r�alis�, correspond � la carte rumeur que le joueur veut d�fausser
		CarteRumeur carteADefausser = Partie.getPartie().getCarteRumeurSelectionne();
		
		//on ajoute cette carte � la liste des cartes d�fauss� pr�sent dans la classe partie
		Partie.getPartie().ajouterCarteRumeurDefausser(carteADefausser);
		//on retire de la main du joueur, la carte
		this.retirerCarteRumeurEnMain(carteADefausser);
		//on lui indique qu'il a bien d�fausser la carte s�lectionn�e
		Partie.getPartie().ajouterUnMessage(2,"Vous avez bien d�fauss� la carte suivante:");
		Partie.getPartie().ajouterUnMessage(1,carteADefausser);
		
		Partie.getPartie().setSelectionCarteRumeurParmiListe(new ArrayList<>(Arrays.asList(carteADefausser)));
		Partie.getPartie().demandeConfirmation(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE);
	}
	
	/**
	 * M�thode qui permet de bloquer l'execution du programme tant que le joueur qui doit jouer n'a pas saisi ce qu'il devait saisir.
	 * Cette m�thode ne permet pas la saisi. C'est dans la vu que la saisi (clic sur bouton ou clavier) � lieu.
	 * Cette m�thode n'est pas static car elle est red�finit dans la classe joueurVirtuel
	 * @param nouveauStatutPartie D�signe le nouveau statut que la partie va prendre. Pour que les joueurs connaissent la raison de l'attente.
	 */
	public void enAttenteDeSelection(EnumStatutPartie nouveauStatutPartie) {
		//on attent tant que le Thread principale est bloqu�, il doit �tre d�bloqu� par les m�thodes de s�lections textuelle
		//la premi�re fois que l'on arrive dans la m�thode, le compteur du latch � pour valeur 0, l'ex�cution de la m�thode n'est donc pas bloqu� dans le try catch suivant
		//Dans des configuration tr�s pr�cise, d'autre Threads peuvent modifier la valeur du bool�en selectionEnAttente, ce qui va permettre de sortir de la m�thode sans avoir attendu
		//si on sort et que le joueur n'a rien saisi, on reboucle dans la m�thode mais cette fois si avec un compteur de 1
		//donc dans le try suivant, l'execution du programme est alors bloqu�
		try {
			Partie.getPartie().getLatch().await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//on d�finit le nouveau compteur, on peut que le Thread du texte diminue la valeur du compteur (1->0) pour d�bloquer l'ex�cution du Thread principale
		//cette m�thode peut etre appeler plusieurs fois d'affiler avant que le thread secondaire ne soit lanc�
		Partie.getPartie().setLatch(new CountDownLatch(1));
		//m�thode qui permet d'attendre tant qu'aucune s�lection valide n'a �tait faite
		//on modifie le statut de la partie par le nouveau statut pass� en param�tre de la m�thode
		Partie.getPartie().setStatutPartie(nouveauStatutPartie);
		//on indique que la partie est en attente d'une saisi d'un joueur r�el
		Partie.getPartie().setSelectionEnAttente(true);
		//tant que la s�lection n'a pas �tait faite, on bloque l'execution du programme		
		boolean enAttente = true;
		
		while(enAttente) {
			//on sort de la boucle quand le joueurs a saisi l'�l�ment attendu ou cliquer sur le bouton ad�quats des vues
			//on ne demande donc pas ici de saisir un truc
			synchronized(Partie.getPartie()) {
				enAttente= Partie.getPartie().isSelectionEnAttente();
			}
		}	
		
	}
	
	/**
	 * M�thode qui permet d'afficher � l'�cran une liste de carte rumeur, avec ces d�tails
	 * Cette m�thode pas static car besoin d'�tre red�finit chez le joueur virtuel
	 * @param listeCarteRumeur Liste de carte rumeurs qui seront affich�es
	 */
	public void affichageListeCarte(ArrayList<CarteRumeur> listeCarteRumeur) {
		//m�thode qui permet d'afficher � l'�cran une liste de carte rumeur
		//on parcours toute les cartes rumeur pr�sent dans la liste
		Iterator<CarteRumeur> it = listeCarteRumeur.iterator();
		while(it.hasNext()) {
			//on ajoute la carte rumeur de l'it�ration sous la forme d'une chaine de caract�re � la liste des messages d�taill�
			Partie.getPartie().ajouterUnMessage(1,it.next());
		}
	}

	/**
	 * M�thode qui permet d'afficher les diff�rentes caract�ristique d'un joueur
	 * <ul>
	 * <li>Nom</li>
	 * <li>Nombre de point</li>
	 * <li>S'il s'agit d'un joueur virtuel ou non</li>
	 * <li>S'il a r�v�ler son identit�</li>
	 * <li>Son role par l'interm�diaire de sa carte d'identit�</li>
	 * <li>La liste de ces cartes rumeurs</li>
	 * <li>Le nom de la derni�re carte rumeur qu'il a jou�e</li>
	 * </ul>
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		//indicant le d�but de l'affichage d'un joueur
		sb.append("----------\n");
		sb.append("Nom du joueur: "+this.getNomJoueur()+"\n");
		sb.append("Score: "+this.getScore()+"\n");
		
		if(this.getEstJoueurVirtuel() == false) {
			sb.append("Joueur virtuel: Non\n");
		}else {
			sb.append("Joueur virtuel: Oui\n");
		}
		
		if(this.getEstRevele() == true) {
			sb.append("Joueur a r�v�l� son identit� : Oui\n");
		}else {
			sb.append("Joueur a r�v�l� son identit� : Non\n");
		}
		
		sb.append("Carte Identit� :\n");
		sb.append("     - "+this.getCarteIdentite()+"\n");
		
		sb.append("Liste des noms des cartes rumeurs :\n");
		if(this.getListeCartesRumeurEnMain().size() == 0) {
			sb.append("     - Aucune carte rumeur disponible\n");
		}else {
			for(CarteRumeur carteR : this.getListeCartesRumeurEnMain()) {
				sb.append("     - "+carteR.getNomCarte()+"\n");
			}
		}
		
		if(this.getDerniereCarteRumeurJouee() != null) {
			sb.append("Derni�re carte rumeur jou�e : "+this.getDerniereCarteRumeurJouee().getNomCarte());
		}
		
		//indicateur marquant la fin de l'affichage d'un joueur
		sb.append("\n----------\n");
		
		return sb.toString();
	}
	
	/**
	 * M�thode qui permet de compar� le joueur avec un autre objet. S'il s'agit du meme joueur alors on retourne la valeur vraie sinon faux.
	 */
	public boolean equals(Object o) {
		if(o instanceof Joueur) {
			Joueur j = (Joueur)o;
			if(j.getNomJoueur() == this.getNomJoueur()) {
				//chaque joueur poss�de un nom unique, pas besoin de v�rifier les autres attributs
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}
	}
	
	/**
	 * M�thode qui permet de calculer l'intersection entre deux listes.
	 * Les �l�ments de la liste retourn�e correspond a des �l�ments appartenant aux deux listes.
	 * @param liste1 Premi�re liste de joueur
	 * @param liste2 Deuxi�me liste de joueur
	 * @return Une liste de joueur appartenant aux deux liste pass� en param�tre
	 */
	public static ArrayList<Joueur> intersectionEntreListes(ArrayList<Joueur> liste1, ArrayList<Joueur> liste2){
		//m�thode qui retourne l'intersection entre deux liste de joueurs
		ArrayList<Joueur> res = new ArrayList<>();
		
		Iterator<Joueur> it = liste1.iterator();
		while(it.hasNext()) {
			Joueur j = it.next();
			if(liste2.contains(j)) {
				res.add(j);
			}
		}
		
		return res;
	}
	
	
}
