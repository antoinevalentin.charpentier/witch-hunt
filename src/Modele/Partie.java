package Modele;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Observable;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

import Modele.effet.AccuserUneAutrePersonne;
import Modele.effet.AjouterCarteDefausserPuisDefausser;
import Modele.effet.AvantLeurTourPrendreCarteJoueurSuivant;
import Modele.effet.AvantLeurTourRegarderIdentite;
import Modele.effet.ChoisirJoueurReverlerSonIdentiteOuDefausser;
import Modele.effet.ChoisirJoueurSuivant;
import Modele.effet.DefausserCarte;
import Modele.effet.DefausserCarteJoueurAccusateur;
import Modele.effet.PrendreCarteJoueurAccusateur;
import Modele.effet.PrendreCarteRumeurDUnePersonne;
import Modele.effet.PrendreLeProchainTour;
import Modele.effet.RecupererCarteRumeurRevele;
import Modele.effet.RevelerIdentiteAutreJoueur;
import Modele.effet.RevelerVotreIdentite;
import Modele.enums.EnumJouabiliteEffetCarteRumeur;
import Modele.enums.EnumRole;
import Modele.enums.EnumStatutPartie;

/**
 * Repr�sente une partie du jeu de Witch Hunt.
 * Une partie est constitu�e de plusieurs manches et une manche de plusieurs tours.
 * La partie se termine quand il y a au moins un joueur qui a plus de 5 points.
 * Une manche se termine quand il ne reste plus qu'un joueur qui n'a pas r�v�l� son identit�.
 * Dans une partie, s'affronte entre 3 et 6 joueurs r�els ou virtuels.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
@SuppressWarnings("deprecation")
public class Partie extends Observable{
	/**
	 * Synchroniseur qui permet � un Thread d'attendre un ou plusieurs Thread s avant le d�but du traitement.
	 * Dans notre cas, il permet au Thread principale (celui du mod�le) d'attendre les Threads secondaire (VueTexte).
	 */
	private CountDownLatch latch;

	//liste des attributs
	/**
	 * D�signe quant � d�marr� la partie
	 */
	private long tempsDebutJeu;
	/**
	 * D�signe le num�ro de la manche o� les joueurs sont en train de jouer
	 */
	private int numeroManche;
	/**
	 * Permet de garder en m�moire l'instance de la partie lanc� (singleton)
	 */
	private static final Partie partie = new Partie(); //une seul instanciation qui ne peut pas changer
	
	/**
	 * Liste des cartes rumeurs qui sont d�fauss�es. Elles regroupent � la fois les cartes d�fauss�es lors de la distribution des cartes, mais �galement celles d�fauss�es par le joueur.
	 */
	private ArrayList<CarteRumeur> listeCarteRumeurDefausser;
	
	/**
	 * D�signe la liste des joueurs jouant a la partie. Ils peuvent s'agir de joueur r�el ou de joueur virtuel. 
	 */
	private ArrayList<Joueur> listeJoueurs;
	/**
	 * L'attribut joueurDebutTour d�signe le joueur qui doit lancer une accusation ou utiliser l'effet chasseur d'une de ces cartes rumeurs. Attention, il ne prend jamais comme valeur l'instance du joueur qui doit r�pondre � l'accusation ou utiliser l'effet sorci�re d'une de ces cartes. Ce joueur peut �tre dans certain cas diff�rent ou �gale au joueurEnCours 
	 */
	private Joueur joueurDebutTour;
	/**
	 * L'attribut joueurQuiDoitJoueur d�signe le joueur qui doit jouer � un moment donn� de la partie. Il prend comme valeur le joueur qui doit accuser/r�pondre � une accusation/utiliser une carte rumeur/choisir son r�le ,... 
	 */
	private Joueur joueurQuiDoitJouer;
	/**
	 * Bool�en qui permet de savoir si un changement de joueur est pr�vu � la fin du tour. S'il n'y a pas de changement de joueur de pr�vu, on prend le joueur suivant dans la liste, sinon on prend le joueur choisit. Il prend la valeur False s'il n'y a pas de changement de joueur de pr�vu, sinon true si c'est pr�vu.
	 */
	private boolean changementJoueurAccusateurEffectue;
	//on a ajout� les bool�en suivant en raison du syst�me de s�lections par rapport aux diff�rentes vues du jeu
	/**
	 * Bool�en qui prend la valeur vrai, si une saisi est demande aux joueurs. S'il est � faux, aucune s�lection n'a besoin d'�tre effectu� par un utilisateur.
	 */
	private boolean selectionEnAttente;
	/**
	 * Un joueur pourra choisir un joueur parmi cette liste lorsqu'il devra choisir un autre joueur durant la partie.
	 */
	private ArrayList<Joueur> selectionJoueurParmiListe;
	/**
	 * Un joueur pourra choisir une carte rumeur parmi cette liste lorsqu'il devra choisir une carte rumeur durant la partie.
	 */
	private ArrayList<CarteRumeur> selectionCarteRumeurParmiListe;
	/**
	 * Un joueur pourra choisir un entier appartenant � l'intervalle stock� dans cette liste. On stocke � l'indice 0 la borne inf�rieur et � l'indice 1, la borne sup�rieur.
	 */
	private ArrayList<Integer> selectionEntierIntervale;
	/**
	 * Attribut qui stocke le joueur s�lectionn� par le joueur qui doit jouer
	 */
	private Joueur joueurSelectionne;
	/**
	 * Attribut qui stocke l'entier s�lectionn� par le joueur qui doit jouer
	 */
	private Integer entierSelectionne;
	/**
	 * Attribut qui stocke la carte rumeur s�lectionn� par le joueur qui doit jouer
	 */
	private CarteRumeur carteRumeurSelectionne;
	/**
	 * Nom du joueur saisi lors du d�marrage de la partie. Aucun joueur dans une partie ne doit avoir le m�me nom qu'un autre.
	 */
	private String nomJoueurInitialisationPartieSelectionne;
	/**
	 * Attribut qui permet de garder en m�moire � quel endroit de la partie, on se situe. Pour pouvoir que chaque joueur saisisse la bonne chose en fonction du contexte du jeu.
	 * Cet attribut poss�de �galement une influence sur la strat�gie des robots. En effet, les joueurs virtuels peuvent adapter leur strat�gie en fonction du contexte du jeu. Un m�me robot ne va pas forc�ment choisir la m�me personne pour �tre le joueur qui va joueur le tour suivant, ou celui qu'il va accuser d'�tre une sorci�re.
	 */
	private EnumStatutPartie statutPartie;
	/**
	 * Attribut qui correspond � l'ensemble des messages qui seront affich� dans la vue.
	 * Cette liste ne comporte pas les messages associ� � la description des cartes/des joueurs lorsqu'il y a un choix � faire.
	 */
	private ArrayList<String> listeMessagesAAfficher;
	/**
	 * Attribut qui correspond � l'ensemble des messages qui seront affich� dans la vue. L'affichage est ici d�taill�e.
	 * C'est � dire que la liste comporte tous les messages de la liste listeMessagesAAfficher, mais pas seulement.
	 * Cette liste comporte �galement les messages associ� � la description des cartes/des joueurs lorsqu'il y a un choix � faire.
	 */
	private ArrayList<String> listeMessagesDetailleAAfficher;
	
	//constructeur
	/**
	 * Constructeur de la classe Partie, il permet d'instancier une partie et de l'initialiser.
	 */
	private Partie() {
		this.tempsDebutJeu = (new Date()).getTime();
		this.numeroManche = 0;
		
		this.listeCarteRumeurDefausser = new ArrayList<>();
		
		this.listeJoueurs = new ArrayList<>();
		this.joueurDebutTour = null;
		this.joueurQuiDoitJouer=null;
		
		this.changementJoueurAccusateurEffectue = false;
		
		//initialisation des attributs permettant la s�lection de chaque �l�ment lors du d�roulement de la partie
		this.selectionEnAttente = true;
		this.selectionJoueurParmiListe=new ArrayList<>();
		this.selectionCarteRumeurParmiListe=new ArrayList<>();
		this.selectionEntierIntervale=new ArrayList<>();
		this.joueurSelectionne=null;
		this.entierSelectionne=null;
		this.carteRumeurSelectionne=null;
		this.nomJoueurInitialisationPartieSelectionne = null;
		
		//initialisation de la liste des messages qui seront affich� dans la vue
		this.listeMessagesAAfficher=new ArrayList<>();
		this.listeMessagesDetailleAAfficher=new ArrayList<>();
		
		//on d�finit le statut de la partie
		this.statutPartie = EnumStatutPartie.EN_COURS;
	}

	//getter
	/**
	 * M�thode qui permet de r�cup�rer le nom du joueur saisi lors de l'attributation des noms aux joueurs lors de l'initialisation de la partie.
	 * @return Le nom du joueur saisi par l'utilisateur lors de l'initialisation de la partie.
	 */
	public String getNomJoueurInitialisationPartieSelectionne() {
		return nomJoueurInitialisationPartieSelectionne;
	}
	/**
	 * M�thode qui permet de r�cup�rer la liste des messages qui seront affich� dans la vue.
	 * @return Une liste de cha�nes de caract�res, correspondant aux messages d�di�s � la vue.
	 */
	public ArrayList<String> getListeMessagesAAfficher() {
		return listeMessagesAAfficher;
	}
	/**
	 * M�thode qui permet de r�cup�rer la liste des messages qui seront affich� dans la vue.
	 * La liste correspond � la liste des messages la plus d�taill� possible.
	 * Elle comporte la descriptions d�taill� de ce que le joueur doit choisir.
	 * Elle stocke la descriptions des cartes/... sous la forme d'une chaine de caract�re.
	 * @return Une liste de cha�nes de caract�res, correspondant aux messages d�di�s � la vue.
	 */
	public ArrayList<String> getListeMessagesDetailleAAfficher() {
		return listeMessagesDetailleAAfficher;
	}
	/**
	 * M�thode qui permet de savoir si une saisie est attendue des joueurs ou non.
	 * @return Un bool�en qui prend la valeur True, si un joueur doit saisir un �l�ment. Si personne ne doit saisir un �l�ment, alors le bool�en retourn� prend la valeur False.
	 */
	public boolean isSelectionEnAttente() {
		return selectionEnAttente;
	}
	/**
	 * M�thode qui permet de r�cup�rer la liste des joueurs parmi laquelle le joueurQuiDoitJouer peut choisir un joueur.
	 * @return Une liste de joueurs s�lectionnable
	 */
	public ArrayList<Joueur> getSelectionJoueurParmiListe() {
		return selectionJoueurParmiListe;
	}
	/**
	 * M�thode qui permet de r�cup�rer la liste des cartes rumeurs parmi laquelle le joueurQuiDoitJouer doit choisir une carte.
	 * @return Une liste de cartes rumeurs s�lectionnable
	 */
	public ArrayList<CarteRumeur> getSelectionCarteRumeurParmiListe() {
		return selectionCarteRumeurParmiListe;
	}
	/**
	 * M�thode qui permet de r�cup�rer les bornes de l'intervalle. Le joueur qui doit jouer doit choisir un entier compris entre ces deux bornes.
	 * @return Une liste de deux entiers. A l'indice 0, se situe la borne inf�rieur de l'intervalle. A l'indice 1, se situe la borne sup�rieur de l'intervalle.
	 */
	public ArrayList<Integer> getSelectionEntierIntervale() {
		return selectionEntierIntervale;
	}
	/**
	 * Cette m�thode permet de r�cup�rer quel joueur vient d'�tre s�lectionn� parmi la liste selectionJoueurParmiListe par le joueurQuiDoitJouer.
	 * @return Le joueur s�lectionn� par le joueurQuiDoitJouer. Si aucun joueur s�lectionn�, alors la valeur retourn�e est null.
	 */
	public Joueur getJoueurSelectionne() {
		return joueurSelectionne;
	}
	/**
	 * Cette m�thode permet de r�cup�rer quel entier vient d'�tre s�lectionn� parmi la liste selectionEntierParmiListe par le joueurQuiDoitJouer.
	 * @return Un entier appartenant aux bornes de l'intervalle pr�sent dans la liste selectionEntierParmiListe
	 */
	public Integer getEntierSelectionne() {
		return entierSelectionne;
	}
	/**
	 * Cette m�thode permet de r�cup�rer quelle carte rumeur vient d'�tre s�lectionn� parmi la liste selectionCarteRUmeurParmiListe par le joueurQuiDoitJouer.
	 * @return Une carte rumeur appartenant � liste selectionCarteRumeurParmiListe
	 */
	public CarteRumeur getCarteRumeurSelectionne() {
		return carteRumeurSelectionne;
	}
	/**
	 * M�thode qui permet de r�cup�rer le statut de la partie. Cela correspond � quelle op�ration est demand�e aux joueurs.
	 * @return Le statut � un instant t de la partie.
	 */
	public EnumStatutPartie getStatutPartie() {
		return statutPartie;
	}
	/**
	 * M�thode qui permet de r�cup�rer quant � d�buter la partie
	 * @return La valeur num�rique correspondant au temps pour la date du d�but de la partie d'apr�s le temps universel. Elle est ind�pendante du fuseau horaire.
	 */
	public long getTempsDebutJeu() {
		return this.tempsDebutJeu;
	}
	
	/**
	 * M�thode qui permet d'obtenir le num�ro de manche de la partie et donc de suivre l'�volution de la partie. 
	 * @return Le num�ro de manche de la partie
	 */
	public int getNumeroManche() {
		return this.numeroManche;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer toutes les cartes rumeurs qui ont �t� d�fauss�es � un moment donn� de la partie.
	 * @return Une liste de cartes rumeurs qui ont �t� d�fauss�es
	 */
	public ArrayList<CarteRumeur> getListeCarteRumeurDefausser() {
		return listeCarteRumeurDefausser;
	}

	/**
	 * M�thode qui permet de r�cup�rer tous les joueurs qui s'affrontent lors de la partie en cours. Ces joueurs peuvent �tre des joueurs r�els ou des joueurs virtuels.
	 * @return La liste des joueurs s'affrontant dans la partie
	 */
	public ArrayList<Joueur> getListeJoueurs() {
		return this.listeJoueurs;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer le joueur qui doit/ a lanc� une accusation ou utilis� l'effet chasseur d'une de ces cartes rumeurs
	 * @return Un joueur qui lance le d�but d'un nouveau tour
	 */
	public Joueur getJoueurDebutTour() {
		return this.joueurDebutTour;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer l'instance de la partie en cours. Pour ne pas avoir plusieurs parties ne m�me temps. (singleton)
	 * @return Une instance de Partie
	 */
	public static Partie getPartie() {//retourne l'instance de la partie
		return partie;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer le bool�en pour savoir si un changement de joueur est pr�vu � la fin du tour. 
	 * @return Un bool�en qui prend la valeur False s'il n'y a pas de changement de joueur de pr�vu � la fin d'un tour. S'il y a un changement de pr�vu, alors il prend la valeur True.
	 */
	public boolean getChangementJoueurAccusateurEffectue() {
		return changementJoueurAccusateurEffectue;
	}
	/**
	 * M�thode qui permet de r�cup�rer le joueur qui doit jouer � un moment donn� de la partie.
	 * @return Un joueur qui doit r�aliser une op�ration.
	 */
	public Joueur getJoueurQuiDoitJouer() {
		return this.joueurQuiDoitJouer;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer le CountDownLatch qui bloque l'�xecution du Thread principal lorsqu'une saisi est attendu sur les Threads secondaire.
	 * @return Le Synchroniseur des Threads
	 */
	public CountDownLatch getLatch() {
		return latch;
	}


	//setter
	/**
	 * M�thode qui permet de d�finir le nouveau CountDownLatch. 
	 * @param latch Le nouveau CountDownLatch (synchronizeur des Threads principal avec secondaire)
	 */
	public void setLatch(CountDownLatch latch) {
		this.latch = latch;
	}
	
	/**
	 * M�thode qui permet de modifier le nom du joueur saisi lors de l'attributation des noms aux joueurs lors de l'initialisation de la partie.
	 * @param nomJoueurInitialisationPartieSelectionne Le nouveau nom du joueur saisi par l'utilisateur
	 */
	public void setNomJoueurInitialisationPartieSelectionne(String nomJoueurInitialisationPartieSelectionne) {
		this.nomJoueurInitialisationPartieSelectionne = nomJoueurInitialisationPartieSelectionne;
	}
	/**
	 * M�thode qui permet de d�finir le nouveau joueur qui doit jouer. 
	 * @param joueurQuiDoitJouer Le prochain joueur qui doit jouer.
	 */
	public void setJoueurQuiDoitJouer(Joueur joueurQuiDoitJouer) {
		this.joueurQuiDoitJouer=joueurQuiDoitJouer;
	}
	
	/**
	 * M�thode qui permet de modifier le bool�en qui indique si un changement de joueur qui d�bute le prochain tour (joueurDebutTour de la classe partie) est pr�vu � la fin de la partie ou non.
	 * @param changementJoueurAccusateurEffectue Un bool�en qui doit avoir la valeur vrai si on veut indiquer que le joueur suivant est d�j� d�termin�. S'il n'est pas d�termin�, il faut fournir � cette m�thode la valeur False.
	 */
	public void setChangementJoueurAccusateurEffectue(boolean changementJoueurAccusateurEffectue) {
		this.changementJoueurAccusateurEffectue = changementJoueurAccusateurEffectue;
	}
	
	/**
	 * M�thode qui permet de modifier le temps quant � d�marr� la partie.
	 * @param tempsDebutJeu Le nouveau temps de d�part de la partie.
	 */
	public void setTempsDebutJeu(long tempsDebutJeu) {
		this.tempsDebutJeu = tempsDebutJeu;
	}
	
	/**
	 * M�thode qui permet de modifier le num�ro de la manche actuelle.
	 * @param numeroManche Le nouveau num�ro de manche.
	 */
	public void setNumeroManche(int numeroManche) {
		this.numeroManche = numeroManche;
	}
	
	/**
	 * M�thode qui permet de modifier la liste des joueurs qui s'affrontent durant la partie par une autre liste de joueur pass� en param�tre. 
	 * @param listeJoueurs La nouvelle liste de joueurs
	 */
	public void setListeJoueurs(ArrayList<Joueur> listeJoueurs) {
		this.listeJoueurs = listeJoueurs;
	}
	
	/**
	 * M�thode qui permet de modifier le joueur qui va d�buter un nouveau tour. Celui-ci devra soit accuser une autre personne d'�tre une sorci�re, soit d'utiliser l'effet chasseur d'une de ces cartes rumeurs.
	 * @param joueurDebutTour Le futur joueur qui va d�buter le tour suivant.
	 */
	public void setJoueurDebutTour(Joueur joueurDebutTour) {
		//on modifie le joueur qui d�bute le tour
		this.joueurDebutTour = joueurDebutTour;
		//on indique comme quoi on a bien d�termin� le joueur suivant
		Partie.getPartie().setChangementJoueurAccusateurEffectue(true);
	}
	
	/**
	 * M�thode qui permet de remplacer la liste des cartes rumeurs d�fauss�es par une autre liste de cartes pass�e en param�tre.
	 * @param listeCarteRumeurDefausser La nouvelle liste de carte rumeurs qui sont d�fauss�es
	 */
	public void setListeCarteRumeurDefausser(ArrayList<CarteRumeur> listeCarteRumeurDefausser) {
		this.listeCarteRumeurDefausser = listeCarteRumeurDefausser;
	}
	/**
	 * M�thode qui permet de modifier l'�tat de l'attribut qui permet de savoir si une s�lection est demand�e aupr�s des joueurs.
	 * @param selectionEnAttente Un bool�en qui prend la valeur True, si un joueur doit saisir un �l�ment. Si personne ne doit saisir un �l�ment, alors le bool�en doit avoir la valeur False.
	 */
	public void setSelectionEnAttente(boolean selectionEnAttente) {
		this.selectionEnAttente = selectionEnAttente;
	}	
	/**
	 * M�thode qui permet de modifier la liste des joueurs parmi laquelle le joueurQuiDoitJouer peut choisir un joueur.
	 * @param selectionJoueurParmiListe La nouvelle liste de joueurs que le joueur qui doit jouer peut choisir parmi.
	 */
	public void setSelectionJoueurParmiListe(ArrayList<Joueur> selectionJoueurParmiListe) {
		this.selectionJoueurParmiListe = selectionJoueurParmiListe;
	}	
	/**
	 * M�thode qui permet de modifier la liste des cartes rumeurs parmi laquelle le joueurQuiDoitJouer peut choisir une carte.
	 * @param selectionCarteRumeurParmiListe La nouvelle liste de carte rumeurs que le joueur qui doit jouer peut choisir parmi.
	 */
	public void setSelectionCarteRumeurParmiListe(ArrayList<CarteRumeur> selectionCarteRumeurParmiListe) {
		this.selectionCarteRumeurParmiListe = selectionCarteRumeurParmiListe;
	}	
	/**
	 * M�thode qui permet de modifier la liste des cartes rumeurs parmi laquelle le joueurQuiDoitJouer peut choisir une carte.
	 * @param selectionEntierIntervale La nouvelle liste d'entier contenant les bornes de l'intervalle. A l'indice 0, se situe la borne inf�rieur de l'intervalle. A l'indice 1, se situe la borne sup�rieur de l'intervalle.
	 */
	public void setSelectionEntierIntervale(ArrayList<Integer> selectionEntierIntervale) {
		this.selectionEntierIntervale = selectionEntierIntervale;
	}	
	/**
	 * Cette m�thode permet de modifier le joueur s�lectionn� parmi la liste selectionJoueurParmiListe par le joueurQuiDoitJouer.
	 * @param joueurSelectionne Le nouveau joueur s�lectionn� parmi la liste selectionJoueurParmiListe
	 */
	public void setJoueurSelectionne(Joueur joueurSelectionne) {
		this.joueurSelectionne = joueurSelectionne;
	}	
	/**
	 * Cette m�thode permet de modifier l'entier s�lectionn� parmi la liste selectionEntierParmiListe par le joueurQuiDoitJouer.
	 * @param entierSelectionne Le nouveau entier saisi appartenant aux bornes de l'intervalle de la liste selectionEntierParmiListe
	 */
	public void setEntierSelectionne(Integer entierSelectionne) {
		this.entierSelectionne = entierSelectionne;
	}	
	/**
	 * Cette m�thode permet de modifier la carte rumeur s�lectionn�e parmi la liste selectionCarteRumeurParmiListe par le joueurQuiDoitJouer.
	 * @param carteRumeurSelectionne La nouvelle carte rumeur s�lectionn� par le joueurSuiDoitJouer
	 */
	public void setCarteRumeurSelectionne(CarteRumeur carteRumeurSelectionne) {
		this.carteRumeurSelectionne = carteRumeurSelectionne;
	}
	/**
	 * M�thode qui permet de modifier le statut de la partie (qu'elle type de s�lection est attendu)
	 * @param statutPartie Le nouveau statut de la partie
	 */
	public void setStatutPartie(EnumStatutPartie statutPartie) {
		this.statutPartie = statutPartie;
	}
	/**
	 * M�thode qui permet de remplacer la liste actuelle de messages par une autre pass� en param�tre.
	 * @param listeMessagesAAfficher La nouvelle liste de message a afficher.
	 */
	public void setListeMessagesAAfficher(ArrayList<String> listeMessagesAAfficher) {
		this.listeMessagesAAfficher = listeMessagesAAfficher;
	}
	/**
	 * M�thode qui permet de remplacer la liste actuelle de messages (les messages les plus d�taill� possible) par une autre pass� en param�tre.
	 * Cette liste de messages comporte la descriptions des cartes rumeurs, ... sous la forme d'une chaine de caract�res.
	 * @param listeMessagesDetailleAAfficher La nouvelle liste de message a afficher.
	 */
	public void setListeMessagesDetailleAAfficher(ArrayList<String> listeMessagesDetailleAAfficher) {
		this.listeMessagesDetailleAAfficher = listeMessagesDetailleAAfficher;
	}		
	/**
	 * M�thode qui permet d'ajouter un message � la liste des messages qui seront affich� par la vue
	 * @param niveauDeDetail Entier qui doit avoir la valeur 0, si on veut que le message soit ins�r� uniquement dans la liste des messages pas d�taill�s. 1 dans le cas o� l'on veut que le message soit ins�r� uniquement dans la liste des messages d�taill�s. Si on veut que le messages soit ins�r� dans les deux listes, il faut alors saisir la valeur 2. 
	 * @param msg Le nouveau message que l'on souhaite rajout� dans la liste
	 */
	public void ajouterUnMessage(int niveauDeDetail, Object msg) {
		if(niveauDeDetail == 0 || niveauDeDetail == 2) {
			//si on veut que le message soit afficher dans la liste des messages non d�taill�
			this.listeMessagesAAfficher.add(msg.toString());
		}
		if(niveauDeDetail == 1 || niveauDeDetail == 2) {
			//si on veut que le messages soit dans la liste des messages d�taill�
			this.listeMessagesDetailleAAfficher.add(msg.toString());
		}
	}
	/**
	 * M�thode qui permet de suprimer l'enssemble des messages qui ont �tait affich� dans la liste des messages
	 */
	public void supprimerLesMessages() {
		this.listeMessagesAAfficher.clear();
		this.listeMessagesDetailleAAfficher.clear();
	}
	
	/**
	 * M�thode qui permet de r�cup�rer la dur�e de la partie sous la forme d'une cha�ne de caract�re "X min et X s"
	 * @return La dur�e de la partie : "X min et X s"
	 */
	public String getDureePartie() {
		long dureeSec = (((new Date()).getTime() - this.getTempsDebutJeu())/1000);
		long dureeMin = dureeSec/60;
		dureeSec%=60;
		return dureeMin+"min et "+dureeSec+"s";
	}
	
	/**
	 * M�thode qui permet de distribuer les cartes rumeurs et les cartes identit� � chaque joueur.
	 * Tous les joueurs disposent du m�me nombre de cartes rumeur et une unique carte identit�.
	 * Toute les cartes rumeurs non distribu�es sont alors d�fauss�.
	 */
	public void distributionCartes() {
		//m�thode qui permet de distribuer toute les cartes rumeurs et d'identit� � l'ensemble des joueurs
		Partie.getPartie().ajouterUnMessage(2,">>> Distribution des cartes pour chaque joueur");
		//on r�cup�re la liste de toute les cartes rumeur du jeu qui seront distribu� aux diff�rentss joueur
		ArrayList<CarteRumeur> listeCartesRumeur = Partie.getLaListeDeTouteLesCartesRumeurDuJeu();
		//on calcul le nombre de carte rumeur distribu� a chaque personne (3j=4c,4j=3c,5j=2c,6j=2c)
		int nbCartesRumeurParJoueur = listeCartesRumeur.size()/this.getNombreDeJoueur();
		//on initialise le random et la carte actuel qui est distribu�
		Random random = new Random();
		CarteRumeur carteR = null;
		//on parcours tout les joueurs pour leur donner leur cartes
		for(Joueur joueur:getListeJoueurs()) {
			//on lui donne sa carte identit�
			joueur.setCarteIdentite(new CarteIdentite());
			//on lui retire toute les cartes rumeurs qu'il avait (quand on change de manche, il ne faut pas que le joueur concerve les cartes qu'il lui restait � la manche d'avant)
			joueur.setListeCartesRumeurEnMain(new ArrayList<CarteRumeur>());
			//on lui donne toute ces cartes rumeur
			for(int i=0;i<nbCartesRumeurParJoueur;i++) {
				//on r�cup�re une carte al�atoire parmis la liste des cartesRumeur
				carteR = listeCartesRumeur.get(random.nextInt(listeCartesRumeur.size()));
				//on la donne au joueur
				joueur.ajouterCarteRumeurEnMain(carteR);
				//on la retire de la liste contenant l'ensemble des carte rumeur, pour qu'elle ne soit pas redistribuer une prochaine fois
				listeCartesRumeur.remove(carteR);
			}
		}
		
		//on met toute les cartes rumeurs non distribu�e dans la pile de carte d�fauss�e
		for(CarteRumeur carteADefausser:listeCartesRumeur) {
			this.ajouterCarteRumeurDefausser(carteADefausser);
		}
		
		//on affiche les cartes en main de chaque joueur suite � la distribution toute les carte rumeur ne sont pas r�v�l� donc en main
		//pour ce faire, on parcours l'ensemble des joueurs de la partie
		Iterator<Joueur> it = this.getListeJoueurs().iterator();
		while(it.hasNext()) {
			//on r�cup�re le joueur de l'it�ration & on modifie le joueur qui doit jouer 
			this.joueurQuiDoitJouer = it.next();
			//on affiche a qui appartient les cartes suivante
			Partie.getPartie().ajouterUnMessage(2,this.getJoueurQuiDoitJouer().getNomJoueur()+", voici les cartes dont vous disposez.");
			//on affiche toute les cartes qu'il a en main
			this.getJoueurQuiDoitJouer().affichageListeCarte(this.getJoueurQuiDoitJouer().getListeCartesRumeurEnMain());
			//on stocke la liste des cartes dans la liste de s�lections pour pouvoir les afficher via l'interface graphique
			Partie.getPartie().setSelectionCarteRumeurParmiListe(this.getJoueurQuiDoitJouer().getListeCartesRumeurEnMain());
			//on lui demande de saisir 0 pour pourvoir afficher les cartes du joueur suivant sans qu'il ne les voit
			//cela lui permet d'avoir du temps pour regarder ces cartes rumeurs pour choisir son r�le par la suite
			//on cache les texte pour les IA
			if(this.getJoueurQuiDoitJouer().getEstJoueurVirtuel() == false) {
				if(it.hasNext()) {
					Partie.getPartie().ajouterUnMessage(1,"Veuillez saisir 0 pour que le joueur suivant puisse regarder ses cartes � son tour");
					Partie.getPartie().ajouterUnMessage(0,"Veuillez appuyer sur le bouton CONFIRMER pour que le joueur suivant puisse regarder ses cartes � son tour");
				}else {
					Partie.getPartie().ajouterUnMessage(1,"Veuillez saisir 0 pour que chacun puisse choisir son r�le.");
					Partie.getPartie().ajouterUnMessage(0,"Veuillez appuyer sur le bouton CONFIRMER pour que chacun puisse choisir son r�le.");
				}
			}
			//on demande au joueur de confirmer qu'il a bien regard� les cartes
			Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(0,0))); //on modifie les bornes de l'intervalle. Pour confirmer le joueur doit saisir la valeur 0
			Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION_DISTRIBUTION_CARTE);
			
			//si c'est le dernier joueur et que c'est un joueur virtuel, alors on force la confirmation
			//un joueur r�el devra confirmer
			if(!it.hasNext() && Partie.getPartie().getJoueurQuiDoitJouer().getEstJoueurVirtuel() == true) {
				Partie.getPartie().ajouterUnMessage(1,"Veuillez saisir 0 pour que chacun puisse choisir son r�le.");
				Partie.getPartie().ajouterUnMessage(0,"Veuillez appuyer sur le bouton CONFIRMER pour que chacun puisse choisir son r�le.");
				
				Joueur joueurTemporaire = new Joueur("CONFIRMATION", false);
				Partie.getPartie().setJoueurQuiDoitJouer(joueurTemporaire);
				Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(0,0))); 
				//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
				Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
			}
		}
		
	}
	
	/**
	 * M�thode qui calcule le nombre de joueurs appartenant � la partie.
	 * @return Le nombre de joueur
	 */
	public int getNombreDeJoueur() {
		return this.getListeJoueurs().size();
	}
	
	/**
	 * M�thode qui permet de d�terminer si une manche est termin�e ou non.
	 * Une manche est termin�e lorsqu'il ne reste plus qu'une seule personne n'ayant pas r�v�l� son identit�.
	 * Quand la manche est termin�, cette m�thode attribut un certain nombre de points au joueur n'ayant pas r�v�l� son identit� en fonction de son r�le. Il gagne 2 points si c'est une sorci�re, ou 1 point si c'est un villageois.  
	 * De plus, elle r�v�le l'identit� du dernier joueur aux yeux des autres joueurs.
	 * @return Retourne un bool�en qui prend la valeur True si la manche est termin�e, ou False si la manche n'est pas termin�e.
	 */
	public boolean mancheFinie() {
		//m�thode qui permet de savoir si une manche de la partie est termin�
		//quand il ne reste plus qu'une personne non r�v�l�e, la manche est finie. Il r�v�le alors son r�le aux autres joueurs et gagne un nombre de point en fonction de celui-ci
		//la variable listeJoueurAvecIdentiteNonRevele va contenir l'enssemble des joueurs n'ayant pas r�v�l�e leur identit� lors de la manche
		ArrayList<Joueur> listeJoueurAvecIdentiteNonRevele = new ArrayList<Joueur>();
		//on g�n�re l'it�rateur qui va parcourir toute la liste des joueurs de la partie
		Iterator<Joueur> it = Partie.getPartie().getListeJoueurs().iterator();
		
		//on parcours toute la liste des joueur de la partie, le joueur �tudi� � chaque changement de cellule de la liste est contenu dans la variable joueurIteration
		Joueur joueurIteration;
		while(it.hasNext()) {
			//on r�cup�re le nouveau joueur a �tudier son cas
			 joueurIteration = it.next();
			//on regarde si le joueur n'a pas �tait r�v�l�
			if(joueurIteration.getEstRevele() == false) {
				//s'il n'a pas �tait r�v�l�, on l'ajoute � la liste des personnes n'ayant pas r�v�l�e leur identit�
				listeJoueurAvecIdentiteNonRevele.add(joueurIteration);
			}
		}
		
		//s'il ne reste plus qu'une personne non r�v�l�, alors elle a gagn� la manche, et donc la manche est finie
		if(listeJoueurAvecIdentiteNonRevele.size() == 1) {
			//on indique a tout le monde que la manche est termin�e
			Partie.getPartie().ajouterUnMessage(2,">>> La manche est termin�e");
			//la liste des joueur r�v�l�e ne comporte alors qu'un suel joueur, celui-ci est donc a l'indice 0 de la liste
			//cette personne pr�sente � l'indice 0 de la liste des joueurs non r�v�l�e est dont le dernier jouer a avoir r�v�ler son identit�
			//on fonction de son r�le on lui attribut un certain nombre de point et on l'affiche a tout les joueurs
			if(listeJoueurAvecIdentiteNonRevele.get(0).getCarteIdentite().getRole() == EnumRole.VILLAGEOIS) {
				//on affiche a tout le monde son r�le
				Partie.getPartie().ajouterUnMessage(2,"Hey, je me pr�nomme "+listeJoueurAvecIdentiteNonRevele.get(0).getNomJoueur()+" et je suis un villageois");
				Partie.getPartie().ajouterUnMessage(2,"Je gagne alors 1 point car je suis le dernier a �tre r�v�l�.");
				//il gagne 1 point si c'�tait un villageois
				listeJoueurAvecIdentiteNonRevele.get(0).setScore(listeJoueurAvecIdentiteNonRevele.get(0).getScore()+1);
			}else if(listeJoueurAvecIdentiteNonRevele.get(0).getCarteIdentite().getRole() == EnumRole.SORCIERE){
				//on affiche a tout le monde son r�le
				Partie.getPartie().ajouterUnMessage(2,"Hey, je me pr�nomme "+listeJoueurAvecIdentiteNonRevele.get(0).getNomJoueur()+" et je suis une sorci�re");
				Partie.getPartie().ajouterUnMessage(2,"Je gagne alors 2 points car je suis le dernier a �tre r�v�l�.");
				//il gagne 2 pts s'il s'agissait d'une sorci�re
				listeJoueurAvecIdentiteNonRevele.get(0).setScore(listeJoueurAvecIdentiteNonRevele.get(0).getScore()+2);
			}
			
			//on dit que le vainqueur sera le premier joueur a jouer au d�but de la prochaine manche
			this.setJoueurDebutTour(listeJoueurAvecIdentiteNonRevele.get(0));
			//on mets a jour le bool�en qui nous permet de savoir que le changement de joueur a bien �tait effectu�
			this.setChangementJoueurAccusateurEffectue(true);
			
			//on affiche le vainqueur de la partie dans la m�thode partieFinie() une fois que celle ci est termin�
			//on propose � l'utilisateur de revenir au menu du jeu
			Partie.getPartie().ajouterUnMessage(1,"Veuillez saisir 0 pour passer � la manche suivante.");
			Partie.getPartie().ajouterUnMessage(0,"Veuillez appuyer sur le bouton CONFIRMER pour passer � la manche suivante.");
			Joueur joueurTemporaire = new Joueur("Fin d'une manche", false);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurTemporaire);
			Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(0,0))); 
			//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
			Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
			
			//on d� r�v�le les joueur
			Iterator<Joueur> itJoueur = Partie.getPartie().getListeJoueurs().iterator();
			while(itJoueur.hasNext()) {
				//on mets a jour comme quoi il n'est plus r�v�l�
				itJoueur.next().setEstRevele(false);
			}
			
			//on retourne commme quoi la manche est termin�e
			return true;
		//s'il reste plus qu'une personne non r�v�l�, la manche n'est pas alors termin�
		}else {
			return false;
		}
	}
	
	/**
	 * M�thode qui permet de d�terminer si une partie est termin�e ou non.
	 * La partie se termine quand il y a au moins un joueur qui a plus de 5 points.
	 * S'il n'y a qu'une personne qui a plus de 5 points alors elle a gagn�.
	 * S'il y a plusieurs personne qui ont plus de 5 points, c'est celui qui a le plus de points qui remporte la partie.
	 * En cas d'�galit�, tous les joueurs qui ont le plus grand nombre de points ont gagn�.
	 * @return Retourne un bool�en qui prend la valeur True si la partie est termin�e, ou False si la partie n'est pas termin�e.
	 */
	public boolean partieFinie() {
		//m�thode pour d�tecter si la partie est termin�
		Iterator<Joueur> it = Partie.getPartie().getListeJoueurs().iterator();
		//on d�finit un set qui va contenir tout les joueurs ayant le plus grand score (il peut y en avoir plusieurs dans le cas d'une �galit�)
		LinkedHashSet<Joueur> listeJoueurVainqueur = new LinkedHashSet<Joueur>();
		//plus grand score stocke ne m�moire le plus grand score obtenue par un joueur dans la partie, celui-ci doit etre sup�rieur ou �gale � 5 pour que la partie soit finie
		int plusGrandScore = 0;
		
		//on parcours tout les joueurs de la partie
		while(it.hasNext()) {
			//on r�ucp�re le joueur 
			Joueur j = it.next();
			
			//on regarde s'il a eu plus de 5 pts et qu'il a eu un score au minimum sup�rieur au plus grand score obtenues par les joueurs pr�c�dents
			if(j.getScore() >= 5 && j.getScore() >= plusGrandScore) {
				//On regarde si le joueur en cours a un score qui a d�pass� l'ancien plus grand score parmit les joueurs pr�c�dent
				if(j.getScore() > plusGrandScore) {
					//dans ce cas les anciens vainqueurs ne sont plus vainqueurs, on vide alors la liste
					listeJoueurVainqueur.clear();
					//on d�finit le nouveau plus grand score obtenu lors de la partie
					plusGrandScore = j.getScore();
					//on l'ajoute dans la liste des vainqueurs
					listeJoueurVainqueur.add(j);
				}else {
					//si le joueur en cours est � �galit� alors le joueurs qui a le plus grand nombre de points, on l'ajoute alors � la liste
					listeJoueurVainqueur.add(j);
				}
			}
		}
		
		//si la liste des vainqueurs comptient au moins un vainqueurs, donc la partie est finit , car celui-ci a plus de 5pts
		if(!listeJoueurVainqueur.isEmpty()) {
			//on affiche le noms de chaque vainqueurs
			Partie.getPartie().ajouterUnMessage(1,"'''''''''''''''''''''''' FIN DE LA PARTIE ''''''''''''''''''''''''''''''''''");
			
			if(listeJoueurVainqueur.size() == 1) {
				Partie.getPartie().ajouterUnMessage(2,"Le vainqueur est : ");
			}else {
				Partie.getPartie().ajouterUnMessage(2,"Les vainqueurs sont :");
			}
			//on parcours toute la liste des vianqueurs pour afficher leurs noms
			Iterator<Joueur> it2 = listeJoueurVainqueur.iterator();
			while(it2.hasNext()) {
				Partie.getPartie().ajouterUnMessage(2,"     - "+it2.next().getNomJoueur());
			}
			
			//on affiche un r�capitulatif de la partie avec un message de remerciement
			this.recapitulatifDeLaPartie();
			Partie.getPartie().ajouterUnMessage(2,"Merci, � vous tous d'avoir participer � cette partie de Witch Hunt");
			
			//on indique que la partie est finie car au moins une personne a plus de 5pts
			return true;
		}
		//on retourne false dans le cas o� la partie n'est pas finit car personne n'a plus de 5 pts
		return false;
	}
	
	/**
	 * M�thode qui permet de changer de joueur suivant le sens horaire du tour de la table. Ici, on suppose que les joueurs sont plac�s au tour de la table comme ils sont plac�s dans la liste des joueurs de la partie. Le joueurDebutTour pr�sent dans la partie va ainsi devenir le joueur suivant dans la liste des joueurs qui suit le joueurDebutTour actuel.
	 * De plus, cette m�thode permet de choisir qui sera le premier joueur � jouer au d�but de la partie.
	 * Ce joueur va devoir au tour suivant accuser une autre personne d'�tre une sorci�re ou utiliser l'effet chasseur d'une carte rumeur.
	 */
	public void passerAuJoueurSuivant() {
		if(getJoueurDebutTour() == null) {
			Partie.getPartie().ajouterUnMessage(2,"Nous allons d�terminer al�atoirement le joueur qui va commencer");
			//si c'est le tout d�but de la partie
			Random random = new Random();
			//on choisi de mani�re al�atoire le joueur qui va accuser une personne ou utiliser une carte rumeur effet chasseur
			//il va devoir d�buter le premier tour
			setJoueurDebutTour(this.getListeJoueurs().get(random.nextInt(this.getListeJoueurs().size())));
			//on affiche le premieer joueur
			Partie.getPartie().ajouterUnMessage(2,this.getJoueurDebutTour().getNomJoueur()+" sera le premier joueur a jouer.");
			Partie.getPartie().ajouterUnMessage(2,"Mais avant, on va demander a tout le monde de regarder leur carte et de saisir leur r�le");
			
			//on bloque l'�xecution le temps que les joueurs lisent les msg
			Partie.getPartie().ajouterUnMessage(1, "Veuillez saisir 0, pour lancer la premi�re manche.");
			Partie.getPartie().ajouterUnMessage(0, "Veuillez appuyer sur le bouton CONFIRMER, pour lancer la premi�re manche.");
			Joueur joueurTemporaire = new Joueur("Initialisation de la partie", false);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurTemporaire);
			Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(0,0))); 
			//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
			Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
			
		}else {
			//si la partie a d�j� commenc� avant, le joueur qui va accuser le prochain tours correspond au joueur suivant dans la liste 
			//le joueur qui d�bute un tour ne devient donc jamais le joueur qui est accus�, mais uniquement le joueur accusateur.
			//cependant le jouer en cours peut devenir les deux
			//on consid�re que l'on change d'accusateur en bouclant sur la liste des personnes
			int indexJoueurDebutTour = 0;
			do {
				//on r�cup�re la position du joueur qui d�bute le tour dans la liste
				indexJoueurDebutTour = this.getListeJoueurs().indexOf(this.getJoueurDebutTour());
				//on passe au joueur suivant dans la liste
				indexJoueurDebutTour+=1;
				if(indexJoueurDebutTour>=this.getListeJoueurs().size()) {//�vite les d�passement de capacit�
					indexJoueurDebutTour=0;
				}
				//on modifie l'ancien joueur qui d�bute le tour par le nouveau
				this.setJoueurDebutTour(this.getListeJoueurs().get(indexJoueurDebutTour));
			//on reboucle sur le joueur suivant dans le cas o� il s'agit d'une sorci�re r�v�l�, qui est alors hors du jeu et ne peut donc pas accuser
			}while(this.getJoueurDebutTour().getEstRevele() == true && this.getJoueurDebutTour().getCarteIdentite().getRole()==EnumRole.SORCIERE);
			//on est alors pass� au tour suivant
			//on affiche le nom du joueur suivant
			Partie.getPartie().ajouterUnMessage(2,"Le joueur suivant sera "+this.getJoueurDebutTour().getNomJoueur());
		}
		//on modifie le prochain joueur qui va jouer.
		this.setJoueurQuiDoitJouer(this.getJoueurDebutTour());
	}
	
	/**
	 * M�thode qui retourne la liste de l'ensemble des cartes rumeurs appartenant au jeu. 
	 * @return Une liste de cartes rumeurs contenant toutes les cartes rumeurs qu'il est possible d'avoir durant une partie de Witch Hunt.
	 */
	public static ArrayList<CarteRumeur> getLaListeDeTouteLesCartesRumeurDuJeu(){
		//Instanciation de l'ensembles des cartes dans une liste
		ArrayList<CarteRumeur> listeCarteRumeur = new ArrayList<>();
		
		//carte Angry Mob
		CarteRumeur carteRumeurAngryMob = new CarteRumeur("Angry Mob", "",EnumJouabiliteEffetCarteRumeur.AUCUNE,EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS);
		carteRumeurAngryMob.ajouterUnEffetSorciere(new PrendreLeProchainTour());
		carteRumeurAngryMob.ajouterUnEffetChasseur(new RevelerIdentiteAutreJoueur());
		
		//carte The Inquisition
		CarteRumeur carteRumeurTheInquisition = new CarteRumeur("The Inquisition", "",EnumJouabiliteEffetCarteRumeur.AUCUNE,EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS);
		carteRumeurTheInquisition.ajouterUnEffetSorciere(new DefausserCarte());
		carteRumeurTheInquisition.ajouterUnEffetSorciere(new PrendreLeProchainTour());
		carteRumeurTheInquisition.ajouterUnEffetChasseur(new ChoisirJoueurSuivant());
		carteRumeurTheInquisition.ajouterUnEffetChasseur(new AvantLeurTourRegarderIdentite());
		
		//carte Pointed Hat
		CarteRumeur carteRumeurPointedHat = new CarteRumeur("Pointed Hat", "",EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_CARTE_RUMEUR_REVELEE,EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_CARTE_RUMEUR_REVELEE);
		carteRumeurPointedHat.ajouterUnEffetSorciere(new RecupererCarteRumeurRevele());
		carteRumeurPointedHat.ajouterUnEffetSorciere(new PrendreLeProchainTour());
		carteRumeurPointedHat.ajouterUnEffetChasseur(new RecupererCarteRumeurRevele());
		carteRumeurPointedHat.ajouterUnEffetChasseur(new ChoisirJoueurSuivant());
		
		//carte Hooked Nose
		CarteRumeur carteRumeurHookedNose = new CarteRumeur("Hooked Nose", "",EnumJouabiliteEffetCarteRumeur.AUCUNE,EnumJouabiliteEffetCarteRumeur.AUCUNE);
		carteRumeurHookedNose.ajouterUnEffetSorciere(new PrendreCarteJoueurAccusateur());
		carteRumeurHookedNose.ajouterUnEffetSorciere(new PrendreLeProchainTour());
		carteRumeurHookedNose.ajouterUnEffetChasseur(new ChoisirJoueurSuivant());
		carteRumeurHookedNose.ajouterUnEffetChasseur(new AvantLeurTourPrendreCarteJoueurSuivant());
		
		//carte Broomstick
		CarteRumeur carteRumeurBroomstick = new CarteRumeur("Broomstick", "Angry Mob",EnumJouabiliteEffetCarteRumeur.AUCUNE,EnumJouabiliteEffetCarteRumeur.AUCUNE);
		carteRumeurBroomstick.ajouterUnEffetSorciere(new PrendreLeProchainTour());
		carteRumeurBroomstick.ajouterUnEffetChasseur(new ChoisirJoueurSuivant());
		
		//carte Wart
		CarteRumeur carteRumeurWart = new CarteRumeur("Wart", "Ducking Stool",EnumJouabiliteEffetCarteRumeur.AUCUNE,EnumJouabiliteEffetCarteRumeur.AUCUNE);
		carteRumeurWart.ajouterUnEffetSorciere(new PrendreLeProchainTour());
		carteRumeurWart.ajouterUnEffetChasseur(new ChoisirJoueurSuivant());
		
		//carte Ducking Stool
		CarteRumeur carteRumeurDuckingStool = new CarteRumeur("Ducking Stool", "",EnumJouabiliteEffetCarteRumeur.AUCUNE,EnumJouabiliteEffetCarteRumeur.AUCUNE);
		carteRumeurDuckingStool.ajouterUnEffetSorciere(new ChoisirJoueurSuivant());
		carteRumeurDuckingStool.ajouterUnEffetChasseur(new ChoisirJoueurReverlerSonIdentiteOuDefausser());
		
		//carte Cauldron
		CarteRumeur carteRumeurCauldron = new CarteRumeur("Cauldron", "",EnumJouabiliteEffetCarteRumeur.AUCUNE,EnumJouabiliteEffetCarteRumeur.AUCUNE);
		carteRumeurCauldron.ajouterUnEffetSorciere(new DefausserCarteJoueurAccusateur());
		carteRumeurCauldron.ajouterUnEffetSorciere(new PrendreLeProchainTour());
		carteRumeurCauldron.ajouterUnEffetChasseur(new RevelerVotreIdentite());
		
		//carte Evil Eye
		CarteRumeur carteRumeurEvilEye = new CarteRumeur("Evil Eye", "",EnumJouabiliteEffetCarteRumeur.AUCUNE,EnumJouabiliteEffetCarteRumeur.AUCUNE);
		carteRumeurEvilEye.ajouterUnEffetSorciere(new ChoisirJoueurSuivant());
		carteRumeurEvilEye.ajouterUnEffetSorciere(new AccuserUneAutrePersonne());
		carteRumeurEvilEye.ajouterUnEffetChasseur(new ChoisirJoueurSuivant());
		carteRumeurEvilEye.ajouterUnEffetChasseur(new AccuserUneAutrePersonne());
		
		//carte Toad
		CarteRumeur carteRumeurToad = new CarteRumeur("Toad", "",EnumJouabiliteEffetCarteRumeur.AUCUNE,EnumJouabiliteEffetCarteRumeur.AUCUNE);
		carteRumeurToad.ajouterUnEffetSorciere(new PrendreLeProchainTour());
		carteRumeurToad.ajouterUnEffetChasseur(new RevelerVotreIdentite());
		
		//carte Black Cat
		CarteRumeur carteRumeurBlackCat = new CarteRumeur("Black Cat", "",EnumJouabiliteEffetCarteRumeur.AUCUNE,EnumJouabiliteEffetCarteRumeur.AUCUNE);
		carteRumeurBlackCat.ajouterUnEffetSorciere(new PrendreLeProchainTour());
		carteRumeurBlackCat.ajouterUnEffetChasseur(new AjouterCarteDefausserPuisDefausser());
		carteRumeurBlackCat.ajouterUnEffetChasseur(new PrendreLeProchainTour());
		
		//carte Pet Newt
		CarteRumeur carteRumeurPetNewt = new CarteRumeur("Pet Newt", "",EnumJouabiliteEffetCarteRumeur.AUCUNE,EnumJouabiliteEffetCarteRumeur.AUCUNE);
		carteRumeurPetNewt.ajouterUnEffetSorciere(new PrendreLeProchainTour());
		carteRumeurPetNewt.ajouterUnEffetChasseur(new PrendreCarteRumeurDUnePersonne());
		carteRumeurPetNewt.ajouterUnEffetChasseur(new ChoisirJoueurSuivant());
		
		//on ajoute toute les cartes rumeur dans la liste
		listeCarteRumeur.add(carteRumeurAngryMob);
		listeCarteRumeur.add(carteRumeurTheInquisition);
		listeCarteRumeur.add(carteRumeurPointedHat);
		listeCarteRumeur.add(carteRumeurHookedNose);
		listeCarteRumeur.add(carteRumeurBroomstick);
		listeCarteRumeur.add(carteRumeurWart);
		listeCarteRumeur.add(carteRumeurDuckingStool);
		listeCarteRumeur.add(carteRumeurCauldron);
		listeCarteRumeur.add(carteRumeurEvilEye);
		listeCarteRumeur.add(carteRumeurToad);
		listeCarteRumeur.add(carteRumeurBlackCat);
		listeCarteRumeur.add(carteRumeurPetNewt);

		return listeCarteRumeur;
	}
	
	/**
	 * M�thode qui permet d'ajouter un joueur � la partie. Ce joueur est pass� en param�tre de la m�thode.
	 * @param j Joueur que l'on souhaite ajouter � la partie
	 */
	public void ajouterJoueurALaPartie(Joueur j) {
		this.listeJoueurs.add(j);
	}
	
	/**
	 * M�thode qui permet la saisit de tous les joueurs r�els et virtuels.
	 * Elle demande � l'utilisateur de saisir le nombre de joueurs r�el compris entre 0 et 6.
	 * Puis, demande de saisir les noms de chaque joueur r�el.
	 * � noter qu'une partie doit �tre compos� entre 3 et 6 joueurs.
	 * S'il y a moins de 3 joueurs r�el, la partie doit �tre imp�rativement compl�t� avec des joueurs virtuels.
	 * Si la partie comporte entre 3 et 6 joueurs, l'utilisateur peut choisir de compl�ter la partie avec des joueurs virtuels sans d�passer le nombre maximal de joueur dans une partie.
	 * Si la partie comporte 6 joueurs r�els, alors, la partie ne peut pas �tre compl�t� avec des joueurs virtuels.
	 * Tous les joueurs virtuels disposent d'un nom commen�ant par Jean pour pouvoir les identifier plus rapidement possible.
	 * Il ne peut pas y avoir deux personnes avec le m�me nom.
	 */
	public void saisiDeLaListeDesJoueurs() {
		//stocke le nombre de joueur virtuel et r�el
		int nbJoueurReel = -1;
		int nbJoueurVirtuel = -1;
		//liste qui contient tout les noms des joueurs pour permettre de regarder plus facilement si un nom est d�j� utilis�, pour ne pas avoir de doublons
		ArrayList<String> listeNomJoueursReelEtVirtuel = new ArrayList<>(); 
		//nom du joueur saisit (temporairement)
		String nomJoueur="";
		
		//on d�finit tout les noms que les joueurs incarn� par des IA peuvent prendre
		ArrayList<String> listeNomJoueurVirtuel = new ArrayList<>(); 
		listeNomJoueurVirtuel.add("Jean-Christophe");
		listeNomJoueurVirtuel.add("Jean-Marie");
		listeNomJoueurVirtuel.add("Jean-Pierre");
		listeNomJoueurVirtuel.add("Jean-Daniel");
		listeNomJoueurVirtuel.add("Jean-Charles");
		listeNomJoueurVirtuel.add("Jean-Yve");
		
		//on demande de saisir le nombre de joueur r�el compris entre 0 et 6 et non entre 3-6 car il peut tr�s bien avoir une partie avec que des IA
		Partie.getPartie().ajouterUnMessage(1,"Veuillez saisir le nombre de joueur r�el : Entre 0 et 6");
		Partie.getPartie().ajouterUnMessage(0,"Veuillez s�lectionner le nombre de joueur r�el : Entre 0 et 6. Puis appuyer sur le boutton CONFIRMER.");
		//on modifie les bornes de l'intervalle. Il doit choisir enrte 0 et 6
		//on cr�e un joueur temporaire, car c'est au tour de personne de jouer
		Joueur joueurTemporaire = new Joueur("Initialisation de la partie", false);
		Partie.getPartie().setJoueurQuiDoitJouer(joueurTemporaire);

		Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(0,6))); 
		//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
		Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_ENTIER_NOMBRE_JOUEUR);
		//on r�cup�re la valeur du choix r�alis�
		nbJoueurReel = Partie.getPartie().getEntierSelectionne();
		
		//on demande d'inscrire le nom pour chaque joueur r�el
		if(nbJoueurReel != 0) {
			Partie.getPartie().ajouterUnMessage(2,"Veuillez saisir les noms de chaque joueurs");
			for(int i=0;i<nbJoueurReel;i++) {
				Partie.getPartie().ajouterUnMessage(2,"Nom du joueur n�"+(i+1));
				//on r�cup�re le nom du joueur et on regarde sa validiti� pour ne pas avoir de doublons de nom
				do {
					Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_CHAINE_CARACTERE_NOM_JOUEUR);
					nomJoueur = Partie.getPartie().getNomJoueurInitialisationPartieSelectionne();
				}while(nomJoueur == "" || listeNomJoueursReelEtVirtuel.contains(nomJoueur));
				//on ins�re le nom dans la liste des noms des joueurs, ce qui facilite la validation des noms
				listeNomJoueursReelEtVirtuel.add(nomJoueur);
				//on ajoute un joueur � la partie avec ce nom
				this.ajouterJoueurALaPartie(new Joueur(nomJoueur,false));
			}
		}
		
		
		Partie.getPartie().ajouterUnMessage(2,"Une partie comprend entre 3 et 6 joueurs.");
		//on affiche combien de joueur virtuels ils peuvent demander
		if(nbJoueurReel == 6) {
			Partie.getPartie().ajouterUnMessage(2,"La partie est pleine.");
		}else if(nbJoueurReel >= 3 && nbJoueurReel < 6){
			Partie.getPartie().ajouterUnMessage(2,"Vous �tes assez nombreux pour que la partie commence. Vous pouvez tout de m�me affronter des joueur virtuel. (maximum: "+(6-nbJoueurReel)+")");
			Partie.getPartie().ajouterUnMessage(1,"veuillez saisir 0, si vous ne voulez pas affronter de joueur virtuel.");
			Partie.getPartie().ajouterUnMessage(1,"Veuillez saisir le nombre de joueur virtuel (Entre 0 et "+(6-nbJoueurReel)+")");
			Partie.getPartie().ajouterUnMessage(0,"veuillez s�lectionner 0, si vous ne voulez pas affronter de joueur virtuel.");
			Partie.getPartie().ajouterUnMessage(0,"Veuillez s�lectionner le nombre de joueur virtuel (Entre 0 et "+(6-nbJoueurReel)+")");
		}else {
			Partie.getPartie().ajouterUnMessage(2,"Vous n'�tes pas assez nombreux. Vous devez �tre compl�ter entre "+(3-nbJoueurReel)+" et "+(6-nbJoueurReel)+" Joueurs virtuel");
			Partie.getPartie().ajouterUnMessage(1,"Veuillez saisir le nombre de joueur virtuel (Entre "+(3-nbJoueurReel)+" et "+(6-nbJoueurReel)+")");
			Partie.getPartie().ajouterUnMessage(0,"Veuillez s�lectionner le nombre de joueur virtuel (Entre "+(3-nbJoueurReel)+" et "+(6-nbJoueurReel)+")");
		}
		
		//s'ils peuvent pr�tendre avoir recours a des IA
		if(nbJoueurReel < 6) {
			//on recup�re un nombre de robot valablee selon les diff�rents cas
			
			//on modifie les bornes de l'intervalle. Il doit choisir enrte 0 et 6
			Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(3-nbJoueurReel,6-nbJoueurReel))); 
			//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
			Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_ENTIER_NOMBRE_JOUEUR);
			//on r�cup�re la valeur du choix r�alis�
			nbJoueurVirtuel = Partie.getPartie().getEntierSelectionne();
			
			//on prend des noms al�atoire pour les robots
			Random random = new Random();
			for(int j=0;j<nbJoueurVirtuel;j++) {
				//on r�cup�re un nom pour l'ia qui ne soit pas prit par un joueur r�el
				do {
					nomJoueur = listeNomJoueurVirtuel.get(random.nextInt(listeNomJoueurVirtuel.size()));
				}while(listeNomJoueursReelEtVirtuel.contains(nomJoueur));
				//on lee retire de la liste des noms pour les robots, pour ne pas repiocher ce meme nom par la suite et donc pour des gains de performance (pas de chance de reboucler dessus)
				listeNomJoueurVirtuel.remove(nomJoueur);
				//on l'ajoute dans la liste des joueurs
				listeNomJoueursReelEtVirtuel.add(nomJoueur);
				//on affiche que le joueur IA tir� va joueur
				Partie.getPartie().ajouterUnMessage(2,">> "+nomJoueur+" (IA), va jouer.");
				//on cr�e le joueur et on le mets dans la liste des joueurs de la partie
				this.ajouterJoueurALaPartie(new JoueurVirtuel(nomJoueur));
			}
		}
		
		Partie.getPartie().ajouterUnMessage(2,"La partie est initialis�. Elle peut alors commencer.");

	}
	
	/**
	 * M�thode qui permet d'ajouter une carte rumeur � la liste des cartes rumeurs d�fauss�es. Cette carte rumeurs est alors pass� en param�tre de la m�thode.
	 * @param carte Carte rumeur que l'on souhaite d�fausser.
	 */
	public void ajouterCarteRumeurDefausser(CarteRumeur carte) {
		this.listeCarteRumeurDefausser.add(carte);
	}
	
	/**
	 * 
	 * @param carte
	 */
	public void retirerCarteRumeurDefausser(CarteRumeur carte) {
		//on regarde si la carte appartient au tas de carte d�fausser avant de la retirer
		if(this.listeCarteRumeurDefausser.contains(carte)) {
			this.listeCarteRumeurDefausser.remove(carte);
		}
	}
	
	/**
	 * M�thode qui permet d'afficher les informations associ�es � une partie.
	 * <ul>
	 * <li>Temps de jeu de la partie</li>
	 * <li>Le num�ro de la manche</li>
	 * <li>Le nombre de joueurs dans la partie</li>
	 * <li>Le nom des diff�rents joueurs de la partie</li>
	 * <li>Le nom des joueurs non �limin�</li>
	 * <li>La liste des joueurs avec leur identit� pas encore r�v�l�</li>
	 * </ul>
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		//indicant le d�but de l'affichage
		sb.append("++++++++++\n");
		//affichage du temps de jeu depuis le tout d�but de la partie
		sb.append("Temps de Jeu: "+this.getDureePartie()+"\n");
		//affichage du num�ro de la manche en cours
		sb.append("Num�ro de manche: "+this.getNumeroManche()+"\n");
		//affichage du nombre de joueur
		sb.append("Nombre de joueur dans la partie : "+this.getNombreDeJoueur()+"/6\n");
		//affichage du joueur qui d�bute le tour si la partie a commenc�
		if(this.getJoueurDebutTour() != null) {
			sb.append("Nom du joueur qui d�bute le tour: "+this.getJoueurDebutTour().getNomJoueur()+"\n");
		}
		//affichage du joueur qui doit jouer
		if(this.getJoueurQuiDoitJouer() != null) {
			sb.append("Nom du joueur qui doit jouer: "+this.getJoueurQuiDoitJouer().getNomJoueur()+"\n");
		}
		//affichage des noms de chaque joueur
		if(!this.getListeJoueurs().isEmpty()) sb.append("Liste des noms des joueurs de la partie: \n");
		for(Joueur j : this.getListeJoueurs()) {
			//on affiche le nom de la personne
			sb.append("     - "+j.getNomJoueur());
			//on ajoute un sufixe (IA) pour savoir s'il s'agit d'une IA ou non
			if(j.getEstJoueurVirtuel() == true) {
				sb.append(" (IA)");
			}
			//on ajoute ensuite son score
			sb.append(" [Score = "+j.getScore()+"]");
			//s'il est r�v�l�e, on affiche sont r�le
			if(j.estElemine() == true) {
				sb.append(" {R�le : "+j.getCarteIdentite().getRole()+"}");
			}
			//on mets un retour � la ligne
			sb.append("\n");
			
		}
		
		//on affiche tout les joueurs de la manche actuelle
		sb.append("Liste des joueurs non �limin�s:\n");
		Iterator<Joueur> itJoueurManche = this.getListeJoueurNonEliminee(null).iterator();
		Joueur joueurManche;
		while(itJoueurManche.hasNext()) {
			joueurManche = itJoueurManche.next();
			//on ajoute le nom de la personnne
			sb.append("     - "+joueurManche.getNomJoueur());
			//on ajoute un sufixe (IA) pour savoir s'il s'agit d'une IA ou non
			if(joueurManche.getEstJoueurVirtuel() == true) {
				sb.append(" (IA)");
			}
			sb.append("\n");
		}
		
		//on affiche tout les joueurs non r�v�l�
		sb.append("Liste des joueurs avec identit� non r�v�l�s:\n");
		Iterator<Joueur> itJoueurNonRevele = this.getListeJoueurAvecIdentiteNonRevele(null).iterator();
		Joueur joueurNonRevele;
		while(itJoueurNonRevele.hasNext()) {
			joueurNonRevele = itJoueurNonRevele.next();
			//on ajoute le nom de la personnne
			sb.append("     - "+joueurNonRevele.getNomJoueur());
			//on ajoute un sufixe (IA) pour savoir s'il s'agit d'une IA ou non
			if(joueurNonRevele.getEstJoueurVirtuel() == true) {
				sb.append(" (IA)");
			}
			sb.append("\n");
		}
		
		//indicateur marquant la fin de l'affichage
		sb.append("++++++++++\n");
		//on retourne la chaine de caract�res qui sera affich� lorsque l'on souhaite afficher une partie
		return sb.toString();
	}
	
	/**
	 * M�thode qui permet de r�cup�rer un joueur parmi la liste des joueurs de la partie gr�ce � son nom pass� en param�tre de la m�thode.
	 * @param nomJoueur Nom du joueur recherch�
	 * @return On retourne l'instance du joueur avec le nom pass� en param�tre. S'il n'y a pas de joueur avec ce nom, alors on retourne la valeur null.
	 */
	public Joueur getJoueurDansListeGlobaleAPartircNom(String nomJoueur) {
		//retourne le joueur dans la liste contenant l'enssemble des jouers de la partie � partir de son nom
		for(Joueur j:this.getListeJoueurs()) {
			//on regarde s'il ont le meme nom que le joueur recherch�
			if(j.getNomJoueur().equals(nomJoueur)) {
				//on retourne le joueur avec le nom nomJoueur
				return j;
			}
		}
		//s'il n'existe pas, on retourne la valeur -1
		return null;
	}
	
	/**
	 * M�thode qui permet de demander a chaque joueur de la partie de choisir leur r�le.
	 */
	public void selectionDesRolesDeChaqueJoueur() {
		//m�thode qui permet de s�lectionner le r�le de tout les joueurs de la partie
		Partie.getPartie().ajouterUnMessage(2,">>> D�but de la s�lection des r�les de chaque joueur");
		//on r�cup�re l'it�rateur sur la liste de toute les joueurs de la partie
		Iterator<Joueur> it = this.getListeJoueurs().iterator();
		//on parcourt toute la liste des joueurs jusqu'� la fin
		while(it.hasNext()) {
			Joueur j = it.next();
			//on modifie le joueur qui doit jouer 
			this.joueurQuiDoitJouer = j;
			//on modifie le r�le du joueur o� pointe l'it�rateur
			j.choisirRole();
		}
		
		if(this.getListeJoueurs().get(this.getListeJoueurs().size()-1).getEstJoueurVirtuel() == true) {
			this.demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
		}
	}
	
	/**
	 * M�thode qui permet de demander une confirmation � un joueur r�el
	 * Dans la vue texte, un joueur r�el doit imp�rativement saisir la valeur 0 pour confirmer une chose.
	 * Ou un joueur r�el peut cliquer sur un bouton avec pour champ CONFIRMER.
	 */
	public void demandeConfirmationJoueurReel(EnumStatutPartie statut) {
		Partie.getPartie().ajouterUnMessage(1, "Veuillez saisir 0 pour confirmer.");
		Partie.getPartie().ajouterUnMessage(0, "Veuillez appuyer sur le boutton CONFIRMER.");
		Joueur joueurTemporaire = new Joueur("Confirmation", false);
		Partie.getPartie().setJoueurQuiDoitJouer(joueurTemporaire);
		Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(0,0))); 
		//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
		Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(statut);
	}
	
	/**
	 * M�thode qui permet de demander une confirmation � un joueur r�el ou virtuel.
	 * C'est au joueur qui doit jouer de r�aliser la confirmation. Il peut s'agir d'un joueur r�el mais �galement d'un joeuur virtuel.
	 * Si le joueur qui doit jouer est un joueur r�el, alors : 
	 * Dans la vue texte, il doit imp�rativement saisir la valeur 0 pour confirmer une choseou il peut cliquer sur un bouton avec pour champ CONFIRMER.
	 * Si c'est un joueur r�el, alors aucun joueur r�el ne doit r�aliser d'op�ration. 
	 */
	public void demandeConfirmation(EnumStatutPartie statut) {
		//on regarde si on doit afficher des messages sur les vues
		if(Partie.getPartie().getJoueurQuiDoitJouer().getEstJoueurVirtuel() == false) {
			Partie.getPartie().ajouterUnMessage(1, "Veuillez saisir 0 pour confirmer.");
			Partie.getPartie().ajouterUnMessage(0, "Veuillez appuyer sur le boutton CONFIRMER.");
		}
		//on dit que le joueur doit saisir la valeur 0
		Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(0,0))); 
		//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
		Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(statut);
	}
	
	/**
	 * M�thode qui permet de r�cup�rer tous les joueurs n'ayant pas r�v�l� leur identit� sans r�cup�rer le joueur qui est pass� en param�tre
	 * @param excepterCeJoueur Joueur que l'on ne veut pas r�cup�rer dans la liste. Mettre la valeur null, si on ne veut pas d'exception.
	 * @return Une liste de joueurs n'ayant pas encore r�v�l� leur identit�.
	 */
	public ArrayList<Joueur> getListeJoueurAvecIdentiteNonRevele(Joueur excepterCeJoueur){
		//m�thode qui permet de r�cup�rer tout les joueurs n'ayant pas r�v��le leur identit�
		//sans r�cup�rer le joueur qui est passer en param�tre
		//cette liste de joueur est stock� dans la variable res
		ArrayList<Joueur> res = new ArrayList<>();
		//on parcours l'ensemble des joueurs de la partie
		Iterator<Joueur> it = this.getListeJoueurs().iterator();
		//tant qu'il y a un joueur dont nous avons pas encore regard�
		while(it.hasNext()) {
			//on le stocke temporairement
			Joueur j = it.next();
			//on regarde s'il n'a pas r�v�l� son identit� et qu'il ne s'agit pas du joueur d'exception
			if(j.getEstRevele() == false && !j.equals(excepterCeJoueur)) {
				//si c'est le cas ont l'ajoute dans la liste des joueur qui n'ont pas r�v�l� leur identit�
				res.add(j);
			}
		}
		//on retourne la liste des joueurs
		return res;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer tous les joueurs de la manche, c'est-�-dire les joueurs qui ne sont pas �limin�s. Elle retourne une liste qui regroupe tous les joueurs avec une identit� non r�v�l�e ou poss�dant le r�le villageois.
	 * Sans r�cup�rer le joueur qui est pass� en param�tre
	 * @param excepterCeJoueur Joueur que l'on ne veut pas r�cup�rer dans la liste. Si on veut r�cup�rer tous les joueurs de la manche, il suffit de mettre excepterCeJoueur � null
	 * @return Une liste des joueurs �tant encore en vie dans la manche. Ces joueurs ne sont donc pas encore �limin�s de la manche.
	 */
	public ArrayList<Joueur> getListeJoueurNonEliminee(Joueur excepterCeJoueur){
		//m�thode qui permet de r�cup�rer tout les joueur de la manche, c'est a dire les joueurs qui ne sont pas �limin�. Elle retourne une liste qui regroupe tout les joueurs avec une identit� non r�v�l� ou poss�dant le r�le villageois
		//sans r�cup�rer le joueur qui est passer en param�tre
		//si on veut r�cup�rer tout les joueur de la manche, il suffit de mettre excepterCeJoueur � null
		//cette liste de joueur est stock� dans la variable res
		ArrayList<Joueur> res = new ArrayList<>();
		//on parcours l'ensemble des joueurs de la partie
		Iterator<Joueur> it = this.getListeJoueurs().iterator();
		//tant qu'il y a un joueur dont nous avons pas encore regard�
		while(it.hasNext()) {
			//on le stocke temporairement
			Joueur j = it.next();
			//on regarde s'il n'est pas �limin�, donc qu'il joue encore dans la manche 
			//et qu'il ne s'agit pas du joueur exceptionelle
			if(j.estElemine() == false && !j.equals(excepterCeJoueur)) {
				//si c'est le cas ont l'ajoute dans la liste des joueur qui font parti de la manche
				res.add(j);
			}
		}
		//on retourne la liste des joueurs
		return res;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer tous les joueurs de la manche, qui ont encore des cartes rumeur non r�v�l� sauf le joueur pass� en param�tre.
	 * @param excepterCeJoueur Joueur que l'on ne veut pas r�cup�rer dans la liste. Mettre la valeur null, si vous ne voulez pas d'exception.
	 * @return Une liste de joueurs poss�dant encore des cartes rumeurs non r�v�ler.
	 */
	public ArrayList<Joueur> getListeJoueurAvecCarteRumeurNonReveler(Joueur excepterCeJoueur){
		//m�thode qui permet de r�cup�rer tout les joueur de la manche, qui ont encore des cartes rumeur non r�v�l�
		ArrayList<Joueur> res = new ArrayList<>();
		//on parcours l'ensemble des joueurs de la partie
		Iterator<Joueur> it = this.getListeJoueurs().iterator();
		//tant qu'il y a un joueur dont nous avons pas encore regard�
		while(it.hasNext()) {
			//on le stocke temporairement
			Joueur j = it.next();
			//on regarde s'il n'est pas �limin�, donc qu'il joue encore dans la manche 
			//et qu'il ne s'agit pas du joueur exceptionelle
			if(j.estElemine() == false && !j.equals(excepterCeJoueur)) {
				//on regarde si ce joueur poss�de des carte rumeurs non r�v�l�
				if(j.listeCarteRumeurNonReveleEnMain().size() != 0) {
					//si c'est le cas, alors on l'ajoute � la liste des joueur poss�dant encore des carte rumeur
					res.add(j);
				}
			}
		}
		//on retourne la liste des joueurs
		return res;
	}
	
	/**
	 * M�thode qui permet de faire un r�capitulatif de la partie
	 */
	public void recapitulatifDeLaPartie() {
		Partie.getPartie().ajouterUnMessage(1,">>> R�capitulatif de la partie");
		Partie.getPartie().ajouterUnMessage(1,this);
	}
	
	/**
	 * M�thode qui permet de lancer un tour (accusation + r�ponse)
	 * Le joueur qui d�bute le tour (joueurDebutTour de la classe Partie) peut accuser un autre joueur d'�tre une sorci�re OU d'utiliser l'effet chasseur d'une de ces cartes rumeurs s'il peut en jouer.
	 * 	� la fin d'un tour, cette m�thode regarde s'il y a besoin ou non de pass� au joueur suivant dans la liste des joueurs.
	 */
	public void lancerUnTour() {
		//m�thode qui permet de lancer un tour de jeu (Accusation + r�ponse)
		//on r�initialise le bool�en qui nous permet de savoir que le joueur suivant � �tait d�termin� lors du tour
		this.setChangementJoueurAccusateurEffectue(false);
		//on modifie la valeur du joueur qui doit jouer par celui qui d�bute le tour
		this.setJoueurQuiDoitJouer(this.getJoueurDebutTour());
		//on affiche comme quoi c'est le d�but d'un tour
		Partie.getPartie().ajouterUnMessage(1,"'''''''''''''''''''''''' NOUVEAU TOUR | Accusateur : "+this.getJoueurQuiDoitJouer().getNomJoueur()+" [Score = "+this.getJoueurQuiDoitJouer().getScore()+"]"+" ''''''''''''''''''''''''''''''''''");
		
		int choix = 1;//Accuser =1, effet chasseur =2
		//on r�cup�re l'instance de la partie
		Partie p = Partie.getPartie();
		//on affiche le nom de la personne qui doit soit accuser un autre joueur soit utiliser l'une de ces carte rumeur
		Partie.getPartie().ajouterUnMessage(2,"C'est au tour de "+p.getJoueurQuiDoitJouer().getNomJoueur());
		//on regarde s'il peut utiliser l'effet chasseur d'une de ces cartes rumeurs
		if(p.getJoueurQuiDoitJouer().listeCarteRumeurJouableEnMain(false,true).size() != 0) {
			//si le joueur dispose de carte rumeur non r�l�v�, on lui demande alors de choisir entre accuser une autre personne d'�tre soric�re ou d'utiliser l'effet chasseur d'une de ces carte rumeur
			if(p.getJoueurQuiDoitJouer().getEstJoueurVirtuel() == false) {
				//on demande au joueur de choisir entre r�v�l�r son identit� ou tuiliser une carte rumeur effet chasseur
				Partie.getPartie().ajouterUnMessage(1,"Veuillez choisir entre 1 ou 2");
				Partie.getPartie().ajouterUnMessage(1,"     - 1 : Accuser un autre joueur d'�tre une sorci�re");
				Partie.getPartie().ajouterUnMessage(1,"     - 2 : Utiliser l'effet chasseur d'une carte rumeur");
				Partie.getPartie().ajouterUnMessage(0, "Veuillez choisir entre :");
				Partie.getPartie().ajouterUnMessage(0,"     - Accuser un autre joueur d'�tre une sorci�re");
				Partie.getPartie().ajouterUnMessage(0,"     - Utiliser l'effet chasseur d'une carte rumeur");
			}else {
				//on affiche aux autres joueur quoi comme l'ia doit proc�der a un choix
				Partie.getPartie().ajouterUnMessage(2,p.getJoueurQuiDoitJouer().getNomJoueur()+" doit choisir entre accuser un autre joueur ou utiliser une de ces carte rumeur.");
			}
				
			//on demande � l'utilisateur de choisir entre ces deux options et on v�rifie ce qu'il a saisi
			//on modifie les bornes de l'intervalle. Il doit choisir enrte 1 et 2
			Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(1,2))); 
			//on bloque l'execution du programme tant que le choix n'a pas �tait effectu�
			Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_ENTIER_ACCUSATION_OU_EFFET_CHASSEUR);
			//on r�cup�re la valeur du choix r�alis�
			choix = Partie.getPartie().getEntierSelectionne();
			
		}else {
			//si le joueur n'a pas le choix que d'accuser une autre personne car il n'a plus de carte rumeur
			//on affiche des message diff�rent de s'il et une IA ou un joueur r�el
			if(p.getJoueurQuiDoitJouer().getEstJoueurVirtuel() == false) {
				Partie.getPartie().ajouterUnMessage(2,"Vous n'avez plus de carte rumeur en main non r�vel�e dont vous pouvez utiliser son effet chasseur");
			}else {
				Partie.getPartie().ajouterUnMessage(2,p.getJoueurQuiDoitJouer().getNomJoueur()+" n'a plus de de carte rumeur en main non r�vel�e dont il peut utiliser son effet chasseur");
			}
			
		}
		
		if(choix == 1) {
			//si le joueur � d�cider d'accuser une autre personne ou est contraint d'accuser une autre personne car plus de carte rumeur jouable
			//on affiche l'intention du joueur
			if(p.getJoueurQuiDoitJouer().getEstJoueurVirtuel() == false) {
				Partie.getPartie().ajouterUnMessage(2,"Vous avez d�cid� d'accuser un autre joueur d'�tre une sorci�re");
			}
			//on lance l'accusation
			p.getJoueurQuiDoitJouer().accuser(Partie.getPartie().getListeJoueurAvecIdentiteNonRevele(Partie.getPartie().getJoueurQuiDoitJouer()));
		}else {
			//si le joueur � d�cid� d'utiliser l'effet chasseur d'une de ces carte rumeurs
			//on affiche l'intention du joueur
			if(p.getJoueurQuiDoitJouer().getEstJoueurVirtuel() == false) {
				Partie.getPartie().ajouterUnMessage(2,p.getJoueurQuiDoitJouer().getNomJoueur()+", vous avez d�cid� d'utiliser l'effet chasseur d'une de vos carte rumeur");
			}else {
				Partie.getPartie().ajouterUnMessage(2,p.getJoueurQuiDoitJouer().getNomJoueur()+" a d�cid� d'utiliser l'effet chasseur d'une de ces cartes rumeur.");
			}
			//on lui fais utiliser une carte rumeur effet chasseur
			p.getJoueurQuiDoitJouer().utiliserCarteRumeur(false,true);
		}
		
		//Si l'accus� ou l'accusateur n'ont pas r�aliser une op�ration modifiant le changement de personne (r�v�ler identit� villageois, utiliser un effet sp�cial de carte rumeur, ...)
		//Alors, on passe au joueur suivant dans la liste des joueur
		if(this.changementJoueurAccusateurEffectue == false) {
			//on passe au joueur suivant dans la liste des joueeur
			this.passerAuJoueurSuivant();
			//on indique que le changement de joueur a �tait effectu�
			this.changementJoueurAccusateurEffectue = true;
		}
		
	}
	
	/**
	 * M�thode qui permet de lancer une manche.
	 * <ul>
	 * <li>Distribution des cartes </li>
	 * <li>S�lection des r�les de chaque joueur.</li>
	 * <li>Lancer des tours jusqu'� ce que la manche soit termin�e.</li>
	 * <li>Donn� les points au gagnant de la manche.</li>
	 * </ul>
	 */
	public void lancerUneManche() {
		//M�thode qui permet de lancer une manche
		//on incr�ment le num�ro de la manche
		this.setNumeroManche(this.getNumeroManche()+1);
		Partie.getPartie().ajouterUnMessage(1,"'''''''''''''''''''''''' LANCEMENT DE LA MANCHE n�"+this.getNumeroManche()+" ''''''''''''''''''''''''''''''''''");
		//on distribue les cartes aux joueurs en d�but de chaque manche & affichage main des joueurs
		this.distributionCartes();
		//on r�capitule toute les informations de la partie en d�but de manche pour suivre l'�volution de la partie plus facilement
		this.recapitulatifDeLaPartie();
		//on leur demande de choisir leur r�le qui vont concerver durant toute la manche, il peut etre diff�rent d'une manche � l'autre
		this.selectionDesRolesDeChaqueJoueur();
		//tant qu'une manche n'est pas termin�, c'est � dire tant qu'il reste au moins deux personnes dont leur r�le n'est pas r�v�l�e, 
		while(this.mancheFinie() == false) {
			//on lance un nouveau tour
			this.lancerUnTour();
		}
		//quand la manche est finie, un certain nombre de point est attribu� au dernier joueur n'ayant pas r�v�l� son identit�
		//cette op�ration est effectu� dans la m�thode mancheFinie() une fois la manche finie
		
		//le joueur accusateur qui va d�buter la prochaine manche, s'il y a correspond au vainqueur de la macnhe pr�c�dante
		//l'attribut du joueur accusateur prend la valeur de l'instance du vainqueur 
		//cette op�ration est �galemeent effectu� dans mancheFinie()
	}
	
	/**
	 * M�thode qui permet de lancer une partie.
	 * <ul>
	 * <li>R�initialisation de la partie</li>
	 * <li>Saisi de la liste des joueurs</li>
	 * <li>S�lection du premier joueur qui joue</li>
	 * <li>Lancement des manches jusqu'� ce que la partie soit termin�.</li>
	 * <li>Affichage du vainqueur de la partie.</li>
	 * <li>Revenir au menu principal du jeu.</li>
	 * </ul>
	 */
	public void lancerUnePartie() {
		//on r�initialise la partie dans le cas o� l'on lance la partie une deuxi�me fois d'affil� si on d�sire rejouer
		this.reinitialisationPartie();
		//M�thode qui permet de lancer une partie
		Partie.getPartie().ajouterUnMessage(1,"'''''''''''''''''''''''' LANCEMENT DE LA PARTIE ''''''''''''''''''''''''''''''''''");	
		//on demande de saisir le nombre de joueur virtuel/physique, avec leur noms : qui vont jouer durant totue la partie
		this.saisiDeLaListeDesJoueurs();
		//on d�termine le premier joueur a jouer de mani�re al�atoire
		this.passerAuJoueurSuivant();
		//tant que la partie n'est pas finit, c'est � dire qu'aucune personne n'a plus de 5 pts
		while(this.partieFinie() == false) {
			//on lance une nouvele manche
			this.lancerUneManche();
		}
		//on affiche le vainqueur de la partie dans la m�thode partieFinie() une fois que celle ci est termin�
		//on propose � l'utilisateur de revenir au menu du jeu
		Partie.getPartie().ajouterUnMessage(1,"Voulez-vous revenir au menu du jeu ?");
		Partie.getPartie().demandeConfirmationJoueurReel(EnumStatutPartie.SELECTION_ENTIER_CONFIRMATION);
	}
	
	/**
	 * M�thode qui permet de r�initialiser une partie.
	 * Cette m�thode est essentiellement utile lorsque l'utilisateur d�cide de lancer plusieurs parties d'affil�es.
	 * On doit alors r�initialiser tous les attributs de la classe Partie.
	 * <ul>
	 * <li>R�initialisation du temps auquel le jeu a commenc�</li>
	 * <li>Remise � 0 du num�ro de manche</li>
	 * <li>Suppression des cartes d�fauss�es</li>
	 * <li>Suppression des joueurs</li>
	 * <li>Suppression du joueur qui d�bute le tour & joueur qui doit jouer</li>
	 * <li>Initialisation des attributs permettant la s�lection de chaque �l�ment lors du d�roulement de la partie</li>
	 * <li>Suppression des messages a afficher dans la vue</li>
	 * </ul>
	 */
	public void reinitialisationPartie() {
		//m�thode qui permet de r�initialiser l'�tat d'une partie
		//essentiellement utile lors que l'utilisateur d�cide de lancer plusieurs partie d'affil�
		//on r�initialise alors les attributs de la classe
		Partie.getPartie().supprimerLesMessages();
		
		this.tempsDebutJeu=(new Date()).getTime();
		this.numeroManche=0;
		this.listeCarteRumeurDefausser = new ArrayList<>();
		this.listeJoueurs = new ArrayList<>();
		this.joueurDebutTour=null;
		this.joueurQuiDoitJouer=null;
		this.changementJoueurAccusateurEffectue=false;
		
		//initialisation des attributs permettant la s�lection de chaque �l�ment lors du d�roulement de la partie
		this.selectionEnAttente = false;
		this.joueurSelectionne=null;
		this.entierSelectionne=null;
		this.carteRumeurSelectionne=null;
		this.nomJoueurInitialisationPartieSelectionne = null;
		
	}
	
	/**
	 * M�thode qui permet de notifier les observers afin qu'ils actualisent leur affichage.
	 */
	public void actualisationAffichage() {
		//on dit quu'ne modification � �tait apport�
		this.setChanged();
		//on notifie les oberserveur de la classe partie
		this.notifyObservers();
		
	}
	
}
