package Modele;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import Modele.enums.EnumJouabiliteEffetCarteRumeur;

/**
 * Classe qui repr�sente une carte rumeur du jeu.
 * Une carte rumeur est constitu� de plusieurs effets sorci�re et chasseur.
 * Certaines cartes peuvent �tre jou� sous certaines conditions (li�es aux joueurs qui l'utilisent et/ou aux autres joueurs).
 * L'effet chasseur d'une carte rumeur peut �tre utilis� � la place d'accuser une autre personne d'�tre une sorci�re.
 * L'effet sorci�re d'une carte rumeur peut �tre utilis� � la place de r�v�ler son identit�.
 * Une carte peut �tre r�v�l�e ou non.
 * Un joueur poss�de un certain nombre de cartes rumeur.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class CarteRumeur{
	//les attributs
	/**
	 * D�signe le nom d'une carte rumeur. (Exemple : Wart, Angry Mob, ...)
	 */
	private String nomCarte;
	/**
	 * D�signe le nom d'une carte rumeur, dont la carte rumeur (this) bloque les effets de la carte dont le nom est saisi. Ainsi, le joueur qui est prot�g� contre celle-ci lorsque la carte (this) est r�v�l�e. (Correspond au : While revealed, you cannot be chosen by the Angry Mob).
	 * Si la carte (this) ne prot�ge pas le joueur contre une autre carte, alors cet attribut doit avoir la valeur d'une cha�ne de caract�res vide, c'est-�-dire "". 
	 */
	private String protectionContreAutreCarte;
	/**
	 * D�signe l'�tat d'une carte rumeur, elle peut �tre r�v�l�e par un joueur ou non. Dans le cas o� le joueur l'a utilis�, il la r�v�le aux autres joueur en la montrant, alors cet attribut a pour valeur False. Dans le cas o� la carte rumeur appartient � la main du joueur, donc pas encore r�v�l�, cette carte est face cach�e, l'attribut vaut alors True.
	 */
	private boolean faceCache;
	/**
	 * D�signe le fait que les effets sorci�res peuvent �tre utilis�s sous certaines conditions. (Exemple : Jouable si le joueur qui utilise la carte est r�v�l� comme villageois, s'il a une carte rumeur de r�v�l�e, ...)
	 */
	private EnumJouabiliteEffetCarteRumeur conditionJouabiliteEffetsSorciere;
	/**
	 * D�signe le fait que les effets chasseurs peuvent �tre utilis�s sous certaines conditions. (Exemple : Jouable si le joueur qui utilise la carte est r�v�l� comme villageois, s'il a une carte rumeur de r�v�l�e, ...)
	 */
	private EnumJouabiliteEffetCarteRumeur conditionJouabiliteEffetsChasseur;
	/**
	 * Une carte rumeurs peut avoir plusieurs effets chasseurs. Cet attribut constitue alors une liste d'effets qui seront appliqu�s lorsque l'on d�sire utiliser l'effet chasseur d'une carte rumeur.
	 */
	private List<Effet> listeEffetsChasseur;
	/**
	 * Une carte rumeurs peut avoir plusieurs effets sorci�res. Cet attribut constitue alors une liste d'effets qui seront appliqu�s lorsque l'on d�sire utiliser l'effet sorci�re d'une carte rumeur.
	 */
	private List<Effet> listeEffetsSorciere;
	
	//constructeur
	/**
	 * Constructeur de la classe CarteRumeur, il permet d'instancier une carte rumeur et de l'initialiser.
	 * On y d�finit les conditions sur la jouabilit� de ces effets (r�v�l� villageois, ...), si la carte prot�ge le joueur contre certaines cartes lorsqu'elle est r�v�l�e.
	 * Cependant, suite � l'instanciation de la carte rumeur, celle-ci ne dispose pas d'effets (que cela soit des effets chasseurs ou des effets sorci�res).
	 * La carte est par d�faut face cach�e, elle n'est donc pas montr�e aux autres joueurs.
	 * @param nomCarte D�signe le nom d'une carte rumeur. (Exemple : Wart, Angry Mob, ...)
	 * @param protectionContreAutreCarte D�signe le nom d'une carte rumeur, dont la carte rumeur (this) bloque les effets dont le nom de la carte est saisi. Ainsi, le joueur qui est prot�g� contre celle-ci lorsqu'elle est r�v�l�e. (Correspond au : While revealed, you cannot be chosen by the Angry Mob).
	 * @param conditionJouabiliteEffetsSorciere D�signe le fait que les effets sorci�res peuvent �tre utilis�s sous certaines conditions. (Exemple : Jouable si le joueur qui utilise la carte est r�v�l� comme villageois, s'il a une carte rumeur de r�v�l�e, ...)
	 * @param conditionJouabiliteEffetsChasseur D�signe le fait que les effets chasseurs peuvent �tre utilis�s sous certaines conditions. (Exemple : Jouable si le joueur qui utilise la carte est r�v�l� comme villageois, s'il a une carte rumeur de r�v�l�e, ...)
	 */
	public CarteRumeur(String nomCarte, String protectionContreAutreCarte, EnumJouabiliteEffetCarteRumeur conditionJouabiliteEffetsSorciere, EnumJouabiliteEffetCarteRumeur conditionJouabiliteEffetsChasseur) {
		this.nomCarte=nomCarte;
		this.protectionContreAutreCarte=protectionContreAutreCarte;
		this.faceCache = true;
		this.conditionJouabiliteEffetsChasseur = conditionJouabiliteEffetsChasseur;
		this.conditionJouabiliteEffetsSorciere = conditionJouabiliteEffetsSorciere;
		
		this.listeEffetsChasseur=new ArrayList<>();
		this.listeEffetsSorciere=new ArrayList<>();
	}
	
	//getter
	/**
	 * M�thode qui permet de r�cup�rer le nom d'une carte rumeur (Exemple : Angry Mob)
	 * @return le nom d'une carte rumeur
	 */
	public String getNomCarte() {
		return this.nomCarte;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer le nom d'une carte rumeur dont les effets ne peuvent pas �tre utilis�s si la carte (this) est r�v�l�e.
	 * @return le nom d'une carte rumeur
	 */
	public String getProtectionContreAutreCarte() {
		return this.protectionContreAutreCarte;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer tous les effets qui doivent �tre appliqu�s lorsque le joueur qui d�tient la carte d�cide d'utiliser l'effet chasseur de sa carte.
	 * @return Une liste d'effets chasseurs
	 */
	public List<Effet> getListeEffetsChasseur(){
		return this.listeEffetsChasseur;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer tous les effets qui doivent �tre appliqu�s lorsque le joueur qui d�tient la carte d�cide d'utiliser l'effet sorci�re de sa carte.
	 * @return Une liste d'effets sorci�res
	 */
	public List<Effet> getListeEffetsSorciere(){
		return this.listeEffetsSorciere;
	}
	/**
	 * M�thode qui permet de savoir si la carte rumeur est r�v�l� ou non. Si elle n'est pas r�v�l�e, alors tous les autres joueurs ne la voient pas, et elle est situ� dans la main du joueur. Si elle est r�v�l�e, alors elle est visible par tous les joueurs.
	 * @return Un bool�en qui prend la valeur True si la carte est face cach�e, c'est-�-dire que la carte appartient � la main du joueur et n'est pas visible par les autres joueurs. Sinon, le bool�en prend la valeur False, si la carte a �t� utilis�e et donc visible par les autres joueurs.
	 */
	public boolean getFaceCache() {
		return this.faceCache;
	}
	
	/**
	 * M�thode qui permet de r�cup�rer la condition de jouabilit� des effets sorci�res de la carte rumeur. Certains effets sorciers n�cessitent d'�tre r�v�l�s villageois, ...
	 * @return La condition de jouabilit� de l'effet sorci�re de la carte rumeur. Si la carte n'a pas de restriction sur la jouabilit� de l'effet, alors la valeur retourn�e est EnumJouabiliteEffetCarteRumeur.AUCUNE.
	 */
	public EnumJouabiliteEffetCarteRumeur getConditionJouabiliteEffetsSorciere() {
		return conditionJouabiliteEffetsSorciere;
	}

	/**
	 * M�thode qui permet de r�cup�rer la condition de jouabilit� des effets chasseurs de la carte rumeur. Certains effets chasseurs n�cessitent d'�tre r�v�l�s villageois, ...
	 * @return La condition de jouabilit� de l'effet chasseur de la carte rumeur. Si la carte n'a pas de restriction sur la jouabilit� de l'effet, alors la valeur retourn�e est EnumJouabiliteEffetCarteRumeur.AUCUNE.
	 */
	public EnumJouabiliteEffetCarteRumeur getConditionJouabiliteEffetsChasseur() {
		return conditionJouabiliteEffetsChasseur;
	}

	//setter
	/**
	 * M�thode qui permet de modifier le nom d'une carte rumeur.
	 * @param nomCarte Le nouveau nom de la carte rumeur
	 */
	public void setNomCarte(String nomCarte) {
		this.nomCarte = nomCarte;
	}
	
	/**
	 * M�thode qui permet de modifier le nom de la carte rumeur, dont la carte rumeur (this) bloque les effets. Ainsi, le joueur qui est prot�g� contre celle-ci lorsque la carte est r�v�l�e.
	 * @param protectionContreAutreCarte Le nom d'une carte rumeur
	 */
	public void setProtectionContreAutreCarte(String protectionContreAutreCarte) {
		this.protectionContreAutreCarte = protectionContreAutreCarte;
	}
	
	/**
	 * M�thode qui permet de remplacer la liste des effets chasseurs de la carte rumeur par celle pass�e en param�tre de la m�thode.
	 * @param listeEffetsChasseur La nouvelle liste d'effets chasseurs de la carte rumeur.
	 */
	public void setListeEffetsChasseur(List<Effet> listeEffetsChasseur) {
		this.listeEffetsChasseur=listeEffetsChasseur;
	}
	
	/**
	 * M�thode qui permet de remplacer la liste des effets sorci�res de la carte rumeur par celle pass�e en param�tre de la m�thode.
	 * @param listeEffetsChasseur La nouvelle liste d'effets sorci�res de la carte rumeur.
	 */
	public void setListeEffetsSorciere(List<Effet> listeEffetsSorciere) {
		this.listeEffetsSorciere=listeEffetsSorciere;
	}
	
	/**
	 * M�thode qui permet de modifier l'�tat d'une carte rumeur. Cette carte rumeur peut �tre r�v�l�e ou non. Dans le cas o� elle n'est pas r�v�l�e, alors elle appartient � la main du joueur. Si elle est r�v�l�e, alors les autres joueurs peuvent la voir, ou l'ont vu.
	 * @param faceCache Un bool�en qui prend la valeur True, si la carte ne doit pas �tre visible par les autres et donc appartient � la main du joueur. Sinon prend la valeur False, si la carte rumeur est r�v�l�, elle est dans ce cas si visible par tout le monde.
	 */
	public void setFaceCache(boolean faceCache) {
		this.faceCache = faceCache;
	}
	
	/**
	 * M�thode qui permet les conditions de jouabilit� des effets sorci�res d'une carte rumeur. (Exemple : Jouable si le joueur qui utilise la carte est r�v�l� comme villageois, s'il a une carte rumeur de r�v�l�e, ...)
	 * @param conditionJouabiliteEffetsSorciere Les nouvelles conditions de jouabilit� des effets sorci�re de la carte rumeur
	 */
	public void setConditionJouabiliteEffetsSorciere(EnumJouabiliteEffetCarteRumeur conditionJouabiliteEffetsSorciere) {
		this.conditionJouabiliteEffetsSorciere = conditionJouabiliteEffetsSorciere;
	}
	
	/**
	 * 
M�thode qui permet les conditions de jouabilit� des effets chasseurs d'une carte rumeur. (Exemple : Jouable si le joueur qui utilise la carte est r�v�l� comme villageois, s'il a une carte rumeur de r�v�l�e, ...)
	 * @param conditionJouabiliteEffetsChasseur Les nouvelles conditions de jouabilit� des effets chasseurs de la carte rumeur
	 */
	public void setConditionJouabiliteEffetsChasseur(EnumJouabiliteEffetCarteRumeur conditionJouabiliteEffetsChasseur) {
		this.conditionJouabiliteEffetsChasseur = conditionJouabiliteEffetsChasseur;
	}
	
	//les autres m�thodes
	/**
	 * M�thode qui permet d'ajouter un nouvel effet suppl�mentaire qui sera appliqu� lorsque le joueur va utiliser les effets chasseurs de la carte rumeur.
	 * @param effet L'effet que l'on souhaite ajouter � la liste des effets chasseurs de la carte rumeur.
	 */
	public void ajouterUnEffetChasseur(Effet effet) {
		this.listeEffetsChasseur.add(effet);
	}
	
	/**
	 * M�thode qui permet d'ajouter un nouvel effet suppl�mentaire qui sera appliqu� lorsque le joueur va utiliser les effets sorci�res de la carte rumeur.
	 * @param effet L'effet que l'on souhaite rajouter � la liste des effets sorci�res de la carte rumeur.
	 */
	public void ajouterUnEffetSorciere(Effet effet) {
		this.listeEffetsSorciere.add(effet);
	}
	
	/**
	 * M�thode qui permet d'appliquer tous les effets cat�goris�s en effet sorci�re d'une carte rumeur. Ainsi, tous les effets pr�sents dans la liste des effets sorci�res seront actionn�s.
	 */
	public void appliquerEffetsSorciere() {
		//m�thode qui permet d'appliquer tout les effets cat�goris� en effet sorci�re d'une carte rumeur
		//on affiche comme quoi on va appliques les effets sorci�re
		Partie.getPartie().ajouterUnMessage(2,"Les effets sorci�res de la carte "+this.getNomCarte()+" vont �tre appliqu�s par "+Partie.getPartie().getJoueurQuiDoitJouer().getNomJoueur()+".");
		
		//on parcours l'ensemble des effets sorci�re de la carte rumeur
		Iterator<Effet> itEffets = this.listeEffetsSorciere.iterator();
		while(itEffets.hasNext()) {
			//on active chaque effet, un par un, en indiquant qui lance l'effet
			itEffets.next().activerEffet(Partie.getPartie().getJoueurQuiDoitJouer());
		}

	}
	
	/**
	 * M�thode qui permet d'appliquer tous les effets cat�goris�s en effet chasseurs d'une carte rumeur. Ainsi, tous les effets pr�sents dans la liste des effets chasseurs seront actionn�s.
	 */
	public void appliquerEffetsChasseur() {
		//m�thode qui permet d'appliquer tout les effets cat�goris� en effet chasseur d'une carte rumeur
		//on affiche comme quoi on va appliques les effets chasseur
		Partie.getPartie().ajouterUnMessage(2,"Les effets chasseurs de la carte "+this.getNomCarte()+" vont �tre appliqu�s par "+Partie.getPartie().getJoueurQuiDoitJouer().getNomJoueur()+".");
		
		//on parcours l'ensemble des effets chasseur de la carte rumeur
		Iterator<Effet> itEffets = this.listeEffetsChasseur.iterator();
		while(itEffets.hasNext()) {
			//on active chaque effet, un par un, en indiquant qui lance l'effet
			itEffets.next().activerEffet(Partie.getPartie().getJoueurQuiDoitJouer());
		}
		
	}
	
	/**
	 * M�thode qui permet d'afficher les informations les plus d�taill� possible d'une carte rumeur.
	 * <ul>
	 * <li>Son nom</li>
	 * <li>Si elle permet au joueur d'�tre prot�g� contre les effets d'une autre carte rumeur si elle est r�v�l�</li>
	 * <li>Si la carte rumeur est face cach�e ou non</li>
	 * <li>Les diff�rentes conditions de jouabilit�s des effets chasseurs et des effets sorci�res.</li>
	 * <li>La liste des effets chasseurs et sorci�res avec une description de chacun d'entre eux</li>
	 * </ul>
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		String conditionJouabilite="";
		
		//indicant le d�but de l'afficahge d'une carte
		sb.append("==========\n");
		
		//affichage du nom de la carte rumeur
		sb.append("Nom de la carte rumeur = "+this.getNomCarte()+"\n");
		
		//affichage de la protection contre les effets d'une autre contree s'il y en a 
		if(this.getProtectionContreAutreCarte() != "") sb.append("Protection contre la carte ayant pour nom "+this.getProtectionContreAutreCarte()+"\n");
		
		//indique le status d'une carte, si elle est montr� ou non aux autres
		sb.append("Carte face cach�="+this.getFaceCache()+"\n");
		
		
		if(this.getConditionJouabiliteEffetsSorciere() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS_ET_CARTE_RUMEUR_REVELEE) {
			conditionJouabilite = " (Jouable seulement si vous �tes r�v�l� comme Villageois et que vous avez une carte rumeur de r�v�l�e)";
		}else if(this.getConditionJouabiliteEffetsSorciere() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS) {
			conditionJouabilite = " (Jouable seulement si vous �tes r�v�l� comme Villageois)";
		}else if(this.getConditionJouabiliteEffetsSorciere() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_CARTE_RUMEUR_REVELEE){
			conditionJouabilite = " (Jouable seulement si vous avez une carte rumeur de r�v�l�e)";
		}
		
		//affichage de la liste des effets sorci�re de la carte
		if(!this.listeEffetsSorciere.isEmpty()) {
			sb.append("Liste Effet Sorciere :"+conditionJouabilite+"\n");
			for(Effet effet: this.listeEffetsSorciere) {
				sb.append("    - "+effet+"\n");
			}
		}
		
		if(this.getConditionJouabiliteEffetsChasseur() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS_ET_CARTE_RUMEUR_REVELEE) {
			conditionJouabilite = " (Jouable seulement si vous �tes r�v�l� comme Villageois et que vous avez une carte rumeur de r�v�l�e)";
		}else if(this.getConditionJouabiliteEffetsChasseur() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_REVELE_VILLAGEOIS) {
			conditionJouabilite = " (Jouable seulement si vous �tes r�v�l� comme Villageois)";
		}else if(this.getConditionJouabiliteEffetsChasseur() == EnumJouabiliteEffetCarteRumeur.JOUABLE_SI_CARTE_RUMEUR_REVELEE){
			conditionJouabilite = " (Jouable seulement si vous avez une carte rumeur de r�v�l�e)";
		}
		
		//affichage de la liste des effets chasseur de la carte
		if(!this.listeEffetsChasseur.isEmpty()) {
			sb.append("Liste Effet Chasseur :"+conditionJouabilite+"\n");
			for(Effet effet: this.listeEffetsChasseur) {
				sb.append("    - "+effet+"\n");
			}
		}
		
		//indicateur marquant la fin de l'affichage d'une carte
		sb.append("==========\n");
		
		return sb.toString();
	}
	
	/**
	 * M�thode qui permet de comparer une carte rumeur avec un objet. La comparaison s'effectue sur le type d'objet, le nom, si la carte est face cach�e ou non, et si elles ont la m�me protection contre une autre carte rumeur.
	 * @return Si les deux objets sont identiques, la valeur retourn�e est donc True, sinon False.
	 */
	public boolean equals(Object o) {
		if(o instanceof CarteRumeur) {
			CarteRumeur c = (CarteRumeur) o;
			//on regarde si les deux cartes ont le m�me nom
			if(c.getNomCarte().equals(this.getNomCarte())) {
				//on regarde si les deux cartes sont toute les deux face cach� ou non
				if(c.getFaceCache() == this.getFaceCache()) {
					//on regarde s'ils ont la meme proctection contre une autre carte
					if(c.getProtectionContreAutreCarte().equals(this.getProtectionContreAutreCarte())) {
						//si tout ces champs sont identiques alors c'est la meme carte
						return true;
					}
				}
			}
		}
		return false;
	}
	
}
