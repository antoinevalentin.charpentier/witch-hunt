package Modele;

import Modele.enums.EnumRole;

/**
 * Repr�sente la carte d'identit� du joueur.
 * Gr�ce � celle-ci, il peut choisir le r�le qu'il endossera lors de la partie.
 * On peut retrouver le r�le d'un joueur par l'interm�diaire de sa carte d'identit�.
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
public class CarteIdentite{
	//les attributs
	/**
	 * Garde en m�moire le r�le du joueur par l'interm�diaire d'une �num�ration des diff�rents r�les existant.
	 */
	private EnumRole role;
	
	//constructeur
	/**
	 * Constructeur de la classe CarteIdentite, il permet d'instancier une carte d'identit� et de l'initialiser.
	 * Par d�faut, le r�le s�lectionn� est celui de villageois.
	 */
	public CarteIdentite() {
		this.role = EnumRole.VILLAGEOIS;
	}
	
	//getter
	/**
	 * M�thode qui permet de retourner le r�le s�lectionn� via la carte d'identit�.
	 * Il correspond au r�le du joueur qui la d�tient.
	 * @return Le r�le du joueur qui poss�de la carte d'identit�
	 */
	public EnumRole getRole() {
		return this.role;
	}
	
	//setter
	/**
	 * M�thode qui permet de modifier le r�le associ� � la carte d'identit�.
	 * Il correspond au r�le du joueur qui la d�tient.
	 * @param role Le nouveau r�le associ� � la carte d'identit� et donc au joueur qui la poss�de.
	 */
	public void setRole(EnumRole role) {
		this.role = role;
	}
	
	//les autres m�thodes	
	/**
	 * M�thode qui affiche les caract�ristiques associ�es � la carte d'identit�
	 */
	public String toString() {
		return "CarteIdentite(r�le selectionn�="+getRole()+")";
	}
	
}
