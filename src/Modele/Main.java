package Modele;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.CountDownLatch;

import Modele.enums.EnumStatutPartie;

public class Main {
	/**
	 * M�thode qui permet d'afficher le menu du jeu
	 */
	public static void afficherMenu() {
		//m�thode qui permet l'affichage du menu du jeu
		//on affiche le menu du jeu
		Partie.getPartie().ajouterUnMessage(1,"============================================================");
		Partie.getPartie().ajouterUnMessage(1,"|  __          ___ _       _       _    _             _    |");
		Partie.getPartie().ajouterUnMessage(1,"|  \\ \\        / (_) |     | |     | |  | |           | |   |");
		Partie.getPartie().ajouterUnMessage(1,"|   \\ \\  /\\  / / _| |_ ___| |__   | |__| |_   _ _ __ | |_  |");
		Partie.getPartie().ajouterUnMessage(1,"|    \\ \\/  \\/ / | | __/ __| '_ \\  |  __  | | | | '_ \\| __| |");
		Partie.getPartie().ajouterUnMessage(1,"|     \\  /\\  /  | | || (__| | | | | |  | | |_| | | | | |_  |");
		Partie.getPartie().ajouterUnMessage(1,"|      \\/  \\/   |_|\\__\\___|_| |_| |_|  |_|\\__,_|_| |_|\\__| |");
		Partie.getPartie().ajouterUnMessage(1,"|                                                          |");
		Partie.getPartie().ajouterUnMessage(1,"|      Par Antoine-Valentin CHARPENTIER & Robin LEDUC      |");
		Partie.getPartie().ajouterUnMessage(1,"============================================================");
		Partie.getPartie().ajouterUnMessage(1,"");
		Partie.getPartie().ajouterUnMessage(1,"        MENU : ");
		Partie.getPartie().ajouterUnMessage(1,"             > 1 : Lancer une partie");
		Partie.getPartie().ajouterUnMessage(1,"             > 0 : Quitter");
		for(int i=0;i<6;i++) {
			Partie.getPartie().ajouterUnMessage(1,"");
		}
	}
	
	/**
	 * M�thode qui permet de lancer le jeu
	 */
	public static void lancerWitchHunt() {
		//méthode qui permet de lancer le jeu avec le menu
		int choix = -1;
		//tant que l'utilisateur n'a pas choisit de sortir du jeu
		while(choix != 0) {
			Partie.getPartie().setLatch(new CountDownLatch(0));
			//on affiche le menu
			Main.afficherMenu();
			//on demande au joueur de choisir entre 1 et 0, selon l'affichage du jeu
			//on modifie les bornes de l'intervalle. Il doit choisir enrte 1 et 0
			Partie.getPartie().setSelectionEntierIntervale(new ArrayList<>(Arrays.asList(0,1))); 
			//on crée un joueur temporaire, car c'est au tour de personne de jouer
			Joueur joueurTemporaire = new Joueur("Initialisation de la partie", false);
			Partie.getPartie().setJoueurQuiDoitJouer(joueurTemporaire);
			//on bloque l'execution du programme tant que le choix n'a pas était effectué			
			Partie.getPartie().getJoueurQuiDoitJouer().enAttenteDeSelection(EnumStatutPartie.SELECTION_ENTIER_MENU);
			//on récupère la valeur du choix réalisé
			choix = Partie.getPartie().getEntierSelectionne();
			
			//s'il a choisit de lancer la partie, alors on la lance
			if(choix == 1) {
				//on lance la partie
				Partie.getPartie().lancerUnePartie();
			}else {
				System.exit(0);
			}
			
		}
	}
	
}
