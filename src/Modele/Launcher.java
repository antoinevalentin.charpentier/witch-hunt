package Modele;

import java.io.Console;
import java.io.IOException;
import java.net.URISyntaxException;
import java.awt.GraphicsEnvironment;

import Vue.VueGraphique;

public class Launcher {
	/**
	 * M�thode qui permet de lancer le jeu. 
	 * Elle permet d'ouvrir le cmd puis d'executer la commande java -jar leNomDuFichier.jar.
	 * Elle constitue le point d'entr�e du programme.
	 * Elle permet d'avoir les deux vues concurrentes et non seulement la vue graphique mais �galement la vue textuelle.
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws URISyntaxException
	 */
	public static void main (String [] args) throws IOException, InterruptedException, URISyntaxException{
        Console console = System.console();
        if(console == null && !GraphicsEnvironment.isHeadless()){
            String filename = Main.class.getProtectionDomain().getCodeSource().getLocation().toString().substring(6);
            Runtime.getRuntime().exec(new String[]{"cmd","/c","start","cmd","/k","java -jar " + filename + ""});
        }else{
            VueGraphique.main(new String[0]);
        }
    }
}
