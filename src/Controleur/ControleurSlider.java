package Controleur;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JSlider;

import Modele.Partie;

/**
 * Controlleur qui permet de d�tecter un clic sur un JButton et de r�cup�rer la valeur d'un JSlider. La valeur envoy� au mod�le lors du clic est celui du JSlider.
 * @author Charpentier
 */
public class ControleurSlider {
	/**
	 * Bouton sur lequel le clic est d�tect�. Un clic sur celui-ci envoie la valeur du slider � la classe Partie.
	 */
	private JButton btn;
	/**
	 * Slider qui permet de s�lectionner un nombre compris entre deux bornes.
	 */
	private JSlider slider;
	
	/**
	 * M�thode qui permet de r�cup�rer le bouton sur lequel le clic doit �tre d�tect�.
	 * @return Le bouton sur lequel le clic doit �tre d�tect�.
	 */
	public JButton getBtn() {
		return btn;
	}

	/**
	 * M�thode qui permet de modifier le bouton sur lequel le clic doit �tre d�tect�.
	 * @param btn Le nouveau bouton
	 */
	public void setBtn(JButton btn) {
		this.btn = btn;
	}

	/**
	 * M�thode qui permet de r�cup�rer le slider, utilis� pour s�lectionn� un nombre.
	 * @return Le slider
	 */
	public JSlider getSlider() {
		return slider;
	}

	/**
	 * M�thode qui permet de remplacer le slider pour s�lectionner un nombre par un autre.
	 * @param slider Le nouveau slider
	 */
	public void setSlider(JSlider slider) {
		this.slider = slider;
	}
	
	/**
	 * Constructeur de la classe ControleurSlider
	 * Cette m�thode permet d'ajouter un listener au bouton afin de d�tecter un clic sur celui-ci.
	 * Quand un clic est effectu�, on r�cup�re la valeur du slider, que l'on affiche � l'�cran.
	 * Puis on le stocke dans l'attribut entierSelectionne de la classe Partie.
	 * Ensuite on d�bloque l'ex�cution du programme principale si l'entier est valide (normalement oui).
	 * @param slider Slider qui permet de r�cup�rer un entier compris entre les deux bornes de l'intervalle situ� dans l'attribut 
	 * @param btn Bouton permettant d'envoyer l'entier s�lectionn� par le slider � l'attribut entierSelectionne de la classe Partie. 
	 */
	public ControleurSlider(JSlider slider, JButton btn) {
		//on modifie l'�tat des attributs
		this.btn = btn;
		this.slider = slider;
		
		//d�tection du clic sur le bouton confirmer
		btn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//on affiche l'entier s�lectionn�
				System.out.println("Entier s�lectionn� : "+slider.getValue());
				//on informe la partie de l'entier s�lectionn�
				Partie.getPartie().setEntierSelectionne(slider.getValue());
				
				//on regarde si la s�lection est bonne (v�rification)
				if(Partie.getPartie().getEntierSelectionne() >= Partie.getPartie().getSelectionEntierIntervale().get(0) && Partie.getPartie().getEntierSelectionne() <= Partie.getPartie().getSelectionEntierIntervale().get(1)) {
					//si elle appartient � l'intervalle, alors on indique que l'on n'a plus besoin d'attendre
					Partie.getPartie().setSelectionEnAttente(false);
				}
				
			}
		});
	}

	
}
