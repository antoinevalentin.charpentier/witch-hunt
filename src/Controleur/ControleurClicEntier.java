package Controleur;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import Modele.Partie;

/**
 * Controlleur qui permet de d�tecter un clic sur un Component afin de simuler un clic sur un entier.
 * De plus, nous pouvons y retrouver la m�thode permettant de s�lectionner un entier dans la console.
 * @author Charpentier
 * @param <T> Le type de component sur lequel le joueur doit cliquer pour simuler un clic sur un entier
 */
public class ControleurClicEntier<T extends Component> {
	/**
	 * Attribut correspondant � l'entier associ� au component. Si un joueur clic sur le component, alors l'entier s�lectionn� sera cette entier.
	 */
	private int entier;
	/**
	 * Le component sur lequel le clic doit �tre d�tect�.
	 */
	private T t;
	
	/**
	 * M�thode permettant de r�cup�rer l'entier associ� au component.
	 * @return l'entier associ� au component.
	 */
	public int getEntier() {
		return entier;
	}

	/**
	 * M�thode permettant de modifier l'entier associ� au component
	 * @param entier Le nouvel entier associ� au component.
	 */
	public void setEntier(int entier) {
		this.entier = entier;
	}

	/**
	 * M�thode qui permet de r�cup�rer le component sur lequel le clic doit �tre effectu�.
	 * @return Un component sur lequel un joueur doit cliquer.
	 */
	public T getT() {
		return t;
	}

	/**
	 * M�thode qui permet de modifier le component sur lequel le joueur doit cliquer.
	 * @param t Le nouveau component
	 */
	public void setT(T t) {
		this.t = t;
	}
	
	/**
	 * Constructeur de la classe ControleurClicEntier
	 * M�thode permettant d'ajouter un listener sur le composant, et d'associer un clic sur ce composant � un entier.
	 * Si le joueur clic sur le component, alors on affiche l'entier associ� au component, on indique � la partie quel entier est s�lectionn�.
	 * Avant de d�bloquer l'ex�cution du programme, on v�rifie si cet entier appartient bien aux bornes de l'intervalle parmi lequelle le joueur peut choisir. Si c'est le cas, alors on d�bloque l'ex�cution du Thread principale.
	 * @param entier L'entier associ� au clic sur le component
	 * @param t Le component sur lequel le joueur doit cliquer
	 */
	public ControleurClicEntier(int entier, T t) {
		//modification des valeurs des attributs
		this.setEntier(entier);
		this.setT(t);
		
		//d�tection du clic sur le component
		t.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//on affiche quelle entier est s�lectionn�
				System.out.println("Entier s�lectionn� : "+entier);
				
				//on indique � la classe partie, quel entier est s�lectionn�
				Partie.getPartie().setEntierSelectionne(entier);
				
				//on regarde si la s�lection est bonne (v�rification)
				if(Partie.getPartie().getEntierSelectionne() >= Partie.getPartie().getSelectionEntierIntervale().get(0) && Partie.getPartie().getEntierSelectionne() <= Partie.getPartie().getSelectionEntierIntervale().get(1)) {
					//si elle appartient � l'intervalle, alors on indique que l'on n'a plus besoin d'attendre
					Partie.getPartie().setSelectionEnAttente(false);
				}
				
			}
		});
	}
	
	/**
	 * M�thode qui permet de choisir un entier compris entre deux bornes d'un intervalle
	 * La borne inf�rieur de l'intervalle est stock� dans Partie.getPartie().getSelectionEntierIntervale() � l'indice 0. Ainsi, le joueur ne pourra pas choisir une valeur inf�rieur � celle-ci
	 * La borne sup�rieur de l'intervalle est stock� dans Partie.getPartie().getSelectionEntierIntervale() � l'indice 1. Ainsi, le joueur ne pourra pas choisir une valeur sup�rieur � celle-ci
	 * L'entier choisi par le joueur, compris entre les deux bornes de l'intervalle est stock� dans l'attribut Partie > entierSelectionne
	 * Une fois la s�lection effectu�, on indique que la s�lection a �tait faite. Ce qui va permettre de d�bloquer l'�xecution du programme.
	 */
	public static void choisirParmiIntervalleEntier() {
		///m�thode qui permet de choisir un entier compris entre deux bornes d'un intervalle
		//on regarde ce que le joueur saisi sur le clavier
		//on mets � jour ce que le joueur doit voir
		Partie.getPartie().actualisationAffichage();
		
		//on dit que la valeur chosiit correspond � la valeur minimal -1 pour rentr�er dans les condition et pouvoir reboucler.
		//car la valeur du choix n'appartient donc pas � l'intervalle de de s�lection
		int choix = Partie.getPartie().getSelectionEntierIntervale().get(0)-1;//la borne inf�rieur -1
		
		//�coute du flux de donn�e
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); 
		//on stocke ce que le joueur � saisi dans la variable suivante
		String chaineSaisi = "";
		//on boucle tant que la valeur s�lectionn� n'appartient pas � l'intervalle et que le statut est en attente d'avoir un entier choisi
		//cette m�thode est non bloquante contrairement au scanner
		boolean enAttente = true;
		do {
			
			try {
				//on regarde si la joueur a envoy� un �l�ment dans le flux
				if(in.ready()) {
					//s'il a envoy� un truc, on le r�cup�re donc
					chaineSaisi=in.readLine();
					//on le converti ensuite en entier si possible
					try {
						choix = Integer.parseInt(chaineSaisi);
					}catch(NumberFormatException e) {
						choix = Partie.getPartie().getSelectionEntierIntervale().get(0)-1;
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			synchronized(Partie.getPartie()) {
				enAttente = Partie.getPartie().isSelectionEnAttente() && (choix < Partie.getPartie().getSelectionEntierIntervale().get(0) || choix >Partie.getPartie().getSelectionEntierIntervale().get(1));
			}
		}while(enAttente);
		
		
		//on enregistre quel entier vient d'�tre s�lectionn� dans un attribut de la classe partie
		if(Partie.getPartie().isSelectionEnAttente()) {
			Partie.getPartie().setEntierSelectionne(choix);
			//on indique que l'on n'a plus besoin d'attendre car le joueur � saisi une valeur correcte appartenant aux bornes de l'intervalles
			Partie.getPartie().setSelectionEnAttente(false);
		}
		//on diminue la valeur du compteur du synchronizeur de Threads, pour indiquer que l'on peut d�bloquer le Threads principale, o� tourne le mod�le
		Partie.getPartie().getLatch().countDown();
	}
	
}