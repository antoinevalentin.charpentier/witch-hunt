package Controleur;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

import Modele.Joueur;
import Modele.Partie;

/**
 * Controlleur qui permet de d�tecter un clic sur un Component afin de simuler un clic sur un joueur.
 * De plus, nous pouvons y retrouver la m�thode permettant de s�lectionner un joueur via son nom dans la console.
 * @author Charpentier
 * @param <T> Le type de component sur lequel le joueur doit cliquer pour simuler un clic sur un joueur
 */
public class ControleurClicJoueur<T extends Component> {
	/**
	 * Attribut correspondant au joueur associ� au component. Si un joueur clic sur le component, alors le joueur s�lectionn� sera ce joueur.
	 */
	private Joueur joueur;
	/**
	 * Le component sur lequel le clic doit �tre d�tect�.
	 */
	private T t;
	
	/**
	 * M�thode permettant de r�cup�rer le joueur associ� au component.
	 * @return le joueur associ� au component.
	 */
	public Joueur getJoueur() {
		return joueur;
	}

	/**
	 * M�thode permettant de modifier le joueur associ� au component
	 * @param joueur Le nouveau joueur associ� au component.
	 */
	public void setJoueur(Joueur joueur) {
		this.joueur = joueur;
	}
	/**
	 * M�thode qui permet de r�cup�rer le component sur lequel le clic doit �tre effectu�.
	 * @return Un component sur lequel un joueur doit cliquer.
	 */
	public T getT() {
		return t;
	}

	/**
	 * M�thode qui permet de modifier le component sur lequel le joueur doit cliquer.
	 * @param t Le nouveau component
	 */
	public void setT(T t) {
		this.t = t;
	}
	/**
	 * Constructeur de la classe ControleurClicJoueur
	 * M�thode permettant d'ajouter un listener sur le composant, et d'associer un clic sur ce composant � un joueur.
	 * Si le joueur clic sur le component, alors on affiche le nom du joueur associ� au component, on indique � la partie quel joueur est s�lectionn�.
	 * Avant de d�bloquer l'ex�cution du programme, on v�rifie si ce joueur appartient bien � la liste des joueurs parmi laquelle le joueur peut choisir. Si c'est le cas, alors on d�bloque l'ex�cution du Thread principale.
	 * @param joueur Le joueur associ� au clic sur le component
	 * @param t Le component sur lequel le joueur doit cliquer
	 */
	public ControleurClicJoueur(Joueur joueur, T t) {
		//modification des valeurs des attributs
		this.joueur = joueur;
		this.t = t;
		
		//d�tection du clic sur le component
		t.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//affichage du nom du joueur s�lectionn�
				System.out.println("Joueur s�lectionn� : "+joueur.getNomJoueur());
				
				//on indique � la partie qu'elle joueur vient d'�tre s�lectionn�
				Partie.getPartie().setJoueurSelectionne(joueur);
				
				//on regarde si la s�lection est bonne (v�rification)
				if(Partie.getPartie().getSelectionJoueurParmiListe().contains(joueur)) {
					//si elle appartient � l'intervalle, alors on indique que l'on n'a plus besoin d'attendre
					Partie.getPartie().setSelectionEnAttente(false);
				}
				
			}
		});
	}
	
	/**
	 * M�thode qui permet � un joueur de choisir un autre joueur parmit une liste pr�sent dans un attribut de la classe partie.
	 * On lui affiche l'ensemble des noms des joueurs qu'il peut choisir, puis on lui demande de saisir le nom du joueur qu'il veut s�lectionner.
	 * Le r�sultat de la s�lection est stock� dans l'attribut joueurSelectionne de la classe Partie
	 * Une fois la s�lection effectu�, on indique que la s�lection a �tait faite. Ce qui va permettre de d�bloquer l'�xecution du programme.
	 */
	public static void choisirJoueurParmiListe() {
		//m�thode qui permet de r�cup�rer un joueur de la partie appartenant � la liste pass� en param�tres en fonction du nom saisi
		ArrayList<Joueur> listeJoueur = Partie.getPartie().getSelectionJoueurParmiListe();
		//on regarde si la liste n'est pas vide
		if(listeJoueur.size() != 0) {
			//on affiche la liste des personnes qu'il peut saisir son nom
			Partie.getPartie().ajouterUnMessage(2,"Vous avez le choix entre :");
			Iterator<Joueur> it = listeJoueur.iterator();
			while(it.hasNext()) {
				Partie.getPartie().ajouterUnMessage(2,"     - "+it.next().getNomJoueur());
			}
			Partie.getPartie().actualisationAffichage();
			//variable qui stocke le nom du joueur saisi par le joueur
			String nomJoueur = "";
			//joueur qui sera retourn�
			Joueur res = null;
			//on initialise le scanner qui va permettre de r�cup�rer les saisi claviers
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			//bool�en qui passe a faux lorsque l'on a trouv� un joueur parmit la liste
			boolean continuerDemandeSaisiNomJouer = true;
			do {
				//on demande de saisir le nom du joueur
				try {
					if(in.ready()) {
						//on demande de saisir le nom du joueur
						nomJoueur = in.readLine();
						//on r�cup�re le joueur dans la partie associ� a ce nom (res = null si n'existe pas)
						res = Partie.getPartie().getJoueurDansListeGlobaleAPartircNom(nomJoueur);
						//on v�rifie que le joueur a saisi le nom d'un joueur existant dans la partie
						if(res != null) {
							//on regarde maintenant, s'il appartient � la liste pass� en param�tre
							if(listeJoueur.contains(res)) {
								//s'il appartient � la liste, on peut arr�ter de demander de saisir des noms
								continuerDemandeSaisiNomJouer = false;
								//on enregistre quel joueur vient d'�tre s�lectionn� dans un attribut de la classe partie
								Partie.getPartie().setJoueurSelectionne(res);
							}
						}
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			//on reboucle si le joueur n'a pas saisit un nom valide de joueur appartenant � la liste
			}while(continuerDemandeSaisiNomJouer == true && Partie.getPartie().isSelectionEnAttente());
			
			if(Partie.getPartie().isSelectionEnAttente()) {
				//on indique que l'on n'a plus besoin d'attendre car le joueur � saisi une valeur correcte appartenant aux bornes de l'intervalles
				Partie.getPartie().setSelectionEnAttente(false);
			}
			Partie.getPartie().getLatch().countDown();
		}

	}
}