package Controleur;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;

import Modele.Partie;
import Modele.enums.EnumRole;

/**
 * Controlleur qui permet de d�tecter un clic sur un JLabel afin de simuler la rotation de la carte rumeur lors de la s�lection des r�le de chaque joueur.
 * Le clic est d�tect� sur ce JLabel. Si le r�le actuel du joueur qui doit jouer est villageois, alors on envoie � la partie l'entier 2 ( pour qu'il choisit le r�le sorci�re), sinon 1 pour le r�le villageois.
 * @author Charpentier
 *
 */
public class ControleurRotationIdentite {
	/**
	 * JLabel servant de bouton. Le clic est d�tect� sur ce component.
	 */
	private JLabel labelRotationCarte;
	
	/**
	 * M�thode permetant de r�cup�rer le JLabel sur lequel le clic doit �tre d�tect�.
	 * @return Le JLabel sur lequel le clic doit �tre d�tect�.
	 */
	public JLabel getLabelRotationCarte() {
		return labelRotationCarte;
	}
	/**
	 * M�thode permetant de modifier le JLabel sur lequel le clic doit �tre d�tect�.
	 * @param labelRotationCarte Le nouveau JLabel sur lequel le clic doit �tre d�tect�.
	 */
	public void setLabelRotationCarte(JLabel labelRotationCarte) {
		this.labelRotationCarte = labelRotationCarte;
	}

	/**
	 * Constructeur de la classe ControleurRotationIdentit�
	 * M�thode permettant d'ajouter un listener sur le JLabel, et d'associer un clic sur ce composant � une rotation d'indentit�.
	 * Si le joueur clic sur le JLabel, alors on regarde qu'elle est le r�le du joueur qui a d� effectu� le clic.
	 * Si c'est un villageois, alors on dit � la partie que l'entier s�lectionn� est le 2, pour simuler qu'il a choisit le r�le sorci�re dans la console.
	 * Sinon si c'est une sorci�re, on envoie � la partie l'entier 1 qui correspond au r�le de villageois.
	 * Puis on d�bloque l'ex�cution du programme.
	 * @param labelRotationCarte Le composant qui permet de faire changer le r�le du joueur.
	 */
	public ControleurRotationIdentite(JLabel labelRotationCarte) {
		//modification de la valeur de l'attrbut
		this.setLabelRotationCarte(labelRotationCarte);
		
		//d�tection du clic sur le JLabel
		labelRotationCarte.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//on regarde le r�le du joueur qui a d� r�aliser la s�lection
				if(Partie.getPartie().getJoueurQuiDoitJouer().getCarteIdentite().getRole().equals(EnumRole.VILLAGEOIS)) {
					//si c'est un villageois
					//on indique � la partie que le joueur � s�lectionn� l'entier 2, ce qui correspond au r�le sorci�re
					Partie.getPartie().setEntierSelectionne(2);
				}else {
					// si c'est une sorci�re
					//on envoie � la partie que le joueur � s�lectionn� l'entier 1, ce qui correspond au r�le de villageois
					Partie.getPartie().setEntierSelectionne(1);
				}
				
				//on regarde si la s�lection est bonne (v�rification)
				if(Partie.getPartie().getEntierSelectionne() >= Partie.getPartie().getSelectionEntierIntervale().get(0) && Partie.getPartie().getEntierSelectionne() <= Partie.getPartie().getSelectionEntierIntervale().get(1)) {
					//si elle appartient � l'intervalle, alors on indique que l'on n'a plus besoin d'attendre
					Partie.getPartie().setSelectionEnAttente(false);
				}
			}
		});
	}

}
