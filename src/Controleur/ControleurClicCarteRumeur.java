package Controleur;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

import Modele.CarteRumeur;
import Modele.Joueur;
import Modele.Partie;

/**
 * Controlleur qui permet de d�tecter un clic sur un Component afin de simuler un clic sur une carte rumeur.
 * De plus, nous pouvons y retrouver la m�thode permettant de s�lectionner une carte rumeur via son nom dans la console.
 * @author Charpentier
 * @param <T> Le type de component sur lequel le joueur doit cliquer pour simuler un clic sur une carte rumeur.
 */
public class ControleurClicCarteRumeur<T extends Component> {
	/**
	 * Attribut correspondant � la carte rumeur associ�e au component. Si un joueur clic sur le component, alors la carte s�lectionn�e sera cette carte.
	 */
	private CarteRumeur carteRumeur;
	/**
	 * Le component sur lequel le clic doit �tre d�tect�.
	 */
	private T t;
	
	/**
	 * M�thode permettant de r�cup�rer la carte associ�e au component.
	 * @return la carte rumeur associ�e au component.
	 */
	public CarteRumeur getCarteRumeur() {
		return carteRumeur;
	}

	/**
	 * M�thode permettant de modifier la carte rumeur associ�e au component
	 * @param carteRumeur La nouvelle carte associ�e au component.
	 */
	public void setCarteRumeur(CarteRumeur carteRumeur) {
		this.carteRumeur = carteRumeur;
	}

	/**
	 * M�thode qui permet de r�cup�rer le component sur lequel le clic doit �tre effectu�.
	 * @return Un component sur lequel un joueur doit cliquer.
	 */
	public T getT() {
		return t;
	}

	/**
	 * M�thode qui permet de modifier le component sur lequel le joueur doit cliquer.
	 * @param t Le nouveau component
	 */
	public void setT(T t) {
		this.t = t;
	}
	
	/**
	 * Constructeur de la classe ControleurClicCarteRumeur
	 * M�thode permettant d'ajouter un listener sur le composant, et d'associer un clic sur ce composant � une carte rumeur.
	 * Si le joueur clic sur le component, alors on affiche le nom de la carte associ� au component, on indique � la partie quelle carte rumeur est s�lectionn�e.
	 * Avant de d�bloquer l'ex�cution du programme, on v�rifie si cette carte rumeur appartient bien � la liste des cartes rumeurs parmi laquelle le joueur peut choisir. Si c'est le cas, alors on d�bloque l'ex�cution du Thread principale.
	 * @param carteRumeur La carte rumeur associ�e au clic sur le component
	 * @param t Le component sur lequel le joueur doit cliquer
	 */
	public ControleurClicCarteRumeur(CarteRumeur carteRumeur, T t) {
		//on attribut des valeur aux attribut de la classe
		this.setCarteRumeur(carteRumeur);
		this.setT(t);
		
		//d�tection du clic sur le component
		t.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//on indique quelle carte a �tait cliqu�e dessus
				System.out.println("Carte Rumeur s�lectionn�e : "+carteRumeur.getNomCarte());
				
				//on modifie la valeur de l'attribut de la carte rumeur s�lection�e dans la classe partie
				Partie.getPartie().setCarteRumeurSelectionne(carteRumeur);
				
				//on regarde si la s�lection est bonne (v�rification)
				if(Partie.getPartie().getSelectionCarteRumeurParmiListe().contains(carteRumeur)) {
					//si elle appartient � l'intervalle, alors on indique que l'on n'a plus besoin d'attendre
					//on d�bloque alors l'ex�cution
					Partie.getPartie().setSelectionEnAttente(false);
				}
			}
		});
	}
	
	/**
	 * M�thode qui permet � un joueur de choisir une carte rumeur sp�cifique parmi une liste de carte rumeur pr�sent dans un attribut de la classe Partie > selectionCarteRumeurParmiListe
	 * Le r�sultat de la s�lection est stock� dans l'attribut carteRumeurSelectionne de la classe Partie
	 * Une fois la s�lection effectu�, on indique que la s�lection a �tait faite. Ce qui va permettre de d�bloquer l'�xecution du programme.
	 * @param suffixTexte Cette chaine de caract�re correspond au texte affich� apr�s "Voici la liste des cartes rumeurs", cela permet d'indiquer des informations suppl�mentaire au joueur lors de la s�lection
	 */
	public static void choisirCarteRumeurParmiListe(String suffixTexte) {
		//m�thode qui permet de r�cup�rer une carte rumeur appartenant � la liste des cartes rumeurs possiblement choissisable
		//on r�cup�re la liste des cartes rumeur parmi laquelle le joueur qui doit jouer peut choisir
		ArrayList<CarteRumeur> listeCarteRumeur = Partie.getPartie().getSelectionCarteRumeurParmiListe();
		//on regarde si la liste n'est pas vide,  
		if(listeCarteRumeur.size() != 0) {
			//on affiche toute les cartes qu'il peut s�lectionner
			Partie.getPartie().ajouterUnMessage(2,"Voici la liste des cartes rumeurs "+suffixTexte);
			Iterator<CarteRumeur> it = listeCarteRumeur.iterator();
			while(it.hasNext()) {
				Partie.getPartie().ajouterUnMessage(1,it.next());
			}
			//on demande de saisir le nom d'une carte rumeur qu'il a vu pr�c�dement
			Partie.getPartie().ajouterUnMessage(1,"Veuillez saisir le nom d'une de ces cartes rumeur");
			Partie.getPartie().ajouterUnMessage(0,"Veuillez cliquer sur une carte rumeur pour pouvoir la s�lectionner.");

			//variable qui stocke le nom de la carte saisi par le joueur
			String nomCarteRumeur = "";
			//carte rumeur qui sera retourn�
			CarteRumeur res = null;
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); // va permettre la lecture
			
			//on mets � jour ce que le joueur doit voir
			Partie.getPartie().actualisationAffichage();
			
			//bool�en qui passe a faux lorsque l'on a trouv� une carte rumeur parmit la liste
			boolean continuerDemandeSaisiNomCarteRumeur = true;
			do {
				//on demande de saisir le nom d'une des carte rumeur
				 try {
					if(in.ready()){
						nomCarteRumeur = in.readLine();
						
						//on r�cup�re la carte rumeur parmit la liste des cartes que le joueur a le choix
						res = Joueur.getCarteRumeurAPartircNom(nomCarteRumeur,listeCarteRumeur);
						//on v�rifie que le joueur a saisi le nom d'une carte rumeur existant dans la liste
						if(res != null) {			
							continuerDemandeSaisiNomCarteRumeur = false;
							//on enregistre quelle carte rumeur vient d'�tre s�lectionn� dans un attribut de la classe partie
							Partie.getPartie().setCarteRumeurSelectionne(res);
						}
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			//on reboucle si le joueur n'a pas saisit un nom valide de carte rumeur appartenant � la liste
			}while(continuerDemandeSaisiNomCarteRumeur == true && Partie.getPartie().isSelectionEnAttente());
			
			if(Partie.getPartie().isSelectionEnAttente()) {
				//on indique que l'on n'a plus besoin d'attendre car le joueur � saisi une valeur correcte appartenant aux bornes de l'intervalles
				Partie.getPartie().setSelectionEnAttente(false);
			}
			
		}
		//on diminue la valeur du compteur du synchronizeur de Threads, pour indiquer que l'on peut d�bloquer le Threads principale, o� tourne le mod�le
		Partie.getPartie().getLatch().countDown();
	}
	
}
