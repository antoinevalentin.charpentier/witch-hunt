package Controleur;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JTextField;

import Modele.Joueur;
import Modele.Partie;

/**
 * Controlleur qui permet de r�cup�rer la saisi d'une chaine de caract�res. Plus particuli�rement, le nom d'un joueur de la partie.
 * L'utilisateur saisi la cha�ne de caract�res dans un champ texte puis doit cliquer sur un bouton.
 * De plus, nous pouvons y retrouver la m�thode permettant de saisir le nom d'un joueur via le terminal.
 * @author Charpentier
 *
 */
public class ControleurSaisiNomJoueur {
	/**
	 * Bouton sur lequel le clic est d�tect�. Un clic sur celui-ci envoie la valeur de la cha�ne de cract�res � la classe Partie.
	 */
	private JButton btn;
	/**
	 * Champ texte dans lequel la cha�ne de caract�res est saisie.
	 */
	private JTextField textField;

	/**
	 * M�thode qui permet de r�cup�rer le bouton sur lequel le clic doit �tre d�tect�.
	 * @return Le bouton sur lequel le clic doit �tre d�tect�.
	 */
	public JButton getBtn() {
		return btn;
	}

	/**
	 * M�thode qui permet de modifier le bouton sur lequel le clic doit �tre d�tect�.
	 * @param btn Le nouveau bouton
	 */
	public void setBtn(JButton btn) {
		this.btn = btn;
	}

	/**
	 * M�thode qui permet de r�cup�rer le champ texte, dans lequel la cha�ne de caract�res est saisie.
	 * @return Un champ de type texte
	 */
	public JTextField getTextField() {
		return textField;
	}

	/**
	 *  M�thode qui permet de remplacer le champ texte, dans lequel la cha�ne de caract�res est saisie par un nouveau.
	 * @param textField Le nouveau champ de texte
	 */
	public void setTextField(JTextField textField) {
		this.textField = textField;
	}
	
	/**
	 * Constructeur de la classe ControleurSaisiNomJoueur.
	 * Cette m�thode permet d'ajouter un listener au bouton afin de d�tecter un clic sur celui-ci.
	 * Quand un clic est effectu�, on r�cup�re la valeur du champ texte, que l'on affiche � l'�cran.
	 * Puis on la stocke dans l'attribut nomJoueurInitialisationPartieSelectionne de la classe Partie.
	 * Ensuite, on d�bloque l'ex�cution  du programme si ce champ n'est pas vide et qu'aucun autre joueur n'a le m�me nom.
	 * @param textField Le champ texte dans lequel sera contenu 
	 * @param btn Le bouton servant � valider le nom.
	 */
	public ControleurSaisiNomJoueur(JTextField textField, JButton btn) {
		this.btn = btn;
		this.textField = textField;
		
		//d�tection du clic sur le bouton valider
		btn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.out.println("Nom s�lectionn� : "+textField.getText());
				
				Partie.getPartie().setNomJoueurInitialisationPartieSelectionne(textField.getText());
				
				//on regarde si la s�lection est bonne (v�rification)
				String nomJoueur = textField.getText();
				boolean verificationOK = true;
				//on regarde si le nom est diff�rent du nom vide, aucun joueur ne peut avoir comme nom une chaine de caract�re vide
				if(nomJoueur.isEmpty()) {
					verificationOK=false;
					Partie.getPartie().ajouterUnMessage(2,"Ce nom de joueur est vide.");
					Partie.getPartie().ajouterUnMessage(2, "Veuillez en saisir un nouveau, pris par personne et non vide.");
					//si le nom est vide ou qu'un autre joueur poss�de le meme, alors on lui demande d'en saisir un nouveau
					Partie.getPartie().actualisationAffichage();
				}else {
					//on parcours tout les joueurs et on regarde s'il y a un joueur qui a ce nom
					Iterator<Joueur> it = Partie.getPartie().getListeJoueurs().iterator();
					while(it.hasNext() && verificationOK == true) {
						if(it.next().getNomJoueur().equals(nomJoueur)) {
							verificationOK=false;
						}
					}
					if(verificationOK == false) {
						Partie.getPartie().ajouterUnMessage(2,"Ce nom de joueur ("+textField.getText()+") est d�j� pris par un autre joueur.");
						Partie.getPartie().ajouterUnMessage(2, "Veuillez en saisir un nouveau, pris par personne.");
						//si le nom est vide ou qu'un autre joueur poss�de le meme, alors on lui demande d'en saisir un nouveau
						Partie.getPartie().actualisationAffichage();
					}
				}
				
				if(verificationOK) {
					//si le nom n'est prit par personne, alors on indique que le joueur peut �tre cr�e
					Partie.getPartie().setSelectionEnAttente(false);
				}
				
			}
		});
	}
	
	/**
	 * M�thode qui permet de saisir le nom d'un joueur par l'interm�diaire d'une chaine de caract�res non vide.
	 * Cette m�thode est utilis� lors de l'initialisation de la partie uniquement, lorsqu'il faut saisir le nom de chaque joueur pour la premi�re fois.
	 */
	public static void saisirNomJoueur() {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); // va permettre la lecture
		
		Partie.getPartie().actualisationAffichage();
		String nom = "";
		do {
			 try {
					if(in.ready()){
						nom = in.readLine();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}while(Partie.getPartie().isSelectionEnAttente() && (nom == ""));
		
		//on enregistre le nom saisi par l'utilisateur
		if(Partie.getPartie().isSelectionEnAttente()) {//si on est sorti via le texte
			Partie.getPartie().setNomJoueurInitialisationPartieSelectionne(nom);
			//on indique que l'on n'a plus besoin d'attendre car le joueur � saisi une valeur correcte appartenant aux bornes de l'intervalles
			Partie.getPartie().setSelectionEnAttente(false);
		}	
		//on diminue la valeur du compteur du synchronizeur de Threads, pour indiquer que l'on peut d�bloquer le Threads principale, o� tourne le mod�le
		Partie.getPartie().getLatch().countDown();
	}
	
}