/**
 * Package qui regroupe les diff�rents Controleur du jeu.
 * Ces controleurs permettent la s�lection de joueur, de carte rumeur, d'entier, de cha�ne de caract�res, ...
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
package Controleur;