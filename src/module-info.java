/**
 * Impl�mentation du jeu Witch Hunt en Java.
 * Dans Chasse aux sorci�res, vous jouez le r�le de vieilles femmes qui r�pandent des rumeurs dans un village m�di�val et qui n�ont rien d�autre � faire que de mijoter des injures per�ues, et des ragots sur la probabilit� que les autres villageois soient associ�s � des forces obscures. Curieusement, certains villageois pourraient effectivement pratiquer la sorcellerie, et sont plut�t d�sireux de rester non d�couvert de peur qu�une foule en col�re n�arrive. Qui d�couvrira le plus de sorci�res et qui restera dans le village inaper�u? Est-ce que ce chat noir est vraiment un animal de compagnie de la famille? Qui a construit un tabouret de canard? Et qu�est-ce que tu fais avec ce triton ?
 * @author Antoine-Valentin CHARPENTIER & Robin LEDUC 
 */
module Witch_Hunt {
	requires java.desktop;
}